UPDATE ipatTransaction
SET idTransactionState = 1,
    partiallyReceivedBy = NULL,
    partiallyReceivedTo = NULL,
    receivedBy = NULL,
    receivedTo = NULL,
    partialReceptionDate = NULL,
    receptionDate = NULL,
    accidentDate = NULL,
    diligenceDate = NULL,
    address = NULL,
    idSeverity = NULL,
    idClassification = NULL,
    vehiclePlaque = NULL,
    criminalNews = NULL,
    observation = NULL
WHERE ipatNumber = 801786;