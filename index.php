<!--
== Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
== This software is the confidential and proprietary information of
== Secretaria de Transito. ("Confidential Information").
== You may not disclose such Confidential Information, and may only
== use such Confidential Information in accordance with the terms of
== the license agreement you entered into with Secretaria de Transito.
-->

<?php
    require_once('php/session/session_redirector.php');
?>

<!DOCTYPE html>
<html>
<?php include_once('views/shared/head.php'); ?>
<body ng-app="transitApp" ng-controller="appController">
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <div ng-include="'views/shared/dir_navbar.tpl.php'"></div>
    <div class="container">
        <div ng-include="'views/shared/dir_navigation.tpl.html'"></div>
        <div ng-show="isAgentShown">
            <div ng-include="'views/agent/agents_grid.tpl.html'"></div>
            <div class="fixed-action-btn" style="bottom:185px; right:24px;">
                <a class="waves-effect waves-light btn btn-floating btn-large red tooltipped"
                    data-position="left"
                    data-delay="50"
                    data-tooltip="Eliminar agente(s) seleccionado(s)"
                    ng-click="deleteSelectedAgents()">
                        <i class="large material-icons">clear</i>
                </a>
            </div>
            <div ng-show="isAdministrator()" class="fixed-action-btn" style="bottom:115px; right:24px;">
                <a class="waves-effect waves-light btn btn-floating btn-large cyan tooltipped"
                    data-position="left"
                    data-delay="50"
                    data-tooltip="Exportar a CSV"
                    href="php/action/agent/export_csv_agents.php">
                        <i class="large material-icons">file_download</i>
                </a>
            </div>
            <div class="fixed-action-btn" style="bottom:45px; right:24px;">
                <a class="waves-effect waves-light btn modal-trigger btn-floating btn-large blue tooltipped"
                    data-position="left"
                    data-delay="50"
                    data-tooltip="Crear nuevo agente"
                    href="#modal-agent-form"
                    ng-click="showCreateForm()">
                        <i class="large material-icons">add</i>
                </a>
            </div>
            <div ng-include="'views/agent/agents_modal_form.tpl.html'"></div>
        </div>
        <div ng-show="isSectionShown">
            <div ng-include="'views/section/sections_grid.tpl.html'"></div>
            <div class="fixed-action-btn" style="bottom:185px; right:24px;">
                <a class="waves-effect waves-light btn btn-floating btn-large red tooltipped"
                    data-position="left"
                    data-delay="50"
                    data-tooltip="Eliminar sección(es) seleccionada(s)"
                    ng-click="deleteSelectedSections()">
                        <i class="large material-icons">clear</i>
                </a>
            </div>
            <div ng-show="isAdministrator()" class="fixed-action-btn" style="bottom:115px; right:24px;">
                <a class="waves-effect waves-light btn btn-floating btn-large cyan tooltipped"
                    data-position="left"
                    data-delay="50"
                    data-tooltip="Exportar a CSV"
                    href="php/action/section/export_csv_sections.php">
                        <i class="large material-icons">file_download</i>
                </a>
            </div>
            <div class="fixed-action-btn" style="bottom:45px; right:24px;">
                <a class="waves-effect waves-light btn modal-trigger btn-floating btn-large blue tooltipped"
                    data-position="left"
                    data-delay="50"
                    data-tooltip="Crear nueva sección"
                    href="#modal-section-form"
                    ng-click="showCreateSectionForm()">
                        <i class="large material-icons">add</i>
                </a>
            </div>
            <div ng-include="'views/section/sections_modal_form.tpl.html'"></div>
        </div>
        <div ng-show="isIpatDeliveryShown">
            <div ng-include="'views/ipat_delivery/ipat_delivery_grid.tpl.html'"></div>
            <div ng-include="'views/ipat_delivery/ipat_delivery_modal_form.tpl.html'"></div>
            <div ng-include="'views/ipat_delivery/modify_ipat_delivery_modal_form.tpl.html'"></div>
            <div ng-include="'views/ipat_delivery/view_ipat_delivery_modal_form.tpl.html'"></div>
            <div ng-include="'views/ipat_delivery/ipat_delivery_novelty_modal_form.tpl.html'"></div>
            <div ng-include="'views/ipat_delivery/ipat_delivery_peace_and_save_modal_form.tpl.html'"></div>
            <div class="fixed-action-btn vertical">
                <a class="btn-floating btn-large red">
                    <i class="large material-icons">mode_edit</i>
                </a>
                <ul>
                    <li ng-show="(isAdministrator() || isIpatAdministrator())">
                        <a class="waves-effect waves-light btn btn-floating btn-large grey tooltipped"
                            data-position="left"
                            data-delay="50"
                            data-tooltip="Generar Paz y Salvo"
                            ng-click="openIpatDeliveryGeneratePeaceAndSaveModal()">
                            <i class="large material-icons">accessibility</i>
                        </a>
                    </li>
                    <li ng-show="isAdministrator()">
                        <a class="waves-effect waves-light btn btn-floating btn-large green tooltipped"
                            data-position="left"
                            data-delay="50"
                            data-tooltip="Exportar a CSV para Ministerio"
                            ng-click="openExportMinistryModal()">
                            <i class="large material-icons">file_download</i>
                        </a>
                    </li>
                    <li ng-show="(isAdministrator() || isIpatAdministrator())" >
                        <a class="waves-effect waves-light btn btn-floating btn-large red tooltipped"
                            data-position="left"
                            data-delay="50"
                            data-tooltip="Exportar a PDF para Ministerio"
                            ng-click="openExportMinistryPDFModal()">
                            <i class="large material-icons">picture_as_pdf</i>
                        </a>
                    </li>
                    <li ng-show="isAdministrator()">
                        <a class="waves-effect waves-light btn btn-floating btn-large cyan tooltipped"
                            data-position="left"
                            data-delay="50"
                            data-tooltip="Exportar a CSV"
                            href="php/action/ipat_delivery/export_csv_ipat_deliveries.php">
                            <i class="large material-icons">file_download</i>
                        </a>
                    </li>
                    <li ng-show="(isAdministrator() || isIpatAdministrator())" >
                        <a class="waves-effect waves-light btn modal-trigger btn-floating btn-large blue tooltipped"
                            data-position="left"
                            data-delay="50"
                            data-tooltip="Entregar nuevo IPAT"
                            href="#modal-ipat-delivery-form"
                            ng-click="showCreateIpatDeliveryForm()">
                            <i class="large material-icons">add</i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div ng-show="isTransitTicketsShown" ng-controller="transitTicketTransactionController">
            <div ng-include="'views/transit_ticket/transit_ticket_grid.tpl.html'"></div>
            <div class="fixed-action-btn vertical">
                <a class="btn-floating btn-large red">
                    <i class="large material-icons">mode_edit</i>
                </a>
                <ul>
                    <li>
                        <a class="waves-effect waves-light btn btn-floating btn-large grey tooltipped"
                            data-position="left"
                            data-delay="50"
                            data-tooltip="Generar Paz y Salvo"
                            ng-click="openGeneratePeaceAndSaveModal()">
                            <i class="large material-icons">accessibility</i>
                        </a>
                    </li>
                    <li ng-show="isAdministrator()">
                        <a class="waves-effect waves-light btn btn-floating btn-large cyan tooltipped"
                            data-position="left"
                            data-delay="50"
                            data-tooltip="Exportar a CSV"
                            href="php/action/transit_ticket/export_csv_transit_tickets.php">
                            <i class="large material-icons">file_download</i>
                        </a>
                    </li>
                    <li>
                        <a class="waves-effect waves-light btn modal-trigger btn-floating btn-large blue tooltipped"
                            data-position="left"
                            data-delay="50"
                            data-tooltip="Crear Talonario de Comparendos"
                            href="#modal-transit-ticket-talonary-form">
                            <i class="large material-icons">add</i>
                        </a>
                    </li>
                </ul>
            </div>
            <div ng-include="'views/transit_ticket/transit_ticket_talonary_modal_form.tpl.html'"></div>
            <div ng-include="'views/transit_ticket/transit_ticket_transaction_modal_form.tpl.html'"></div>
            <div ng-include="'views/transit_ticket/transit_ticket_transaction_novelty_modal_form.tpl.html'"></div>
            <div ng-include="'views/transit_ticket/generate_peace_and_save_modal_form.tpl.html'"></div>
        </div>
        <div ng-show="isReportIpatReceivedWithPendingShown">
            <div ng-include="'views/reports/report_ipat_received_with_pending_grid.tpl.html'"></div>
            <div class="fixed-action-btn" style="bottom:45px; right:24px;">
                <a class="waves-effect waves-light btn btn-floating btn-large cyan tooltipped"
                   data-position="left"
                   data-delay="50"
                   data-tooltip="Exportar a CSV"
                   href="php/action/reports/export_csv_ipats_received_with_pending_state.php">
                    <i class="large material-icons">file_download</i>
                </a>
            </div>
        </div>
        <div ng-show="isAdministrator() && isIpatDeliveryStatisticShown" ng-controller="ipatTransactionStatisticController">
            <div ng-include="'views/ipat_delivery_statistic/ipat_delivery_statistic_grid.tpl.html'"></div>
        </div>
        <div ng-show="isAdministrator() && isUsersShown" ng-controller="userController">
            <div class="fixed-action-btn vertical">
                <a class="btn-floating btn-large red">
                    <i class="large material-icons">mode_edit</i>
                </a>
                <ul>
                    <li>
                        <a class="waves-effect waves-light btn modal-trigger btn-floating btn-large blue tooltipped"
                            data-position="left"
                            data-delay="50"
                            data-tooltip="Crear nuevo Usuario"
                            href="#modal-user-form"
                            ng-click="showCreateUserForm()">
                            <i class="large material-icons">add</i>
                        </a>
                    </li>
                </ul>
            </div>
            <div ng-include="'views/user/user_modal_form.tpl.html'"></div>
            <div ng-include="'views/user/user_grid.tpl.html'"></div>
        </div>
        <div ng-show="isAdministrator() && isSupportShown">
            <div ng-include="'views/support/query_manager_form.tpl.html'"></div>
        </div>
        <div ng-show="isUserChangePasswordShown">
            <div ng-include="'views/user/user_change_password_grid.tpl.html'"></div>
        </div>
    </div>
    <div ng-include="'views/shared/dir_footer.tpl.html'"></div>
    <script type="text/javascript" src="libs/js/jquery.js"></script>
    <script type="text/javascript" src="libs/js/jquery-extensions.js"></script>
    <script src="libs/css/materialize-v0.97.3/js/materialize.min.js"></script>
    <script src="libs/js/angular.min.js"></script>
    <script src="libs/js/dirPagination.js"></script>
    <script src="libs/js/common/siteHelper.js"></script>
    <script src="libs/js/common/formHelper.js"></script>
    <script src="libs/js/common/dateHelper.js"></script>
    <script src="libs/js/common/arrayHelper.js"></script>
    <script src="libs/js/common/modalHelper.js"></script>
    <script src="libs/js/common/quickSort.js"></script>
    <script src="libs/js/initApp.js"></script>
    <script src="libs/js/app.js?i=0"></script>
    <script src="libs/js/modules/user/controllers/userController.js"></script>
    <script src="libs/js/modules/ipat-delivery-statistic/controllers/ipatDeliveryStatisticController.js"></script>
    <script src="libs/js/modules/transit-ticket/controllers/transitTicketTransactionController.js"></script>
    <script>

    function leanModal() {
        $('.modal-trigger').leanModal();
    };

    function activateDropDowns() {
        $('.dropdown-button').dropdown({
            hover: true
        });
    };

    $(document).ready(function() {
        leanModal();

        activateDropDowns();

        SiteHelper.displayLoader();

    });
    </script>
</body>
</html>
