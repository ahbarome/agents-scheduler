<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../model/schedule.php');
    include_once('../../model/schedule_detail.php');

    class ScheduleBuilder
    {
        public function buildSchedule($scheduleDate, $registerDate)
        {
            $schedule = new Schedule(null);
            $schedule->scheduleDate = $scheduleDate;
            $schedule->registerDate = $registerDate;
            return $schedule;
        }

        public function buildScheduleDetail($scheduleDate, $idServiceConfiguration, $plaqueAgent)
        {
            $scheduleDetail = new ScheduleDetail();
            $scheduleDetail->scheduleDate = $scheduleDate;
            $scheduleDetail->idServiceConfiguration = $idServiceConfiguration;
            $scheduleDetail->plaqueAgent = $plaqueAgent;
            return $scheduleDetail;
        }

        public function buildScheduleDetailComplete($scheduleDate, $idServiceConfiguration, $plaqueAgent, $agentFirstName, $agentLastName, $locationName, $sectorName, $startService, $endService)
        {
            $scheduleDetail = $this->buildScheduleDetail($scheduleDate, $idServiceConfiguration, $plaqueAgent);
            $scheduleDetail->agentFirstName =  $agentFirstName;
            $scheduleDetail->agentLastName =  $agentLastName;
            $scheduleDetail->locationName =  $locationName;
            $scheduleDetail->sectorName =  $sectorName;
            $scheduleDetail->startService =  $startService;
            $scheduleDetail->endService =  $endService;
            return $scheduleDetail;
        }
    }
?>
