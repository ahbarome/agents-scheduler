<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../model/transit_ticket_transaction.php');

    /**
     * Class TransitTicketTransactionBuilder allows to build a @link TransitTicketTransaction
     * object.
     */
    class TransitTicketTransactionBuilder
    {
        private $transientTicketTransaction;
        const UNSET_INT_VALUE = -1;
        const UNSET_BOOL_VALUE = false;
        const EMPTY_STRING = "";

        public function __construct($transientTicketTransaction)
        {
            $this->transientTicketTransaction = $transientTicketTransaction;
        }

        public function build()
        {
            $transaction = new TransitTicketTransaction();
            $transaction->talonaryNumber =
                property_exists($this->transientTicketTransaction, "talonaryNumber") ?
                    $this->transientTicketTransaction->talonaryNumber
                    : TransitTicketTransactionBuilder::UNSET_INT_VALUE;
            $transaction->ticketNumber =
                property_exists($this->transientTicketTransaction, "ticketNumber") ?
                    $this->transientTicketTransaction->ticketNumber
                    : TransitTicketTransactionBuilder::UNSET_INT_VALUE;
            $transaction->diligenceDate =
                property_exists($this->transientTicketTransaction, "diligenceDate") ?
                    substr($this->transientTicketTransaction->diligenceDate, 0, 10) : null;
            $transaction->infractorCode =
                property_exists($this->transientTicketTransaction, "infractorCode") ?
                    $this->transientTicketTransaction->infractorCode
                    : TransitTicketTransactionBuilder::EMPTY_STRING;
            $transaction->plaqueAgent =
                property_exists($this->transientTicketTransaction, "plaqueAgent") ?
                    $this->transientTicketTransaction->plaqueAgent
                    : TransitTicketTrasanctionBuilder::EMPTY_STRING;
            $transaction->immobilized =
                property_exists($this->transientTicketTransaction, "immobilized") ?
                    $this->transientTicketTransaction->immobilized
                    : TransitTicketTrasanctionBuilder::UNSET_BOOL_VALUE;
            $transaction->immobilizedSubject =
                property_exists($this->transientTicketTransaction, "immobilizedSubject") ?
                    $this->transientTicketTransaction->immobilizedSubject
                    : TransitTicketTrasanctionBuilder::EMPTY_STRING;
            $transaction->immobilizedSite =
                property_exists($this->transientTicketTransaction, "immobilizedSite") ?
                    $this->transientTicketTransaction->immobilizedSite
                    : TransitTicketTrasanctionBuilder::EMPTY_STRING;
            $transaction->createdBy =
                property_exists($this->transientTicketTransaction, "createdBy") ?
                    $this->transientTicketTransaction->createdBy
                    : TransitTicketTrasanctionBuilder::EMPTY_STRING;
            $transaction->createdAt =
                    property_exists($this->transientTicketTransaction, "createdAt") ?
                        substr($this->transientTicketTransaction->createdAt, 0, 10) : null;
            $transaction->modifiedBy =
                property_exists($this->transientTicketTransaction, "modifiedBy") ?
                    $this->transientTicketTransaction->modifiedBy
                    : TransitTicketTrasanctionBuilder::EMPTY_STRING;
            $transaction->modifiedAt =
                property_exists($this->transientTicketTransaction, "modifiedAt") ?
                    substr($this->transientTicketTransaction->modifiedAt, 0, 10) : null;

            return $transaction;
        }
    }