<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../model/ipat_delivery.php');

    /**
     * Class IpatTransactionBuilder allows to build @link IpatTransaction objects.
     */
    class IpatTransactionBuilder
    {
        const PREFIX_DELIVERED_BY = "EP-";
        const PREFIX_DELIVERED_TO = "EA-";
        const PREFIX_PARTIALLY_RECEIVED_BY = "RPP-";
        const PREFIX_PARTIALLY_RECEIVED_TO = "RPA-";
        const PREFIX_RECEIVED_BY = "RP-";
        const PREFIX_RECEIVED_TO = "RA-";

        /**
         * Builds an ipat transaction base on the quantity, delivery date, delivered by, and
         * delivered to.
         *
         * @param $quantity
         * @param $deliveryDate of the transaction.
         * @param $deliveredBy agent that received the ipat.
         * @param $deliveredTo agent that delivered the ipat.
         *
         * @return IpatTransaction with the data passed as a parameter.
         */
        public function buildIpatDeliveryBasic(
            $quantity,
            $deliveryDate,
            $deliveredBy,
            $deliveredTo)
        {
            $transaction = new IpatTransaction();
            $transaction->quantity = $quantity;
            $transaction->deliveryDate = $deliveryDate;
            $transaction->deliveredBy = $deliveredBy;
            $transaction->deliveredTo = $deliveredTo;
            return $transaction;
        }

        /**
         * Builds an ipat transaction.
         *
         * @param $ipatNumber
         * @param $deliveryDate
         * @param $deliveredBy
         * @param $deliveredTo
         * @param $partiallyReceivedBy
         * @param $partiallyReceivedTo
         * @param $receivedBy
         * @param $receivedTo
         * @param $idSeverity
         * @param $idClassification
         * @param $agentDeliveredBy
         * @param $agentDeliveredTo
         * @param $idTransactionState
         * @param $transactionState
         * @param $partialReceptionDate
         * @param $receptionDate
         * @param $accidentDate
         * @param $diligenceDate
         * @param $address
         * @param $vehiclePlaque
         * @param $severity
         * @param $classification
         * @param $criminalNews
         * @param $observation
         * @param $agentPartiallyReceivedBy
         * @param $agentPartiallyReceivedTo
         * @param $agentReceivedBy
         * @param $agentReceivedTo
         * @param $quantity
         * @param $totalNovelties
         *
         * @return IpatTransaction with the data passed as a parameter.
         */
        public function buildIpatDelivery(
            $ipatNumber,
            $deliveryDate,
            $deliveredBy,
            $deliveredTo,
            $partiallyReceivedBy,
            $partiallyReceivedTo,
            $receivedBy,
            $receivedTo,
            $idSeverity,
            $idClassification,
            $agentDeliveredBy,
            $agentDeliveredTo,
            $idTransactionState,
            $transactionState,
            $partialReceptionDate,
            $receptionDate,
            $accidentDate,
            $diligenceDate,
            $address,
            $vehiclePlaque,
            $severity,
            $classification,
            $criminalNews,
            $observation,
            $agentPartiallyReceivedBy,
            $agentPartiallyReceivedTo,
            $agentReceivedBy,
            $agentReceivedTo,
            $quantity,
            $totalNovelties)
        {
            $transaction = new IpatTransaction();
            $transaction->ipatNumber = $ipatNumber;
            $transaction->deliveryDate = $deliveryDate;
            $transaction->deliveredBy = $deliveredBy;
            $transaction->deliveredTo = $deliveredTo;
            $transaction->partiallyReceivedBy = $partiallyReceivedBy;
            $transaction->partiallyReceivedTo = $partiallyReceivedTo;
            $transaction->receivedBy = $receivedBy;
            $transaction->receivedTo = $receivedTo;
            $transaction->idSeverity = $idSeverity;
            $transaction->idClassification = $idClassification;

            if ($agentDeliveredBy != null)
            {
                $agentDeliveredBy->prefix =
                    IpatTransactionBuilder::PREFIX_DELIVERED_BY . $agentDeliveredBy->plaque;
                $transaction->agentDeliveredBy = $agentDeliveredBy;
            }

            if ($agentDeliveredTo != null)
            {
                $agentDeliveredTo->prefix =
                    IpatTransactionBuilder::PREFIX_DELIVERED_TO . $agentDeliveredTo->plaque;
                $transaction->agentDeliveredTo = $agentDeliveredTo;
            }

            $transaction->idTransactionState =
                $idTransactionState != null ? intval($idTransactionState) : 0;
            $transaction->transactionState = $transactionState;
            $transaction->partialReceptionDate = $partialReceptionDate;
            $transaction->receptionDate = $receptionDate;
            $transaction->accidentDate = $accidentDate;
            $transaction->diligenceDate = $diligenceDate;
            $transaction->address = $address;
            $transaction->vehiclePlaque = $vehiclePlaque;
            $transaction->severity = $severity;
            $transaction->classification = $classification;
            $transaction->criminalNews = $criminalNews;
            $transaction->observation = $observation;

            if ($agentPartiallyReceivedBy != null)
            {
                $agentPartiallyReceivedBy->prefix =
                    IpatTransactionBuilder::PREFIX_PARTIALLY_RECEIVED_BY
                        . $agentPartiallyReceivedBy->plaque;
                $transaction->agentPartiallyReceivedBy = $agentPartiallyReceivedBy;
            }

            if($agentPartiallyReceivedTo != null)
            {
                $agentPartiallyReceivedTo->prefix =
                    IpatTransactionBuilder::PREFIX_PARTIALLY_RECEIVED_TO
                        . $agentPartiallyReceivedTo->plaque;
                $transaction->agentPartiallyReceivedTo = $agentPartiallyReceivedTo;
            }

            if ($agentReceivedBy != null)
            {
                $agentReceivedBy->prefix =
                    IpatTransactionBuilder::PREFIX_RECEIVED_BY . $agentReceivedBy->plaque;
                $transaction->agentReceivedBy = $agentReceivedBy;
            }

            if($agentReceivedTo != null)
            {
                $agentReceivedTo->prefix =
                    IpatTransactionBuilder::PREFIX_RECEIVED_TO . $agentReceivedTo->plaque;
                $transaction->agentReceivedTo = $agentReceivedTo;
            }

            $transaction->quantity = $quantity;
            $transaction->totalNovelties = $totalNovelties;

            return $transaction;
        }
    }
?>