<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../model/filter_criteria.php');

    class FilterCriteriaBuilder
    {
        private $filterData;
        const UNSET_INT_VALUE = -1;
        const EMPTY_STRING = "";

        public function __construct($filterData)
        {
            $this->filterData = $filterData->filter;
        }

        public function build()
        {
            $filter = new FilterCriteria();
            $filter->filterId =
                property_exists($this->filterData, "filterId")
                    ? $this->filterData->filterId : FilterCriteriaBuilder::UNSET_INT_VALUE;
            $filter->filterBy =
                property_exists($this->filterData, "filterBy")
                    ? $this->filterData->filterBy : FilterCriteriaBuilder::EMPTY_STRING;
            $filter->filterByAlias =
                property_exists($this->filterData, "filterByAlias")
                    ? $this->filterData->filterByAlias : FilterCriteriaBuilder::EMPTY_STRING;
            $filter->valueFilter =
                property_exists($this->filterData, "valueFilter")
                    ? $this->filterData->valueFilter : FilterCriteriaBuilder::EMPTY_STRING;
            return $filter;
        }
    }