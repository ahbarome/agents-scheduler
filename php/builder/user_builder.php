<?php

    /*
    * Copyright (c) 2018 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../model/user.php');

    /**
     * Class UserBuilder allows to build a @link User object.
     */
    class UserBuilder
    {
        private $transientUser;
        const UNSET_INT_VALUE = -1;
        const UNSET_BOOL_VALUE = false;
        const EMPTY_STRING = "";

        public function __construct($transientUser)
        {
            $this->transientUser = $transientUser;
        }

        public function build()
        {
            $user = new User();
            $user->username =
                property_exists($this->transientUser, "username") ?
                    $this->transientUser->username
                    : UserBuilder::EMPTY_STRING;
            $user->password =
                property_exists($this->transientUser, "password") ?
                    $this->transientUser->password
                    : UserBuilder::EMPTY_STRING;
            $user->idRole =
                property_exists($this->transientUser, "idRole") ?
                    $this->transientUser->idRole
                    : UserBuilder::UNSET_INT_VALUE;
            $user->plaqueAgent =
                property_exists($this->transientUser, "plaqueAgent") ?
                    $this->transientUser->plaqueAgent
                    : UserBuilder::EMPTY_STRING;
            return $user;
        }
    }