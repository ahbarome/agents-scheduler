<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../model/transit_ticket_transaction_novelty.php');

    /**
     * Class TransitTicketTransactionNoveltyBuilder allows to build a
     * @link TransitTicketTransactionNovelty object.
     */
    class TransitTicketTransactionNoveltyBuilder
    {
        private $transientNovelty;
        const UNSET_INT_VALUE = -1;
        const EMPTY_STRING = "";

        public function __construct($transientNovelty)
        {
            $this->transientNovelty = $transientNovelty;
        }

        public function build()
        {
            $novelty = new TransitTicketTransactionNovelty();
            $novelty->talonaryNumber =
                property_exists($this->transientNovelty, "talonaryNumber") ?
                    $this->transientNovelty->talonaryNumber
                    : TransitTicketTransactionNoveltyBuilder::UNSET_INT_VALUE;
            $novelty->ticketNumber =
                property_exists($this->transientNovelty, "ticketNumber") ?
                    $this->transientNovelty->ticketNumber
                    : TransitTicketTransactionNoveltyBuilder::UNSET_INT_VALUE;
            $novelty->diligenceDate =
                property_exists($this->transientNovelty, "diligenceDate") ?
                    substr($this->transientNovelty->diligenceDate, 0, 10) : null;
            $novelty->pstDeliveryDate =
                property_exists($this->transientNovelty, "pstDeliveryDate") ?
                    substr($this->transientNovelty->pstDeliveryDate, 0, 10) : null;
            $novelty->observation =
                property_exists($this->transientNovelty, "observation") ?
                    $this->transientNovelty->observation
                    : TransitTicketTransactionNoveltyBuilder::EMPTY_STRING;
            return $novelty;
        }
    }