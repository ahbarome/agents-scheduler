<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../model/transit_ticket_talonary.php');

    /**
     * Class TransitTicketTalonaryBuilder allows to build a @link TransitTicketTalonary
     * object.
     */
    class TransitTicketTalonaryBuilder
    {
        private $transientTicketTalonary;
        const UNSET_INT_VALUE = -1;
        const EMPTY_STRING = "";

        public function __construct($transientTicketTalonary)
        {
            $this->transientTicketTalonary = $transientTicketTalonary;
        }

        public function build()
        {
            $talonary = new TransitTicketTalonary();
            $talonary->talonaryNumber =
                property_exists($this->transientTicketTalonary, "talonaryNumber") ?
                    $this->transientTicketTalonary->talonaryNumber
                    : TransitTicketTalonaryBuilder::UNSET_INT_VALUE;
            $talonary->initialNumber =
                property_exists($this->transientTicketTalonary, "initialNumber") ?
                    $this->transientTicketTalonary->initialNumber
                    : TransitTicketTalonaryBuilder::UNSET_INT_VALUE;
            $talonary->finalNumber =
                property_exists($this->transientTicketTalonary, "finalNumber") ?
                    $this->transientTicketTalonary->finalNumber
                    : TransitTicketTalonaryBuilder::UNSET_INT_VALUE;
            $talonary->deliveryDate =
                property_exists($this->transientTicketTalonary, "deliveryDate") ?
                    substr($this->transientTicketTalonary->deliveryDate, 0, 10) : null;
            $talonary->assignedTo =
                property_exists($this->transientTicketTalonary, "assignedTo") ?
                    $this->transientTicketTalonary->assignedTo
                    : TransitTicketTalonaryBuilder::EMPTY_STRING;
            $talonary->createdBy =
                property_exists($this->transientTicketTalonary, "createdBy") ?
                    $this->transientTicketTalonary->createdBy
                    : TransitTicketTalonaryBuilder::EMPTY_STRING;
            $talonary->createdAt =
                    property_exists($this->transientTicketTalonary, "createdAt") ?
                        substr($this->transientTicketTalonary->createdAt, 0, 10) : null;
            $talonary->modifiedBy =
                property_exists($this->transientTicketTalonary, "modifiedBy") ?
                    $this->transientTicketTalonary->modifiedBy
                    : TransitTicketTalonaryBuilder::EMPTY_STRING;
            $talonary->modifiedAt =
                property_exists($this->transientTicketTalonary, "modifiedAt") ?
                    substr($this->transientTicketTalonary->modifiedAt, 0, 10) : null;

            return $talonary;
        }
    }