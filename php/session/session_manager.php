<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    final class SessionManager
    {
        const USERNAME_ATTRIBUTE = "USERNAME";
        const PASSWORD_ATTRIBUTE = "PASSWORD";
        const ROLE_ATTRIBUTE = "ROLE";

        public function __construct()
        {
            $this->createOrRefreshSession();
        }

        public function getUserName()
        {
            $username =
                isset($_SESSION[SessionManager::USERNAME_ATTRIBUTE])
                ? $_SESSION[SessionManager::USERNAME_ATTRIBUTE] : null;
            return $username;
        }

        public function getPassword()
        {
            $password =
                isset($_SESSION[SessionManager::PASSWORD_ATTRIBUTE])
                    ? $_SESSION[SessionManager::PASSWORD_ATTRIBUTE] : null;
            return $password;
        }

        public function createSession($username, $password)
        {
            $_SESSION[SessionManager::USERNAME_ATTRIBUTE] = $username;
            $_SESSION[SessionManager::PASSWORD_ATTRIBUTE] = $password;
        }

        private function createOrRefreshSession()
        {
            if (!isset($_SESSION))
            {
                session_start();
            }
        }

        public function closeSession()
        {
            unset($_SESSION[SessionManager::USERNAME_ATTRIBUTE]);
            unset($_SESSION[SessionManager::PASSWORD_ATTRIBUTE]);
            session_destroy();
        }

        public function addSessionAttribute($attribute, $value)
        {
            if ($this->isAllowedAttribute($attribute) && !is_null($value))
            {
                $_SESSION[$attribute] = $value;
            }
        }

        public function getSessionAttributeValue($attribute)
        {
            if ($this->isAllowedAttribute($attribute))
            {
                $value = isset($_SESSION[$attribute]) ? $_SESSION[$attribute] : null;
                return $value;
            }
        }

        public function removeSessionAttribute($attribute)
        {
            if ($this->isAllowedAttribute($attribute))
            {
                unset($_SESSION[$attribute]);
            }
        }

        private function isAllowedAttribute($attribute)
        {
            return !is_null($attribute)
                && $attribute != SessionManager::USERNAME_ATTRIBUTE
                && $attribute != SessionManager::PASSWORD_ATTRIBUTE;
        }
    }
?>