<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('session_manager.php');

    class SessionValidator
    {
        public function existSession()
        {
            $this->createOrRefreshSession();
            $existUsername = isset($_SESSION[SessionManager::USERNAME_ATTRIBUTE]);
            $existPassword = isset($_SESSION[SessionManager::PASSWORD_ATTRIBUTE]);
            return $existUsername && $existPassword;
        }

        public function createOrRefreshSession()
        {
            if (!isset($_SESSION))
            {
                session_start();
            }
        }
    }
?>