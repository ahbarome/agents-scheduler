﻿<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../common/headers.php');
    include_once('../../database/database.php');
    include_once('../../model/inactivity.php');
    include_once('../session/session_init.php');

    if ($SESSION_EXIST)
    {
        $database = new Database();
        $db = $database->getConnection();

        $inactivity = new Inactivity($db);

        $statement = $inactivity->readAll();
        $totalInactivities = $statement->rowCount();

        if ($totalInactivities > 0)
        {
            $data="";
            $counterInactivities=1;

            while ($row = $statement->fetch(PDO::FETCH_ASSOC))
            {
                extract($row);

                $data .= '{';
                    $data .= '"id":"'  . $id . '",';
                    $data .= '"description":"'   . $description . '",';
                    $data .= '"startDate":"'   . $startDate . '",';
                    $data .= '"endDate":"' . $endDate . '",';
                    $data .= '"plaqueAgent":"' . $plaqueAgent . '",';
                    $data .= '"agentFirstName":"' . $agentFirstName . '",';
                    $data .= '"agentLastName":"' . $agentLastName . '",';
                    $data .= '"sectionName":"' . $sectionName . '"';
                $data .= '}';

                $data .= $counterInactivities < $totalInactivities ? ',' : '';

                $counterInactivities++;
            }

            echo '{"records":[' . $data . ']}';
        }
        else
        {
            echo '{"records":[{}]}';
        }
    }
?>
