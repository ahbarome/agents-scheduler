﻿<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../common/headers.php');
    include_once('../../database/database.php');
    include_once('../../model/inactivity.php');
    include_once('../session/session_init.php');

    if ($SESSION_EXIST)
    {
        $database = new Database();
        $db = $database->getConnection();

        $inactivity = new Inactivity($db);

        $data = json_decode(file_get_contents("php://input"));

        $inactivity->id = $data->id;

        $inactivity->readOne();

        $data = array(
            "id" => $inactivity->id,
            "description" => $inactivity->description,
            "startDate" => $inactivity->startDate,
            "endDate" => $inactivity->endDate,
            "plaqueAgent" => $inactivity->plaqueAgent,
            "agentFirstName" => $inactivity->agentFirstName,
            "agentLastName" => $inactivity->agentLastName,
            "sectionName" => $inactivity->sectionName);

        print_r(json_encode($data));
    }
?>
