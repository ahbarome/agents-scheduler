<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../database/database.php');
    include_once('../../model/inactivity.php');
    include_once('../session/session_init.php');

    if ($SESSION_EXIST)
    {
        $database = new Database();
        $db = $database->getConnection();

        $inactivity = new Inactivity($db);

        header("Content-type: text/x-csv; charset=UTF-8");
        header("Content-Disposition: attachment; filename=_inactividades" . date('Y-m-d_H-i-s') . ".csv");
        echo $inactivity->export_CSV();
    }
?>
