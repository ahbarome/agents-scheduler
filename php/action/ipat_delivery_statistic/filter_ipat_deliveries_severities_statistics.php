<?php

    /*
    * Copyright (c) 2018 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ('Confidential Information').
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../common/headers.php');
    include_once('../../common/json_response_collection.php');
    include_once('../../business_layer/ipat_delivery_statistic_business_layer.php');
    include_once('../session/session_init.php');

    if ($SESSION_EXIST)
    {
        $receivedData = json_decode(file_get_contents('php://input'));
        if (is_null($receivedData) || !property_exists($receivedData, 'filter'))
        {
            return;
        }

        $startDate =
            property_exists($receivedData->filter, 'startAccidentDate') ?
                new DateTime(
                    $receivedData->filter->startAccidentDate,
                    new DateTimeZone(AmericaDateTime::TIME_ZONE)) : null;
        $endDate =
            property_exists($receivedData->filter, 'endAccidentDate') ?
                new DateTime(
                    $receivedData->filter->endAccidentDate,
                    new DateTimeZone(AmericaDateTime::TIME_ZONE)) : null;

        $businessLayer = new IpatTransactionStatisticBusinessLayer();
        $deliveries =
            $businessLayer->readAll(
                date($startDate->format(AmericaDateTime::DEFAULT_DATE_FORMAT)),
                date($endDate->format(AmericaDateTime::DEFAULT_DATE_FORMAT)));

        echo JsonResponseCollection::encodeCollectionCompressed($deliveries);
    }
?>