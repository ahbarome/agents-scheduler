<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../model/schedule.php');
    include_once('../../business_layer/schedule_business_layer.php');
    include_once('../session/session_init.php');

    if ($SESSION_EXIST)
    {
        $data = json_decode(file_get_contents("php://input"));

        $schedule = new Schedule();
        $schedule->scheduleDate = date($data->scheduleDate);
        $schedule->registerDate = date("y-m-d");

        $scheduleBusinessLayer = new ScheduleBusinessLayer();

        if($scheduleBusinessLayer->schedule($schedule))
        {
            echo "La programación se creo satisfactoriamente.";
        }
        else
        {
            echo "No fue posible crear la programación.";
        }
    }
?>