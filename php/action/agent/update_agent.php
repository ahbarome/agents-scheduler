<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../business_layer/agent_business_layer.php');
    include_once('../../model/agent.php');
    include_once('../session/session_init.php');

    if ($SESSION_EXIST)
    {
        $decodedAgent = json_decode(file_get_contents("php://input"));

        $agent = new Agent();
        $agent->plaque =
            property_exists($decodedAgent, "plaque") ? $decodedAgent->plaque : 0;
        $agent->firstName =
            property_exists($decodedAgent, "firstName") ? $decodedAgent->firstName : "";
        $agent->lastName =
            property_exists($decodedAgent, "lastName") ? $decodedAgent->lastName : "";
        $agent->idSection =
            property_exists($decodedAgent, "idSection") ? $decodedAgent->idSection : 0;
        $agent->isActive =
            property_exists($decodedAgent, "isActive") ? $decodedAgent->isActive : false;

        $agentBL = new AgentBusinessLayer();
        $updated = $agentBL->update($agent);
        if ($updated)
        {
            echo "El agente se actualizó satisfactoriamente.";
        }
        else
        {
            echo "No fue posible realizar la actualización del agente.";
        }
    }
