<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../business_layer/agent_business_layer.php');
    include_once('../session/session_init.php');

    if ($SESSION_EXIST)
    {
        $decodedAgent = json_decode(file_get_contents("php://input"));

        $agentBL = new AgentBusinessLayer();
        $deleted = $agentBL->deleteSelected($decodedAgent->ids);
        if ($deleted)
        {
            echo "Se eliminaron los agentes de manera satisfactoria.";
        }
        else
        {
            echo "No fue posible eliminar el o los agentes seleccionados.";
        }
    }
?>