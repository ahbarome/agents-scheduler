<?php

    /*
    * Copyright (c) 2018 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../builder/user_builder.php');
    include_once('../../business_layer/user_business_layer.php');
    include_once('../session/session_init.php');

    if ($SESSION_EXIST)
    {
        try
        {
            $data = json_decode(file_get_contents("php://input"));
            $userReceived =
                property_exists($data, "user") ?
                    $data->user : null;
    
            if ($userReceived != null)
            {
                $builder = new UserBuilder($userReceived);
                $user = $builder->build();

                $businessLayer = new UserBusinessLayer();
                $saved = $businessLayer->save($user);
    
                if ($saved)
                {
                    echo "El usuario se creo satisfactoriamente.";
                }
                else
                {
                    echo "No fue posible realizar la creación del usuario.";
                }
            }
            else
            {
                echo "No se recibió información del usuario.";
            }
            
        }
        catch(Exception $exception)
        {
            echo "No fue posible realizar la creación del usuario debido a una excepción.";
        }
    }
