﻿<?php
    include_once('../../business_layer/user_business_layer.php');
    include_once('../../common/json_response_collection.php');
    include_once('../../model/user.php');
    include_once('../../session/session_manager.php');
    include_once('../session/session_init.php');

    if ($SESSION_EXIST)
    {
        $sessionManager = new SessionManager();
        $username = $sessionManager->getUserName();

        $userBusinessLayer = new UserBusinessLayer();
        $user = $userBusinessLayer->readOneByUsername($username);

        $encoder = new JsonResponseCollection();
        echo $encoder->encodeCollection($user);
    }
?>
