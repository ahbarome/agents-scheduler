﻿<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../business_layer/user_business_layer.php');
    include_once('../../model/user.php');
    include_once('../../session/session_manager.php');
    include_once('../session/session_init.php');

    if ($SESSION_EXIST)
    {
        $sessionManager = new SessionManager();
        $username = $sessionManager->getUserName();
        $password = $sessionManager->getPassword();

        $data = json_decode(file_get_contents("php://input"));

        $user = new User();
        $user->username = $username;
        $user->password =
            property_exists($data, "password") ? $data->password : null;
        $user->newPassword =
            property_exists($data, "newPassword") ? $data->newPassword : null;

        $userBusinessLayer = new UserBusinessLayer();
        $wasChanged = $userBusinessLayer->changePassword($username, $password, $user);

        if ($wasChanged)
        {
            MessagePrinter::printMessage('Se actualizó la contraseña de manera satisfactoria');
        }
    }
?>
