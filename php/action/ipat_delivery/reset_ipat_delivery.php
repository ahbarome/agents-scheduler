<?php

    /*
    * Copyright (c) 2018 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../business_layer/ipat_delivery_business_layer.php');
    include_once('../../common/message_printer.php');
    include_once('../session/session_init.php');

    if ($SESSION_EXIST)
    {
        if(!isset($_GET["ipatNumber"]))
        {
            return;
        }

        $ipatNumber = $_GET["ipatNumber"];

        $ipatDeliveryBL = new IpatDeliveryBusinessLayer();
        $reseted = $ipatDeliveryBL->reset($ipatNumber);
        if ($reseted)
        {
            MessagePrinter::printMessage('Se reseteo satisfactoriamente el IPAT.');
        }
    }
?>