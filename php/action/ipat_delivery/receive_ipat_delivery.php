<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../common/constants.php');
    include_once('../../common/message_printer.php');
    include_once('../../model/ipat_delivery.php');
    include_once('../../business_layer/ipat_delivery_business_layer.php');
    include_once('../../business_layer/ipat_incharge_business_layer.php');
    include_once('../../session/session_manager.php');
    include_once('../session/session_init.php');

    if ($SESSION_EXIST)
    {
        $data = json_decode(file_get_contents("php://input"));
        $transaction = new IpatTransaction();

        $sessionManager = new SessionManager();
        $username = $sessionManager->getUserName();

        $ipatInChargeBusinessLayer = new IpatInChargeBusinessLayer();
        $inCharge = $ipatInChargeBusinessLayer->readOne($username);

        if ($inCharge != null)
        {
            $transaction->ipatNumber =
                property_exists($data, "ipatNumber") ? intval($data->ipatNumber) : 0;
            $transaction->idTransactionState =
                property_exists($data, "idTransactionState") ? intval($data->idTransactionState) : 0;
            $transaction->idSeverity =
                property_exists($data, "idSeverity") ? intval($data->idSeverity) : null;
            $idClassification =
                property_exists($data, "idClassification") ? intval($data->idClassification) : null;
            $transaction->idClassification = $idClassification == 0 ? null : $idClassification;
            $transaction->accidentDate =
                property_exists($data, "accidentDate") ? $data->accidentDate : null;
            $transaction->diligenceDate =
                property_exists($data, "diligenceDate") ? $data->diligenceDate : null;
            $transaction->address =
                property_exists($data, "address") ? $data->address : null;
            $transaction->vehiclePlaque =
                property_exists($data, "vehiclePlaque") ? $data->vehiclePlaque : null;
            $transaction->receivedTo =
                property_exists($data, "receivedTo") ? $data->receivedTo : null;
            $transaction->receivedBy = $inCharge->plaqueAgent;
            $transaction->partiallyReceivedTo =
                property_exists($data, "partiallyReceivedTo") ? $data->partiallyReceivedTo : null;
            $transaction->partiallyReceivedBy = $inCharge->plaqueAgent;

            $transaction->criminalNews =
                property_exists($data, "criminalNews") ? $data->criminalNews : null;
            $transaction->observation =
                property_exists($data, "observation") ? $data->observation : null;
            $transaction->deliveredBy =
                property_exists($data, "deliveredBy") ? $data->deliveredBy : null;
            $transaction->deliveredTo =
                property_exists($data, "deliveredTo") ? $data->deliveredTo : null;

            $ipatDeliveryBusinessLayer = new IpatDeliveryBusinessLayer();
            $received = $ipatDeliveryBusinessLayer->receiveIpat($transaction);

            if ($received)
            {
                MessagePrinter::printMessage(
                    'Se recibió satisfactoriamente el IPAT seleccionado.');
            }
            else
            {
                MessagePrinter::printMessage(
                    'No fue posible realizar la recepción del IPAT seleccionado.');
            }
        }
        else
        {
            MessagePrinter::printMessage(
                'El agente que gestiono el IPAT, no esta habilitado, por favor comuniquese con'
                    .' el administrador del sistema.');
        }
    }
?>