<?php
    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../common/america_datetime.php');
    include_once('../../common/constants.php');
    include_once('../../business_layer/ipat_delivery_business_layer.php');
    include_once('../../session/session_manager.php');
    include_once('../session/session_init.php');

    if ($SESSION_EXIST)
    { 
        $data = json_decode(file_get_contents("php://input"));
        $exportDate =
            property_exists($data, "exportMinistryDate")
                ? new DateTime(
                    $data->exportMinistryDate
                    , new DateTimeZone(AmericaDateTime::TIME_ZONE))
                : new DateTime(
                    AmericaDateTime::CURRENT_DATETIME,
                    new DateTimeZone(AmericaDateTime::TIME_ZONE));

        $businessLayer = new IpatDeliveryBusinessLayer();
        $content = $businessLayer->exportCSVMinistry($exportDate);

        $sessionManager = new SessionManager();
        $sessionManager->addSessionAttribute($CSV_MINISTRY_FILE, $content);
        echo true;
        return;
    }
    echo false;
?>