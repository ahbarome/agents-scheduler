<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ('Confidential Information').
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../common/headers.php');
    include_once('../../common/json_response_collection.php');
    include_once('../../business_layer/ipat_delivery_business_layer.php');
    include_once('../session/session_init.php');

    if ($SESSION_EXIST)
    {
        $data = json_decode(file_get_contents('php://input'));
        $deliveredTo = property_exists($data, 'deliveredTo') ? $data->deliveredTo : null;

        $ipatDeliveryBusinessLayer = new IpatDeliveryBusinessLayer();
        $statistics = $ipatDeliveryBusinessLayer->readAllStatistics($deliveredTo);

        echo JsonResponseCollection::encodeCollectionCompressed($statistics);
    }
?>
