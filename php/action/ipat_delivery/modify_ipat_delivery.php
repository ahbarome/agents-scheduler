<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../business_layer/ipat_delivery_business_layer.php');
    include_once('../../common/message_printer.php');
    include_once('../../model/ipat_delivery.php');
    include_once('../../model/ipat_transaction_state.php');
    include_once('../session/session_init.php');

    if ($SESSION_EXIST)
    {
        $transaction = new IpatTransaction();
        $data = json_decode(file_get_contents("php://input"));

        if(!property_exists($data, "transaction"))
        {
            MessagePrinter::printMessage('No se recibió información a actualizar.');
            return;
        }

        $transientTransaction = $data->transaction;

        $transaction->ipatNumber =
            property_exists($transientTransaction, "ipatNumber") ?
                intval($transientTransaction->ipatNumber) : 0;

        $receivedTo =
            property_exists($transientTransaction, "receivedTo") ?
                $transientTransaction->receivedTo : null;
        $transaction->receivedTo = $receivedTo != '' ? $receivedTo : null;

        $receivedBy =
            property_exists($transientTransaction, "receivedBy") ?
                $transientTransaction->receivedBy : null;
        $transaction->receivedBy = $receivedBy != '' ? $receivedBy : null;

        $partiallyReceivedTo =
            property_exists($transientTransaction, "partiallyReceivedTo") ?
                $transientTransaction->partiallyReceivedTo : null;
        $transaction->partiallyReceivedTo =
            $partiallyReceivedTo != '' ? $partiallyReceivedTo : null;

        $partiallyReceivedBy =
            property_exists($transientTransaction, "partiallyReceivedBy") ?
                $transientTransaction->partiallyReceivedBy : null;
        $transaction->partiallyReceivedBy =
            $partiallyReceivedBy != '' ? $partiallyReceivedBy : null;

        $transaction->deliveredBy =
            property_exists($transientTransaction, "deliveredBy") ?
                $transientTransaction->deliveredBy : null;
        $transaction->deliveredTo =
            property_exists($transientTransaction, "deliveredTo") ?
                $transientTransaction->deliveredTo : null;

        $transaction->deliveryDate =
            property_exists($transientTransaction, "deliveryDate") ?
                $transientTransaction->deliveryDate : null;
        $transaction->partialReceptionDate =
            property_exists($transientTransaction, "partialReceptionDate") ?
                $transientTransaction->partialReceptionDate : null;
        $transaction->receptionDate =
            property_exists($transientTransaction, "receptionDate") ?
                $transientTransaction->receptionDate : null;

        $transaction->idTransactionState =
            property_exists($transientTransaction, "idTransactionState") ?
                intval($transientTransaction->idTransactionState)
                : IpatTransactionState::DELIVERED;

        $idSeverity =
            property_exists($transientTransaction, "idSeverity") ?
                intval($transientTransaction->idSeverity) : null;
        $transaction->idSeverity = $idSeverity == 0 ? null : $idSeverity;

        $idClassification =
            property_exists($transientTransaction, "idClassification") ?
                intval($transientTransaction->idClassification) : null;

        $transaction->idClassification = $idClassification == 0 ? null : $idClassification;

        $transaction->accidentDate =
            property_exists($transientTransaction, "accidentDate") ?
                substr($transientTransaction->accidentDate, 0, 10) : null;
        $transaction->diligenceDate =
            property_exists($transientTransaction, "diligenceDate") ?
                substr($transientTransaction->diligenceDate, 0, 10) : null;
        $transaction->address =
            property_exists($transientTransaction, "address") ?
                $transientTransaction->address : null;
        $transaction->vehiclePlaque =
            property_exists($transientTransaction, "vehiclePlaque") ?
                $transientTransaction->vehiclePlaque : null;

        $transaction->criminalNews =
            property_exists($transientTransaction, "criminalNews") ?
                $transientTransaction->criminalNews : null;
        $transaction->observation =
            property_exists($transientTransaction, "observation") ?
                $transientTransaction->observation : null;

        $ipatDeliveryBL = new IpatDeliveryBusinessLayer();
        $modified = $ipatDeliveryBL->modify($transaction);
        if ($modified)
        {
            MessagePrinter::printMessage('Se actualizó satisfactoriamente el IPAT seleccionado.');
        }
        else
        {
            MessagePrinter::printMessage('No fue posible actualizar el IPAT seleccionado.');
        }
    }
?>