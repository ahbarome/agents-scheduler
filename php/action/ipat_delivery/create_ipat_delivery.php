﻿<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../common/america_datetime.php');
    include_once('../../common/constants.php');
    include_once('../../common/message_printer.php');
    include_once('../../builder/ipat_delivery_builder.php');
    include_once('../../business_layer/ipat_delivery_business_layer.php');
    include_once('../../business_layer/ipat_incharge_business_layer.php');
    include_once('../../session/session_manager.php');
    include_once('../session/session_init.php');

    if ($SESSION_EXIST)
    {
        $data = json_decode(file_get_contents("php://input"));

        $builder = new IpatTransactionBuilder();
        
        
        $americaDateTime = new AmericaDateTime();
        $currentDateTime = $americaDateTime->getCurrentDateTime();

        $sessionManager = new SessionManager();
        $username = $sessionManager->getUserName();

        $ipatInChargeBusinessLayer = new IpatInChargeBusinessLayer();
        $inCharge = $ipatInChargeBusinessLayer->readOne($username);

        if ($inCharge != null)
        {
            $ipatDelivery =
                $builder->buildIpatDeliveryBasic(
                    $data->quantity,
                    $currentDateTime->format(AmericaDateTime::DEFAULT_DATETIME_FORMAT),
                    $inCharge->plaqueAgent,
                    $data->deliveredTo);

            $ipatDeliveryBusinessLayer = new IpatDeliveryBusinessLayer();
            $ipatDeliveryBusinessLayer->deliverIpat($ipatDelivery);
        }
        else
        {
            MessagePrinter::printMessage(
                'El agente que gestiono el IPAT, no esta habilitado, por favor comuniquese con'
                    .' el administrador del sistema.');
        }
    }
?>