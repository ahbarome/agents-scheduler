<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../builder/ipat_transaction_novelty_builder.php');
    include_once('../../business_layer/ipat_delivery_business_layer.php');
    include_once('../session/session_init.php');

    if ($SESSION_EXIST)
    {
        $data = json_decode(file_get_contents("php://input"));
        $receivedNovelty =
            property_exists($data, "ipatTransactionNovelty")
                ? $data->ipatTransactionNovelty : null;

        if ($receivedNovelty != null)
        {
            $noveltyBuilder = new IpatTransactionNoveltyBuilder($receivedNovelty);
            $businessLayer = new IpatDeliveryBusinessLayer();
            $novelty = $noveltyBuilder->build();
            $added = $businessLayer->addNovelty($novelty);

            if ($added)
            {
                echo "Se adicionó satisfactoriamente la novedad.";
            }
            else
            {
                echo "No fue posible adicionar la novedad.";
            }
        }
        else
        {
            echo "No se recibió información de la novedad.";
        }
    }
?>
