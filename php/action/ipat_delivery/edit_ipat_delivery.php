﻿<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../common/constants.php');
    include_once('../../common/message_printer.php');
    include_once('../../model/ipat_delivery.php');
    include_once('../../business_layer/ipat_delivery_business_layer.php');
    include_once('../../business_layer/ipat_incharge_business_layer.php');
    include_once('../../session/session_manager.php');
    include_once('../session/session_init.php');

    if ($SESSION_EXIST)
    {
        $data = json_decode(file_get_contents("php://input"));

        $businessLayer = new IpatDeliveryBusinessLayer();
        $transaction = new IpatTransaction();

        $sessionManager = new SessionManager();
        $username = $sessionManager->getUserName();

        $transaction->quantity =
            property_exists($data, "quantity") ? $data->quantity : 0;
        $transaction->ipatNumber =
            property_exists($data, "ipatNumber") ? $data->ipatNumber : 0;
        $transaction->deliveredTo =
            property_exists($data, "deliveredTo") ? $data->deliveredTo : null;
        $transaction->idTransactionState =
            property_exists($data, "idTransactionState") ? intval($data->idTransactionState) : 0;

        $transaction->modifiedBy = $username;

        $ipatInChargeBusinessLayer = new IpatInChargeBusinessLayer();
        $inCharge = $ipatInChargeBusinessLayer->readOne($username);

        if ($inCharge != null)
        {
            $transaction->deliveredBy = $inCharge->plaqueAgent;

            $updated = $businessLayer->editIpat($transaction);

            if ($updated)
            {
                MessagePrinter::printMessage('Se actualizó satisfactoriamente el IPAT seleccionado.');
            }
            else
            {
                MessagePrinter::printMessage('No fué posible realizar la actualización del IPAT seleccionado.');
            }
        }
        else
        {
            MessagePrinter::printMessage(
                'El agente que gestiono el IPAT, no esta habilitado, por favor comuniquese con'
                    .' el administrador del sistema.');
        }
    }
?>