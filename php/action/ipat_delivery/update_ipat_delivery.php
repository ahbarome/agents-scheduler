<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../business_layer/ipat_delivery_business_layer.php');
    include_once('../../common/message_printer.php');
    include_once('../session/session_init.php');

    if ($SESSION_EXIST)
    {
        $data = json_decode(file_get_contents("php://input"));

        $ipatDeliveryBL = new IpatDeliveryBusinessLayer();
        $updated = $ipatDeliveryBL->receiveIpat($data->ipatNumber, $data->idTransactionState);
        if ($updated)
        {
            MessagePrinter::printMessage('Se recibió satisfactoriamente el IPAT seleccionado.');
        }
    }
?>