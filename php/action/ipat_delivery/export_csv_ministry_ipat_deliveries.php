<?php
    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../common/constants.php');
    include_once('../../common/csv_exporter_header.php');
    include_once('../../session/session_manager.php');
    include_once('../session/session_init.php');

    if ($SESSION_EXIST)
    {
        $sessionManager = new SessionManager();
        $content = $sessionManager->getSessionAttributeValue($CSV_MINISTRY_FILE);
        echo $content;
    }
?>