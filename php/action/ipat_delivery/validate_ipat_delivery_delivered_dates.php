<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../common/headers.php');
    include_once('../../common/json_response_collection.php');
    include_once('../../model/ipat_delivery.php');
    include_once('../../business_layer/ipat_delivery_business_layer.php');
    include_once('../session/session_init.php');

    if ($SESSION_EXIST)
    {
        $decodedTransaction = json_decode(file_get_contents("php://input"));
        $transaction = new IpatTransaction();

        $transaction->deliveredTo =
            property_exists($decodedTransaction, "deliveredTo")
                ? $decodedTransaction->deliveredTo : null;

        $encoder = new JsonResponseCollection();

        $ipatDeliveryBusinessLayer = new IpatDeliveryBusinessLayer();
        $isValid = $ipatDeliveryBusinessLayer->isValidDeliveredDate($transaction);
        echo JsonResponseCollection::encodeCollectionCompressed($isValid);
    }
?>