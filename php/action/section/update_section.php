<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../business_layer/section_business_layer.php');
    include_once('../../model/section.php');
    include_once('../session/session_init.php');

    if ($SESSION_EXIST)
    {
        $data = json_decode(file_get_contents("php://input"));
        $section = new Section();
        $section->id = $data->id;
        $section->name = $data->name;

        $sectionBL = new SectionBusinessLayer();
        $updated = $sectionBL->update($section);

        if($updated)
        {
            echo "La sección se actualizó satisfactoriamente.";
        }
        else
        {
            echo "No fue posible realizar la actualización de la sección.";
        }
    }
