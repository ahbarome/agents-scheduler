<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../business_layer/user_business_layer.php');
    include_once('../../session/session_manager.php');
    include_once('session_init.php');

    if (!$SESSION_EXIST)
    {
        $username = isset($_POST['username']) ? $_POST['username'] : null;
        $password = isset($_POST['password']) ? $_POST['password'] : null;

        $userBusinessLayer = new UserBusinessLayer();
        $existUser = $userBusinessLayer->exist($username, $password);

        if ($existUser)
        {
            $sessionManager = new SessionManager();
            $sessionManager->createSession($username, $password);

            $sessionBL->refresh();
        }
    }

    header("location:/agents-scheduler/index.php");
?>