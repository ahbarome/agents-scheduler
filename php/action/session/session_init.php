<?php

    /*
    * Copyright (c) 2018 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../session/session_validator.php');
    include_once('../../business_layer/session_business_layer.php');
    
    $SESSION_EXPIRED = 'Su sesión expiró';

    $validator = new SessionValidator();
    $sessionBL = new SessionBusinessLayer();
    $SESSION_EXIST = $validator->existSession();
    if (!$SESSION_EXIST)
    {
        print_r($SESSION_EXPIRED);
    }
    else
    {
        if (!$sessionBL->exist())
        {
            $SESSION_EXIST = false;
            print_r($SESSION_EXPIRED);
        }
        else
        {
            $sessionBL->refresh();
        }
    }
?>