<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../business_layer/transit_ticket_transaction_business_layer.php');
    include_once('../../builder/transit_ticket_transaction_builder.php');
    include_once('../../session/session_manager.php');
    include_once('../session/session_init.php');

    if ($SESSION_EXIST)
    {
        try
        {
            $data = json_decode(file_get_contents("php://input"));
            $transctionReceived =
                property_exists($data, "transitTicketTransaction") ?
                    $data->transitTicketTransaction : null;
    
            if ($transctionReceived != null)
            {
                $sessionManager = new SessionManager();
                $builder = new TransitTicketTransactionBuilder($transctionReceived);
                $businessLayer = new TransitTicketTransactionBusinessLayer();
                $talonary = $builder->build();
                $saved = $businessLayer->receive($talonary);
    
                if ($saved)
                {
                    echo "El comparendo se recibió satisfactoriamente.";
                }
                else
                {
                    echo "No fue posible realizar la recepción del comparendo.";
                }
            }
            else
            {
                echo "No se recibió información del comparendo.";
            }
            
        }
        catch(Exception $exception)
        {
            echo "No fue posible realizar la recepción del comparendo debido a una excepción.";
        }
    }
?>