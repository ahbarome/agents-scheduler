<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('base_dao.php');
    include_once('../../model/transit_ticket_transaction_state.php');

    class TransitTicketTransactionStateDao extends BaseDao
    {
        public function __construct()
        {
            parent::__construct();
        }

        public function readAll()
        {
            $query = "SELECT * FROM transitTicketTransactionState";
            $statement = $this->connection->prepare($query);
            $statement->execute();
            return $this->getTransitTicketTransactionStates($statement);
        }

        public function readOne($id)
        {
            $query = "SELECT *
                        FROM transitTicketTransactionState
                        WHERE id =:id
                        LIMIT 0,1";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":id", $id);
            $statement->execute();
            $transactions = $this->getTransitTicketTransactionStates($statement);
            return $this->getFirstResult($transactions);
        }

        public function readAllDifferentTo($id)
        {
            $query = "SELECT * FROM transitTicketTransactionState WHERE id <> :id";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":id", $id);
            $statement->execute();
            return $this->getTransitTicketTransactionStates($statement);
        }

        private function getTransitTicketTransactionStates($statement)
        {
            $totalRows = $statement->rowCount();
            $states = array();
            if ($totalRows > 0)
            {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC))
                {
                    extract($row);
                    $state = new TransitTicketTransactionState();
                    $state->id = intval($id);
                    $state->name = $name;
                    array_push($states, $state);
                }
            }
            return $states;
        }
    }
?>