<?php
    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('base_dao.php');
    include_once('../../model/role.php');

    /***
     * Class RoleDao allows to perform queries in the role table
     */
    class RoleDao extends BaseDao
    {
        /**
         * RoleDao constructor.
         */
        public function __construct()
        {
            parent::__construct();
        }

        /**
         * Read all the roles
         *
         * @return array with the roles
         */
        public function readAll()
        {
            $query = "SELECT * FROM role";
            $statement = $this->connection->prepare($query);
            $this->execute($statement);
            return $this->getRoles($statement);
        }

        /**
         * Read a role for a specific id.
         *
         * @param $id logged of the current role.
         * @return role with the id and name
         */
        public function readOne($id)
        {
            $query = "SELECT * FROM role WHERE id =:id";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":id", $id);
            $this->execute($statement);
            $roles = $this->getRoles($statement);
            return $this->getFirstResult($roles);
        }

        /**
         * Generate an array with all the roles in the current statement.
         *
         * @param $statement with the query to be executed.
         * @return array with all the roles.
         */
        private function getRoles($statement)
        {
            $totalRows = $statement->rowCount();
            $roles = array();
            if ($totalRows > 0)
            {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC))
                {
                    extract($row);
                    $role = new Role();
                    $role->id = intval($id);
                    $role->roleName = $roleName;
                    array_push($roles, $role);
                }
            }
            return $roles;
        }
    }