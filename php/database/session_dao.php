<?php

    /*
    * Copyright (c) 2018 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('base_dao.php');
    include_once('../../model/session.php');

    final class SessionDao extends BaseDao
    {
        public function __construct()
        {
            parent::__construct();
        }

        public function save($session)
        {
            $query =
                "INSERT INTO session"
                    . " SET"
                    . " id = :id,"
                    . " idUser = :idUser,"
                    . " createdAt = :createdAt,"
                    . " sessionTimeout = :sessionTimeout";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":id", $session->id);
            $statement->bindParam(":idUser", $session->idUser);
            $statement->bindParam(":createdAt", $session->createdAt);
            $statement->bindParam(":sessionTimeout", $session->sessionTimeout);
            $saved = $this->execute($statement);
            return $saved;
        }

        public function update($idUser, $lastAccessedAt)
        {
            $query =
                "UPDATE session"
                    . " SET"
                    . " lastAccessedAt = :lastAccessedAt"
                    . " WHERE idUser = :idUser";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":lastAccessedAt", $lastAccessedAt);
            $statement->bindParam(":idUser", $idUser);
            $updated = $this->execute($statement);
            return $updated;
        }

        public function readAll()
        {
            $query = "SELECT * FROM session";
            $statement = $this->connection->prepare($query);
            $this->execute($statement);
            $sessions = $this->getSessions($statement);
            return $sessions;
        }

        public function readOneByUserId($idUser)
        {
            $query = "SELECT * FROM session WHERE idUser =:idUser";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":idUser", $idUser);
            $this->execute($statement);
            $sessions = $this->getSessions($statement);
            return $this->getFirstResult($sessions);
        }

        private function getSessions($statement)
        {
            $sessions = array();
            if ($statement->rowCount() > 0)
            {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC))
                {
                    extract($row);
                    $session = new Session();
                    $session->id = $id;
                    $session->idUser = $idUser;
                    $session->createdAt = $createdAt;
                    $session->lastAccessedAt = $lastAccessedAt;
                    $session->sessionTimeout = $sessionTimeout;
                    array_push($sessions, $session);
                }
            }
            return $sessions;
        }
    }
?>