<?php

    /*
    * Copyright (c) 2018 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('base_dao.php');
    include_once('../../model/module.php');

    final class ModuleDao extends BaseDao
    {
        public function __construct()
        {
            parent::__construct();
        }

        public function readOne($id)
        {
            $query = "SELECT * FROM module WHERE id =:id";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":id", $id);
            $this->execute($statement);
            $modules = $this->getModules($statement);
            return $this->getFirstResult($modules);
        }

        private function getModules($statement)
        {
            $modules = array();
            if ($statement->rowCount() > 0)
            {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC))
                {
                    extract($row);
                    $module = new Module();
                    $module->id = $id;
                    $module->name = $name;
                    $module->description = $description;
                    $session->createdAt = $createdAt;
                    $session->createdBy = $createdBy;
                    $session->modifiedAt = $modifiedAt;
                    $session->modifiedBy = $modifiedBy;
                    array_push($modules, $module);
                }
            }
            return $sessions;
        }
    }
?>