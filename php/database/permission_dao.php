<?php

    /*
    * Copyright (c) 2018 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('base_dao.php');
    include_once('../../model/permission.php');

    final class PermissionDao extends BaseDao
    {
        public function __construct()
        {
            parent::__construct();
        }

        public function readOne($id)
        {
            $query = "SELECT * FROM permission WHERE id =:id";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":id", $id);
            $this->execute($statement);
            $permissions = $this->getPermissions($statement);
            return $this->getFirstResult($permissions);
        }

        private function getPermissions($statement)
        {
            $permissions = array();
            if ($statement->rowCount() > 0)
            {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC))
                {
                    extract($row);
                    $permission = new Permission();
                    $permission->id = $id;
                    $permission->idRole = $idRole;
                    $permission->idModule = $idModule;
                    $permission->name = $name;
                    $permission->description = $description;
                    $permission->createdAt = $createdAt;
                    $permission->createdBy = $createdBy;
                    $permission->modifiedAt = $modifiedAt;
                    $permission->modifiedBy = $modifiedBy;
                    array_push($permissions, $permission);
                }
            }
            return $sessions;
        }
    }
?>