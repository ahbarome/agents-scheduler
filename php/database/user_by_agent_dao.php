<?php

    /*
    * Copyright (c) 2018 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('base_dao.php');
    include_once('../../model/user_by_agent.php');

    /**
     * Class UserByAgentDao manage all the methods to work with the user by agent table.
     */
    class UserByAgentDao extends BaseDao
    {
        /**
         * UserByAgentDao constructor.
         */
        public function __construct()
        {
            parent::__construct();
        }

        public function save($userByAgent)
        {
            $query =
                "INSERT INTO userByAgent"
                    . " SET"
                    . " idUser = :idUser,"
                    . " plaqueAgent = :plaqueAgent";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":idUser", $userByAgent->idUser);
            $statement->bindParam(":plaqueAgent", $userByAgent->plaqueAgent);
            $saved = $statement->execute();
            return $saved;
        }

        public function update($userByAgent)
        {
            $query =
                "UPDATE userByAgent"
                    . " SET"
                    . " plaqueAgent = :plaqueAgent"
                    . " WHERE idUser = :idUser";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":plaqueAgent", $userByAgent->plaqueAgent);
            $statement->bindParam(":idUser", $userByAgent->idUser);
            $updated = $statement->execute();
            return $updated;
        }

        public function readOne($idUser)
        {
            $query =
                "SELECT *"
                . " FROM userByAgent"
                . " WHERE idUser = :idUser"
                . " LIMIT 0, 1";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":idUser", $idUser);
            $statement->execute();
            $userByAgents = $this->getUserByAgents($statement);
            return $this->getFirstResult($userByAgents);
        }

        private function getUserByAgents($statement)
        {
            $userByAgents = array();
            if ($statement->rowCount() > 0)
            {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC))
                {
                    extract($row);

                    $userByAgent = new UserByAgent();
                    $userByAgent->idUser = intval($idUser);
                    $userByAgent->plaqueAgent = $plaqueAgent;
                    array_push($userByAgents, $userByAgent);
                }
            }
            return $userByAgents;
        }
    }