<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('agent_dao.php');
    include_once('base_dao.php');
    include_once('ipat_classification_dao.php');
    include_once('ipat_severity_dao.php');
    include_once('ipat_transaction_state_dao.php');
    include_once('ipat_transaction_novelty_dao.php');
    include_once('../../builder/ipat_delivery_builder.php');
    include_once('../../common/america_datetime.php');
    include_once('../../model/ipat_delivery.php');
    include_once('../../model/ipat_transaction_statistic.php');

    class IpatDeliveryDao extends BaseDao
    {
        private $builder;
        private $agentDao;
        private $ipatSeverityDao;
        private $ipatClassificationDao;
        private $ipatTransactionStateDao;
        private $ipatTransactionNoveltyDao;
        private $americaDateTime;

        /**
         * IpatDeliveryDao constructor.
         */
        public function __construct()
        {
            parent::__construct();
            $this->builder = new IpatTransactionBuilder();
            $this->agentDao = new AgentDao();
            $this->ipatSeverityDao = new IpatSeverityDao();
            $this->ipatClassificationDao = new IpatClassificationDao();
            $this->ipatTransactionStateDao = new IpatTransactionStateDao();
            $this->ipatTransactionNoveltyDao = new IpatTransactionNoveltyDao();
            $this->americaDateTime = new AmericaDateTime();
        }

        function create($ipatDelivery)
        {
            $currentDate = $this->getCurrentDate();

            $query =
                "INSERT INTO ipatTransaction
                    SET
                    ipatNumber=:ipatNumber,
                    deliveryDate=:deliveryDate,
                    deliveredBy=:deliveredBy,
                    deliveredTo=:deliveredTo,
                    idTransactionState=:idTransactionState,
                    createdBy=:createdBy,
                    createdAt=:createdAt";

            // prepare query
            $statement = $this->connection->prepare($query);

            // sanitize
            $ipatDelivery->ipatNumber=htmlspecialchars(strip_tags($ipatDelivery->ipatNumber));
            $ipatDelivery->deliveryDate=htmlspecialchars(strip_tags($ipatDelivery->deliveryDate));
            $ipatDelivery->deliveredBy=htmlspecialchars(strip_tags($ipatDelivery->deliveredBy));
            $ipatDelivery->deliveredTo=htmlspecialchars(strip_tags($ipatDelivery->deliveredTo));

            // bind values
            $statement->bindParam(":ipatNumber", $ipatDelivery->ipatNumber);
            $statement->bindParam(":deliveryDate", $ipatDelivery->deliveryDate);
            $statement->bindParam(":deliveredBy", $ipatDelivery->deliveredBy);
            $statement->bindParam(":deliveredTo", $ipatDelivery->deliveredTo);
            $statement->bindParam(":idTransactionState", $ipatDelivery->idTransactionState);
            $statement->bindParam(":deliveredBy", $ipatDelivery->deliveredBy);
            $statement->bindParam(":createdBy", $ipatDelivery->deliveredTo);
            $statement->bindParam(":createdAt", $currentDate);

            return $this->execute($statement); // Review issue using 'this'
        }

        public function readAll()
        {
            $query = "SELECT * FROM ipatTransaction";
            $statement = $this->connection->prepare($query);
            $this->execute($statement);
            $transactions = $this->getTransactions($statement);
            return $transactions;
        }

        public function readAllUsingFilterCriteria($filter)
        {
            $query =
                "SELECT *"
                    . " FROM ipatTransaction"
                    . " WHERE " . $filter->filterBy . " LIKE CONCAT('%',:valueFilter,'%')";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":valueFilter", $filter->valueFilter);
            $this->execute($statement);
            return $this->getTransactions($statement);
        }

        public function readAllByDeliveredToAndTransactionState($deliveredTo, $idTransactionState)
        {
            $query =
                "SELECT *"
                    . " FROM ipatTransaction"
                    . " WHERE deliveredTo = :deliveredTo AND"
                    . " idTransactionState = :idTransactionState";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":deliveredTo", $deliveredTo);
            $statement->bindParam(":idTransactionState", $idTransactionState);
            $this->execute($statement);
            return $this->getTransactions($statement);
        }

        public function readAllSorted()
        {
            $query = "SELECT * FROM ipatTransaction ORDER BY ipatNumber DESC";
            $statement = $this->connection->prepare($query);
            $this->execute($statement);
            return $this->getTransactions($statement);
        }

        public function readLastSorted()
        {
            $query =
                "SELECT transactions.*"
                    . " FROM ("
                    . "( SELECT *"
                    . " FROM ipatTransaction"
                    . " WHERE idTransactionState <> 3"
                    . " ORDER BY ipatNumber DESC"
                    . " LIMIT 0, 30 )"
                    . " UNION"
                    . " ( SELECT *"
                    . " FROM ipatTransaction"
                    . " WHERE idTransactionState = 3"
                    . " ORDER BY receptionDate DESC"
                    . " LIMIT 0, 20 )) transactions"
                    . " ORDER BY transactions.ipatNumber DESC";

            $statement = $this->connection->prepare($query);
            $this->execute($statement);
            $transactions = $this->getTransactions($statement);
            return $transactions;
        }

        public function readAllSortedByReceptionDate($exportDate)
        {
            $receptionDateAsString = $exportDate->format(AmericaDateTime::DEFAULT_DATE_FORMAT);
            $query =
                "SELECT *"
                    . " FROM ipatTransaction"
                    . " WHERE DATE(receptionDate) = :receptionDate"
                    . " ORDER BY receptionDate";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":receptionDate", $receptionDateAsString);
            $this->execute($statement);
            return $this->getTransactions($statement);
        }

        public function readOne($ipatNumber)
        {
            $query = "SELECT * FROM ipatTransaction WHERE ipatNumber=:ipatNumber LIMIT 0, 1";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":ipatNumber", $ipatNumber);
            $this->execute($statement);
            return $this->getTransactions($statement);
        }

        public function readAllWhenDeliveredDateIsGreaterThanMaxDays($deliveredTo, $maxDays)
        {
            $currentDate = date("y-m-d");
            $query =
                "SELECT *"
                    . " FROM ipatTransaction"
                    . " WHERE deliveredTo = :deliveredTo"
                    . " AND idTransactionState = 1"
                    . " AND DATEDIFF(:currentDate, deliveryDate) > :maxDays";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":deliveredTo", $deliveredTo);
            $statement->bindParam(":currentDate", $currentDate);
            $statement->bindParam(":maxDays", $maxDays);
            $this->execute($statement);
            $transactions = $this->getTransactions($statement);
            return $transactions;
        }

        public function readAllBetweenRangeWhenDeliveredDateIsGreaterThanMaxDays(
            $deliveredTo, $initialNumber, $finalNumber, $maxDays)
        {
            $currentDate = date("y-m-d");
            $query =
                "SELECT *"
                    . " FROM ipatTransaction"
                    . " WHERE deliveredTo = :deliveredTo"
                    . " AND idTransactionState = 1"
                    . " AND ipatNumber BETWEEN :initialNumber AND :finalNumber"
                    . " AND DATEDIFF(:currentDate, deliveryDate) > :maxDays";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":deliveredTo", $deliveredTo);
            $statement->bindParam(":currentDate", $currentDate);
            $statement->bindParam(":initialNumber", $initialNumber);
            $statement->bindParam(":finalNumber", $finalNumber);
            $statement->bindParam(":maxDays", $maxDays);
            $this->execute($statement);
            $transactions = $this->getTransactions($statement);
            return $transactions;
        }

        public function readAllWhenPartialReceptionDateIsGreaterThanMaxDays(
            $partiallyReceivedTo, $maxDays)
        {
            $currentDate = date("y-m-d");
            $query =
                "SELECT *"
                    . " FROM ipatTransaction"
                    . " WHERE partiallyReceivedTo = :partiallyReceivedTo"
                    . " AND idTransactionState = 2"
                    . " AND DATEDIFF(:currentDate, partialReceptionDate) > :maxDays";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":partiallyReceivedTo", $partiallyReceivedTo);
            $statement->bindParam(":currentDate", $currentDate);
            $statement->bindParam(":maxDays", $maxDays);
            $this->execute($statement);
            $transactions = $this->getTransactions($statement);
            return $transactions;
        }

        public function getLastIpatNumberDelivered()
        {
            $query = "SELECT MAX(ipatNumber) AS ipatNumber FROM ipatTransaction";
            $statement = $this->connection->prepare($query);
            $this->execute($statement);
            return $this->getTotal($statement, 'ipatNumber');
        }

        public function countTransactionsBetweenRange($initialNumber, $finalNumber)
        {
            $query =
                "SELECT COUNT(ipatNumber) AS total"
                    . " FROM ipatTransaction"
                    . " WHERE  ipatNumber BETWEEN :initialNumber AND :finalNumber";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":initialNumber", $initialNumber);
            $statement->bindParam(":finalNumber", $finalNumber);
            $this->execute($statement);
            return $this->getTotal($statement, 'total');
        }

        public function countTransactionsBetweenRangeAndInState(
            $initialNumber, $finalNumber, $idTransactionState)
        {
            $query =
                "SELECT COUNT(ipatNumber) AS total"
                    . " FROM ipatTransaction"
                    . " WHERE  ipatNumber BETWEEN :initialNumber AND :finalNumber"
                    . " AND idTransactionState= :idTransactionState";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":initialNumber", $initialNumber);
            $statement->bindParam(":finalNumber", $finalNumber);
            $statement->bindParam(":idTransactionState", $idTransactionState);
            $this->execute($statement);
            return $this->getTotal($statement, 'total');
        }

        private function getTransactions($statement)
        {
            $totalRows = $statement->rowCount();
            $transactions = array();
            if ($totalRows > 0)
            {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC))
                {
                    extract($row);
                    $agentDeliveredBy = $this->agentDao->readOne($deliveredBy);
                    $agentDeliveredTo = $this->agentDao->readOne($deliveredTo);
                    $agentPartiallyReceivedBy = $this->agentDao->readOne($partiallyReceivedBy);
                    $agentPartiallyReceivedTo = $this->agentDao->readOne($partiallyReceivedTo);
                    $agentReceivedBy = $this->agentDao->readOne($receivedBy);
                    $agentReceivedTo = $this->agentDao->readOne($receivedTo);
                    $transactionState = $this->ipatTransactionStateDao->readOne($idTransactionState);
                    $severity = $this->ipatSeverityDao->readOne($idSeverity);
                    $classification = $this->ipatClassificationDao->readOne($idClassification);

                    $totalNovelties = $this->ipatTransactionNoveltyDao->count($ipatNumber);

                    $transaction =
                        $this->builder->buildIpatDelivery(
                            intval($ipatNumber),
                            $deliveryDate,
                            $deliveredBy,
                            $deliveredTo,
                            $partiallyReceivedBy,
                            $partiallyReceivedTo,
                            $receivedBy,
                            $receivedTo,
                            $idSeverity,
                            $idClassification,
                            $agentDeliveredBy,
                            $agentDeliveredTo,
                            $idTransactionState,
                            $transactionState,
                            $partialReceptionDate,
                            $receptionDate,
                            $accidentDate,
                            $diligenceDate,
                            $address,
                            $vehiclePlaque,
                            $severity,
                            $classification,
                            $criminalNews,
                            $observation,
                            $agentPartiallyReceivedBy,
                            $agentPartiallyReceivedTo,
                            $agentReceivedBy,
                            $agentReceivedTo,
                            0,
                            $totalNovelties);
                    array_push($transactions, $transaction);
                }
            }
            return $transactions;
        }

        /**
         * Get the transactions in an specific state based on the agent and the range
         * of ipats.
         *
         * @param $plaque of the agent to search.
         * @param $initialNumber lower threshold to search.
         * @param $finalNumber higher threshold to search.
         * @param $idTransactionState state to search.
         * @return array|null that represents the transactions found if those exist, null
         * otherwise.
         */
        public function getTransactionsByAgentAndIpatRangeAndIdTransactionState(
            $plaque, $initialNumber, $finalNumber, $idTransactionState)
        {
            $columnName = $this->getAgentColumnNameBasedOnIdTransactionState($idTransactionState);
            $transactions = array();
            if ($columnName != "")
            {
                $query =
                    "SELECT *"
                        . " FROM ipatTransaction"
                        . " WHERE " . $columnName . " = :plaque AND"
                        . " idTransactionState = :idTransactionState AND"
                        . " ipatNumber BETWEEN :initialNumber AND :finalNumber";
                $statement = $this->connection->prepare($query);
                $statement->bindParam(":plaque", $plaque);
                $statement->bindParam(":initialNumber", $initialNumber);
                $statement->bindParam(":finalNumber", $finalNumber);
                $statement->bindParam(":idTransactionState", $idTransactionState);
                $this->execute($statement);
                $transactions = $this->getTransactions($statement);
            }
            return $transactions;
        }

        public function getTotalByAgentAndTransactionState($plaqueAgent, $idTransactionState)
        {
            $query =
                "SELECT COUNT(ipatNumber) AS total"
                    . " FROM ipatTransaction"
                    . " WHERE deliveredTo=:deliveredTo AND"
                    . " idTransactionState=:idTransactionState";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":deliveredTo", $plaqueAgent);
            $statement->bindParam(":idTransactionState", $idTransactionState);
            $this->execute($statement);
            return $this->getTotal($statement, 'total');
        }


        /**
         * Get the total of transactions based on the agent and the range of ipats.
         *
         * @param $plaque of the agent to search.
         * @param $initialNumber lower threshold to search.
         * @param $finalNumber higher threshold to search.
         * @return int that represents the total of transactions found.
         */
        public function getTotalByAgentAndIpatRange(
            $plaque, $initialNumber, $finalNumber)
        {
            $query =
                "SELECT COUNT(ipatNumber) AS total"
                    . " FROM ipatTransaction"
                    . " WHERE (deliveredTo = :plaque OR partiallyReceivedTo = :plaque OR receivedTo = :plaque) AND"
                    . " ipatNumber BETWEEN :initialNumber AND :finalNumber";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":plaque", $plaque);
            $statement->bindParam(":initialNumber", $initialNumber);
            $statement->bindParam(":finalNumber", $finalNumber);
            $this->execute($statement);
            return $this->getTotal($statement, 'total');
        }

        /**
         * Get the total of transactions in stated delivered based on the agent and the range
         * of ipats.
         *
         * @param $plaque of the agent to search.
         * @param $initialNumber lower threshold to search.
         * @param $finalNumber higher threshold to search.
         * @return int that represents the total of transactions found.
         */
        public function getTotalDeliveredByAgentAndIpatRange(
            $plaque, $initialNumber, $finalNumber)
        {
            $query =
                "SELECT COUNT(ipatNumber) AS total"
                    . " FROM ipatTransaction"
                    . " WHERE deliveredTo = :plaque AND"
                    . " idTransactionState = 1 AND"
                    . " ipatNumber BETWEEN :initialNumber AND :finalNumber";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":plaque", $plaque);
            $statement->bindParam(":initialNumber", $initialNumber);
            $statement->bindParam(":finalNumber", $finalNumber);
            $this->execute($statement);
            return $this->getTotal($statement, 'total');
        }

        /**
         * Get the total of transactions in stated partiallyReceived based on the agent and the
         * range of ipats.
         *
         * @param $plaque of the agent to search.
         * @param $initialNumber lower threshold to search.
         * @param $finalNumber higher threshold to search.
         * @return int that represents the total of transactions found.
         */
        public function getTotalPartiallyReceivedByAgentAndIpatRange(
            $plaque, $initialNumber, $finalNumber)
        {
            $query =
                "SELECT COUNT(ipatNumber) AS total"
                    . " FROM ipatTransaction"
                    . " WHERE partiallyReceivedTo = :plaque AND"
                    . " idTransactionState = 2 AND"
                    . " ipatNumber BETWEEN :initialNumber AND :finalNumber";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":plaque", $plaque);
            $statement->bindParam(":initialNumber", $initialNumber);
            $statement->bindParam(":finalNumber", $finalNumber);
            $this->execute($statement);
            return $this->getTotal($statement, 'total');
        }

        public function readAllByAgentAndTransactionState($plaqueAgent, $idTransactionState)
        {
            $query =
                "SELECT *"
                    . " FROM ipatTransaction"
                    . " WHERE deliveredTo = :deliveredTo AND"
                    . " idTransactionState = :idTransactionState";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":deliveredTo", $plaqueAgent);
            $statement->bindParam(":idTransactionState", $idTransactionState);
            $this->execute($statement);
            return $this->getTransactions($statement);;
        }

        public function updateTransactionState($transaction)
        {
            $currentDate = $this->getCurrentDate();
            $query =
                "UPDATE ipatTransaction"
                    . " SET idTransactionState = :idTransactionState,"
                    . " partialReceptionDate = :partialReceptionDate,"
                    . " partiallyReceivedBy = :partiallyReceivedBy,"
                    . " partiallyReceivedTo = :partiallyReceivedTo,"
                    . " modifiedAt = :modifiedAt,"
                    . " modifiedBy = :modifiedBy"
                    . " WHERE ipatNumber = :ipatNumber";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":ipatNumber", $transaction->ipatNumber);
            $statement->bindParam(":partialReceptionDate", $transaction->partialReceptionDate);
            $statement->bindParam(":partiallyReceivedBy", $transaction->partiallyReceivedBy);
            $statement->bindParam(":partiallyReceivedTo", $transaction->partiallyReceivedTo);
            $statement->bindParam(":idTransactionState", $transaction->idTransactionState);
            $statement->bindParam(":modifiedAt", $currentDate);
            $statement->bindParam(":modifiedBy", $transaction->modifiedBy);
            $this->execute($statement);
            return $statement->rowCount() > 0;
        }

        /**
         * Perform an update in the ipattransaction table.
         *
         * @param $transaction with the data to be updated.
         *
         * @return bool true if the update was done, false otherwise.
         */
        public function updateTransaction($transaction)
        {
            $currentDate = $this->getCurrentDate();
            $query =
                "UPDATE ipatTransaction"
                    . " SET idTransactionState=:idTransactionState,"
                    . " idSeverity = :idSeverity,"
                    . " idClassification = :idClassification,"
                    . " accidentDate = :accidentDate,"
                    . " diligenceDate = :diligenceDate,"
                    . " address = :address,"
                    . " receptionDate = :receptionDate,"
                    . " vehiclePlaque = :vehiclePlaque,"
                    . " criminalNews = :criminalNews,"
                    . " observation = :observation,"
                    . " receivedBy = :receivedBy,"
                    . " receivedTo = :receivedTo,"
                    . " modifiedAt = :modifiedAt,"
                    . " modifiedBy = :modifiedBy"
                    . " WHERE ipatNumber = :ipatNumber";

            $statement = $this->connection->prepare($query);

            $statement->bindParam(":idTransactionState", $transaction->idTransactionState);
            $statement->bindParam(":idSeverity", $transaction->idSeverity);
            $statement->bindParam(":idClassification", $transaction->idClassification);
            $statement->bindParam(":accidentDate", $transaction->accidentDate);
            $statement->bindParam(":diligenceDate", $transaction->diligenceDate);
            $statement->bindParam(":address", $transaction->address);
            $statement->bindParam(":vehiclePlaque", $transaction->vehiclePlaque);
            $statement->bindParam(":criminalNews", $transaction->criminalNews);
            $statement->bindParam(":observation", $transaction->observation);
            $statement->bindParam(":receivedBy", $transaction->receivedBy);
            $statement->bindParam(":receivedTo", $transaction->receivedTo);
            $statement->bindParam(":receptionDate", $transaction->receptionDate);
            $statement->bindParam(":modifiedAt", $currentDate);
            $statement->bindParam(":modifiedBy", $transaction->receivedBy);
            $statement->bindParam(":ipatNumber", $transaction->ipatNumber);

            $this->execute($statement);

            return $statement->rowCount() > 0;
        }

        /**
         * Perform an update given the transaction received.
         * @param $transaction with the data to be updated.
         * @return bool true if the update was done, false otherwise.
         */
        public function updateAll($transaction)
        {
            $query =
                "UPDATE ipatTransaction"
                . " SET idTransactionState= :idTransactionState,"
                . " idSeverity = :idSeverity,"
                . " idClassification = :idClassification,"
                . " accidentDate = :accidentDate,"
                . " diligenceDate = :diligenceDate,"
                . " address = :address,"
                . " deliveryDate = :deliveryDate,"
                . " partialReceptionDate = :partialReceptionDate,"
                . " receptionDate = :receptionDate,"
                . " vehiclePlaque = :vehiclePlaque,"
                . " criminalNews = :criminalNews,"
                . " observation = :observation,"
                . " deliveredBy = :deliveredBy,"
                . " deliveredTo = :deliveredTo,"
                . " partiallyReceivedBy = :partiallyReceivedBy,"
                . " partiallyReceivedTo = :partiallyReceivedTo,"
                . " receivedBy = :receivedBy,"
                . " receivedTo = :receivedTo,"
                . " modifiedAt = :modifiedAt,"
                . " modifiedBy = :modifiedBy"
                . " WHERE ipatNumber = :ipatNumber";

            $statement = $this->connection->prepare($query);

            $statement->bindParam(":idTransactionState", $transaction->idTransactionState);
            $statement->bindParam(":idSeverity", $transaction->idSeverity);
            $statement->bindParam(":idClassification", $transaction->idClassification);
            $statement->bindParam(":accidentDate", $transaction->accidentDate);
            $statement->bindParam(":diligenceDate", $transaction->diligenceDate);
            $statement->bindParam(":address", $transaction->address);
            $statement->bindParam(":vehiclePlaque", $transaction->vehiclePlaque);
            $statement->bindParam(":criminalNews", $transaction->criminalNews);
            $statement->bindParam(":observation", $transaction->observation);
            $statement->bindParam(":deliveredBy", $transaction->deliveredBy);
            $statement->bindParam(":deliveredTo", $transaction->deliveredTo);
            $statement->bindParam(":partiallyReceivedBy", $transaction->partiallyReceivedBy);
            $statement->bindParam(":partiallyReceivedTo", $transaction->partiallyReceivedTo);
            $statement->bindParam(":receivedBy", $transaction->receivedBy);
            $statement->bindParam(":receivedTo", $transaction->receivedTo);
            $statement->bindParam(":deliveryDate", $transaction->deliveryDate);
            $statement->bindParam(":partialReceptionDate", $transaction->partialReceptionDate);
            $statement->bindParam(":receptionDate", $transaction->receptionDate);
            $statement->bindParam(":modifiedAt", $currentDate);
            $statement->bindParam(":modifiedBy", $transaction->receivedBy);
            $statement->bindParam(":ipatNumber", $transaction->ipatNumber);

            $this->execute($statement);

            return $statement->rowCount() > 0;
        }

        public function updateIpatDelivery($transaction)
        {
            $receptionDate = $this->getCurrentDate();
            $query =
                "UPDATE ipatTransaction"
                    . " SET deliveredTo = :deliveredTo,"
                    . " deliveryDate = :deliveryDate,"
                    . " modifiedAt = :modifiedAt"
                    . " WHERE ipatNumber = :ipatNumber";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":deliveredTo", $transaction->deliveredTo);
            $statement->bindParam(":deliveryDate", $receptionDate);
            $statement->bindParam(":modifiedAt", $receptionDate);
            $statement->bindParam(":ipatNumber", $transaction->ipatNumber);
            $this->execute($statement);
            
            return $statement->rowCount() > 0;
        }

        public function readAllStatistics()
        {
            $query = "SELECT idTransactionState,
                        COUNT(ipatNumber) AS totalByTransactionState
                    FROM ipatTransaction GROUP BY idTransactionState";
            $statement = $this->connection->prepare($query);
            $this->execute($statement);
            return $this->getStatistics($statement);
        }

        public function readAllStatisticsByAgent($agentPlaque)
        {
            $query = "SELECT idTransactionState,
                        COUNT(ipatNumber) AS totalByTransactionState
                    FROM ipatTransaction
                    WHERE deliveredTo =:deliveredTo
                    GROUP BY idTransactionState";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":deliveredTo", $agentPlaque);
            $this->execute($statement);
            return $this->getStatistics($statement);
        }

        public function readLastIpatRanges()
        {
            $query = "SELECT idTransactionState,
                        COUNT(ipatNumber) AS totalByTransactionState
                    FROM ipatTransaction GROUP BY idTransactionState";
            $statement = $this->connection->prepare($query);
            $this->execute($statement);
            return $this->getStatistics($statement);
        }

        public function readDistinctAgentPlaques()
        {
            $query =
                "SELECT DISTINCT(deliveredTo) AS plaqueAgent"
                    . " FROM ipatTransaction"
                    . " UNION"
                    . " SELECT DISTINCT(partiallyReceivedTo) AS plaqueAgent"
                    . " FROM ipatTransaction"
                    . " UNION"
                    . " SELECT DISTINCT(receivedTo) AS plaqueAgent"
                    . " FROM ipatTransaction";
            $statement = $this->connection->prepare($query);
            $this->execute($statement);
            $totalRows = $statement->rowCount();
            $plaques = array();
            if ($totalRows > 0)
            {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC))
                {
                    extract($row);
                    array_push($plaques, $plaqueAgent);
                }
            }
            return $plaques;
        }

        public function reset($ipatNumber, $modifiedBy)
        {
            $currentDate = $this->getCurrentDate();

            $query = 'UPDATE ipatTransaction'
                . ' SET idTransactionState = 1,'
                    . ' partiallyReceivedBy = NULL,'
                    . ' partiallyReceivedTo = NULL,'
                    . ' receivedBy = NULL,'
                    . ' receivedTo = NULL,'
                    . ' partialReceptionDate = NULL,'
                    . ' receptionDate = NULL,'
                    . ' accidentDate = NULL,'
                    . ' diligenceDate = NULL,'
                    . ' address = NULL,'
                    . ' idSeverity = NULL,'
                    . ' idClassification = NULL,'
                    . ' vehiclePlaque = NULL,'
                    . ' criminalNews = NULL,'
                    . ' observation = NULL,'
                    . ' modifiedAt = :modifiedAt,'
                    . ' modifiedBy = :modifiedBy'
                    . ' WHERE ipatNumber = :ipatNumber';
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":ipatNumber", $ipatNumber);
            $statement->bindParam(":modifiedAt", $currentDate);
            $statement->bindParam(":modifiedBy", $modifiedBy);
            $this->execute($statement);
            return $statement->rowCount();
        }

        private function getStatistics($statement)
        {
            $totalRows = $statement->rowCount();
            $statistics = array();
            if ($totalRows > 0)
            {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC))
                {
                    extract($row);
                    $transactionState = $this->ipatTransactionStateDao->readOne($idTransactionState);
                    $statistic = new IpatTransactionStatistic();
                    $statistic->idTransactionState = $idTransactionState;
                    $statistic->totalByTransactionState = $totalByTransactionState;
                    $statistic->transactionState = $transactionState;
                    array_push($statistics, $statistic);
                }
            }
            return $statistics;
        }

        private function getCurrentDate()
        {
            $currentDateTime = $this->americaDateTime->getCurrentDateTime();
            return $currentDateTime->format(AmericaDateTime::DEFAULT_DATETIME_FORMAT);
        }

        private function getTotal($statement, $columnName)
        {
            $total = 0;
            if ($statement->rowCount() > 0)
            {
                $row = $statement->fetch(PDO::FETCH_ASSOC);
                $total = intval($row[$columnName]);
            }
            return $total == null ? 0 : $total;
        }

        private function getAgentColumnNameBasedOnIdTransactionState($idTransactionState)
        {
            $columnName = "";
            if ($idTransactionState == 1)
            {
                $columnName = "deliveredTo";
            }
            else if($idTransactionState == 2)
            {
                $columnName = "partiallyReceivedTo";
            }
            else if ($idTransactionState == 3)
            {
                $columnName = "receivedTo";
            }
            return $columnName;
        }
    }
?>