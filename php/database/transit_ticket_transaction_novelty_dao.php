<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('base_dao.php');
    include_once('../../model/transit_ticket_transaction_novelty.php');

    class TransitTicketTransactionNoveltyDao extends BaseDao
    {
        public function __construct()
        {
            parent::__construct();
        }

        public function save($novelty)
        {
            $query =
                "INSERT INTO transitTicketTransactionNovelty"
                    . " SET"
                    . " talonaryNumber = :talonaryNumber,"
                    . " ticketNumber = :ticketNumber,"
                    . " diligenceDate = :diligenceDate,"
                    . " pstDeliveryDate = :pstDeliveryDate,"
                    . " observation = :observation,"
                    . " createdBy = :createdBy,"
                    . " createdAt = :createdAt";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":talonaryNumber", $novelty->talonaryNumber);
            $statement->bindParam(":ticketNumber", $novelty->ticketNumber);
            $statement->bindParam(":diligenceDate", $novelty->diligenceDate);
            $statement->bindParam(":pstDeliveryDate", $novelty->pstDeliveryDate);
            $statement->bindParam(":observation", $novelty->observation);
            $statement->bindParam(":createdBy", $novelty->createdBy);
            $statement->bindParam(":createdAt", $novelty->createdAt);

            return $statement->execute();
        }

        public function count($talonaryNumber, $ticketNumber)
        {
            $query =
                "SELECT COUNT(id) AS Count"
                    . " FROM transitTicketTransactionNovelty"
                    . " WHERE talonaryNumber = :talonaryNumber"
                    . " AND ticketNumber = :ticketNumber";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":talonaryNumber", $talonaryNumber);
            $statement->bindParam(":ticketNumber", $ticketNumber);
            $statement->execute();
            $count = 0;
            if ($statement->rowCount() > 0)
            {
                $row = $statement->fetch(PDO::FETCH_ASSOC);
                $count = intval($row['Count']);
            }
            return $count;
        }

        public function countAll()
        {
            $query =
                "SELECT COUNT(id) AS Count"
                . " FROM transitTicketTransactionNovelty";
            $statement = $this->connection->prepare($query);
            $statement->execute();
            return $this->getCount($statement);
        }

        public function countAllByAgent($plaqueAgent)
        {
            $query =
                "SELECT COUNT(TTN.id) AS Count"
                    . " FROM transitTicketTransactionNovelty TTN"
                    . " INNER JOIN transitTicketTransaction TTT ON"
                    . " TTT.ticketNumber = TTN.ticketNumber"
                    . " AND TTT.talonaryNumber = TTN.talonaryNumber"
                    . " AND TTT.plaqueAgent = :plaqueAgent";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":plaqueAgent", $plaqueAgent);
            $statement->execute();
            return $this->getCount($statement);
        }
    }
