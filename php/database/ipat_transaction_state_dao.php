<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('base_dao.php');
    include_once('../../model/ipat_transaction_state.php');

    final class IpatTransactionStateDao extends BaseDao
    {
        public function __construct()
        {
            parent::__construct();
        }

        public function readAll()
        {
            $query = "SELECT * FROM ipatTransactionState";
            $statement = $this->connection->prepare($query);
            $this->execute($statement);
            return $this->getIpatTransactions($statement);
        }

        public function readOne($id)
        {
            $query = "SELECT *
                        FROM ipatTransactionState
                        WHERE id =:id
                        LIMIT 0,1";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":id", $id);
            $this->execute($statement);
            $transactions = $this->getIpatTransactions($statement);
            return $this->getFirstResult($transactions);
        }

        public function readAllDifferentTo($id)
        {
            $query = "SELECT * FROM ipatTransactionState WHERE id <> :id";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":id", $id);
            $this->execute($statement);
            return $this->getIpatTransactions($statement);
        }

        private function getIpatTransactions($statement)
        {
            $totalRows = $statement->rowCount();
            $ipatTransactions = array();
            if ($totalRows > 0)
            {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC))
                {
                    extract($row);
                    $ipatTransaction = new IpatTransactionState();
                    $ipatTransaction->id = intval($id);
                    $ipatTransaction->name = $name;
                    array_push($ipatTransactions, $ipatTransaction);
                }
            }
            return $ipatTransactions;
        }
    }
?>