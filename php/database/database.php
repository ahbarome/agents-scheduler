<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    class Database
    {
        private $host = "localhost";
        private $db_name = "transit";
        private $username = "root";
        private $password = "";
        private $connection;

        public function getConnection()
        {
            $this->closeConnection();
            try
            {
                $this->connection =
                    new PDO(
                        "mysql:host=" . $this->host. ";dbname=" . $this->db_name,
                        $this->username,
                        $this->password,
                        array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8") );
            }
            catch(PDOException $exception)
            {
                echo "Error en la conexión: " . $exception->getMessage();
            }
            return $this->connection;
        }

        public function closeConnection()
        {
            if ($this->connection !== null)
            {
                $this->connection = null;
            }
        }
    }
