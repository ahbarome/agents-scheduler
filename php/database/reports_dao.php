<?php
   /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('agent_dao.php');
    include_once('base_dao.php');
    include_once('../../model/ipat_delivery.php');
    include_once('../../model/report_ipats_received_with_pending_state.php');

    /**
     * Class ReportsDao allows to get all the data needed for the differents report
     */
    class ReportsDao extends BaseDao
    {
        private $agentDao;

        /**
         * ReportsDao constructor.
         */
        public function __construct()
        {
            parent::__construct();
            $this->agentDao = new AgentDao();
        }

        /**
         * Read all the ipats from the database in state received with pending.
         *
         * @return array with the ipats in state received with pending.
         */
        public function readAllIpatsReceivedWithPendingState()
        {
            $query =
                "SELECT partiallyReceivedTo,"
                    . " COUNT(idTransactionState) AS numberOfIpats"
                    . " FROM ipatTransaction"
                    . " WHERE idTransactionState = '2'"
                    . " GROUP BY partiallyReceivedTo";
            $statement = $this->connection->prepare($query);
            $statement->execute();
            $transactions = $this->getIpatsReceivedWithPendingStateTransactions($statement);
            return $transactions;
        }

        private function getIpatsReceivedWithPendingStateTransactions($statement)
        {
            $transactions = array();
            if ($statement->rowCount() > 0)
            {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC))
                {
                    extract($row);
                    $agent = $this->agentDao->readOne($partiallyReceivedTo);

                    $ipatTransactions =
                        $this->readAllIpatNumbersReceivedWithPendingState($partiallyReceivedTo);

                    $reportData = new ReportIpatsReceivedWithPendingState();
                    $reportData->agent = $agent;
                    $reportData->transactions = $ipatTransactions;
                    $reportData->numberOfIpats = $numberOfIpats;
                    array_push($transactions, $reportData);
                }
            }
            return $transactions;
        }

        private function readAllIpatNumbersReceivedWithPendingState($plaque)
        {
            $query =
                "SELECT ipatNumber, partiallyReceivedBy, partialReceptionDate"
                . " FROM ipatTransaction"
                . " WHERE partiallyReceivedTo = :partiallyReceivedTo"
                . " AND idTransactionState = '2'";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":partiallyReceivedTo", $plaque);
            $statement->execute();
            $transactions = $this->getIpatNumbers($statement);
            return $transactions;
        }

        private function getIpatNumbers($statement)
        {
            $transactions = array();
            if ($statement->rowCount() > 0)
            {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC))
                {
                    extract($row);
                    $agent = $this->agentDao->readOne($partiallyReceivedBy);

                    $transaction = new IpatTransaction();
                    $transaction->ipatNumber = $ipatNumber;
                    $transaction->partiallyReceivedBy = $partiallyReceivedBy;
                    $transaction->partialReceptionDate = $partialReceptionDate;
                    $transaction->agentPartiallyReceivedBy = $agent;
                    array_push($transactions, $transaction);
                }
            }
            return $transactions;
        }
    }