<?php

    /*
    * Copyright (c) 2018 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('base_dao.php');
    include_once('../../model/ipat_delivery_statistic.php');

    class IpatTransactionStatisticDao extends BaseDao
    {
        /**
         * IpatTransactionStatisticDao constructor.
         */
        public function __construct()
        {
            parent::__construct();
        }

        /**
         * Read all the statistics based on the each state of the ipat transaction.
         *
         * @return array|null if were not found the transactions is going to return null, otherwise
         * is going to retreive the total of each state of the ipat transaction.
         */
        public function readAllStatesStatisticsByRange($initialNumber, $finalNumber)
        {
            $query =
                "SELECT"
                . " ( SELECT COUNT(ipatNumber) FROM ipatTransaction"
                . " WHERE ipatNumber BETWEEN :initialNumber AND :finalNumber"
                . " AND idTransactionState = 1 ) as totalDelivered,"
                . " ( SELECT COUNT(ipatNumber) FROM ipatTransaction"
                . " WHERE ipatNumber BETWEEN :initialNumber AND :finalNumber"
                . " AND idTransactionState = 2 ) as totalPartiallyReceived,"
                . " ( SELECT COUNT(ipatNumber) FROM ipatTransaction"
                . " WHERE ipatNumber BETWEEN :initialNumber AND :finalNumber"
                . " AND idTransactionState = 3 ) as totalReceived";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":initialNumber", $initialNumber);
            $statement->bindParam(":finalNumber", $finalNumber);
            $statement->execute();
            return $this->getFirstResult($this->getStatistics($statement));
        }

        /**
         * Read all the statistics based on the each severity stored for the ipat transaction.
         *
         * @return array|null if were not found the transactions is going to return null, otherwise
         * is going to retreive the total of each severity of the ipat transaction.
         */
        public function readAllSeveritiesStatistics()
        {
            $query =
                "SELECT"
                . " ( SELECT COUNT(ipatNumber) FROM ipatTransaction"
                . " WHERE idSeverity = 1 ) as totalDamages,"
                . " ( SELECT COUNT(ipatNumber) FROM ipatTransaction"
                . " WHERE idSeverity = 2 ) as totalInjuries,"
                . " ( SELECT COUNT(ipatNumber) FROM ipatTransaction"
                . " WHERE idSeverity = 3 ) as totalHomicides";
            $statement = $this->connection->prepare($query);
            $statement->execute();
            return $this->getFirstResult($this->getStatistics($statement));
        }

        /**
         * Read all the statistics based on the each severity of the ipat transaction.
         *
         * @return array|null if were not found the transactions is going to return null, otherwise
         * is going to retreive the total of each severity of the ipat transaction.
         */
        public function readAllSeveritiesStatisticsByRange($initialNumber, $finalNumber)
        {
            $query =
                "SELECT"
                . " ( SELECT COUNT(ipatNumber) FROM ipatTransaction"
                . " WHERE ipatNumber BETWEEN :initialNumber AND :finalNumber"
                . " AND idSeverity = 1 ) as totalDamages,"
                . " ( SELECT COUNT(ipatNumber) FROM ipatTransaction"
                . " WHERE ipatNumber BETWEEN :initialNumber AND :finalNumber"
                . " AND idSeverity = 2 ) as totalInjuries,"
                . " ( SELECT COUNT(ipatNumber) FROM ipatTransaction"
                . " WHERE ipatNumber BETWEEN :initialNumber AND :finalNumber"
                . " AND idSeverity = 3 ) as totalHomicides";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":initialNumber", $initialNumber);
            $statement->bindParam(":finalNumber", $finalNumber);
            $statement->execute();
            return $this->getFirstResult($this->getStatistics($statement));
        }

        /**
         * Read all the statistics based on the each severity stored and a specific range date in
         * the ipat transaction.
         *
         * @return array|null if were not found the transactions is going to return null, otherwise
         * is going to retreive the total of each severity and range date of the ipat transaction.
         */
        public function readAllSeveritiesStatisticsByDateRange($startDate, $endDate)
        {
            $query =
                "SELECT"
                . " ( SELECT COUNT(ipatNumber) FROM ipatTransaction"
                . " WHERE accidentDate BETWEEN :startDate AND :endDate"
                . " AND idSeverity = 1 ) as totalDamages,"
                . " ( SELECT COUNT(ipatNumber) FROM ipatTransaction"
                . " WHERE accidentDate BETWEEN :startDate AND :endDate"
                . " AND idSeverity = 2 ) as totalInjuries,"
                . " ( SELECT COUNT(ipatNumber) FROM ipatTransaction"
                . " WHERE accidentDate BETWEEN :startDate AND :endDate"
                . " AND idSeverity = 3 ) as totalHomicides";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":startDate", $startDate);
            $statement->bindParam(":endDate", $endDate);
            $this->execute($statement);
            $statistics = $this->getStatistics($statement);
            $statistic = $this->getFirstResult($statistics);
            return $statistic;
        }

        private function getStatistics($statement)
        {
            $statistics = array();
            if ($statement->rowCount() > 0)
            {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC))
                {
                    $statistic = new IpatTransactionStatistic();
                    $statistic->totalDelivered =
                        array_key_exists('totalDelivered', $row) ?
                            intval($row['totalDelivered']) : 0;
                    $statistic->totalPartiallyReceived =
                        array_key_exists('totalPartiallyReceived', $row) ?
                        intval($row['totalPartiallyReceived']) : 0;
                    $statistic->totalReceived =
                        array_key_exists('totalReceived', $row) ?
                            intval($row['totalReceived']) : 0;
                    $statistic->totalNovelties =
                        array_key_exists('totalNovelties', $row) ?
                            intval($row['totalNovelties']) : 0;
                    $statistic->totalDamages =
                        array_key_exists('totalDamages', $row) ? intval($row['totalDamages']) : 0;
                    $statistic->totalInjuries =
                        array_key_exists('totalInjuries', $row) ?
                            intval($row['totalInjuries']) : 0;
                    $statistic->totalHomicides =
                        array_key_exists('totalHomicides', $row) ?
                            intval($row['totalHomicides']) : 0;
                    array_push($statistics, $statistic);
                }
            }
            return $statistics;
        }
    }
?>