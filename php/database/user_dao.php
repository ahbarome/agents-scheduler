<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('base_dao.php');
    include_once('role_dao.php');
    include_once('user_by_agent_dao.php');
    include_once('../../model/user.php');

    /**
     * Class UserDao manage all the methods to work with the user table.
     */
    class UserDao extends BaseDao
    {
        private $roleDao;
        private $userByAgentDao;

        /**
         * UserDao constructor.
         */
        public function __construct()
        {
            parent::__construct();
            $this->roleDao = new RoleDao();
            $this->userByAgentDao = new UserByAgentDao();
        }

        public function save($user)
        {
            $query =
                "INSERT INTO user"
                    . " SET"
                    . " username = :username,"
                    . " password = :password,"
                    . " idRole = :idRole";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":username", $user->username);
            $statement->bindParam(":password", $user->password);
            $statement->bindParam(":idRole", $user->idRole);

            return $this->execute($statement);
        }

        public function update($user)
        {
            $query =
                "UPDATE user"
                    . " SET"
                    . " password = :password,"
                    . " idRole = :idRole"
                    . " WHERE id = :id";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":password", $user->password);
            $statement->bindParam(":idRole", $user->idRole);
            $statement->bindParam(":id", $user->id);

            return $this->execute($statement);
        }

        /**
         * Read all the users from the database.
         *
         * @return array of users.
         */
        public function readAll()
        {
            $query = "SELECT * FROM user";
            $statement = $this->connection->prepare($query);
            $this->execute($statement);
            return $this->getUsers($statement);
        }

        /**
         * Read a specific user by the id.
         *
         * @param $id to be searched in the database.
         * @return a user if this exist, otherwise is going to return null.
         */
        public function readOne($id)
        {
            $query =
                "SELECT *"
                . " FROM user"
                . " WHERE id = :id"
                . " LIMIT 0, 1";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":id", $id);
            $statement->execute();
            $users = $this->getUsers($statement);
            return $this->getFirstResult($users);
        }

        /**
         * Read a specific user by the username.
         *
         * @param $username to be searched in the database.
         * @return a user if this exist, otherwise is going to return null.
         */
        public function readOneByUsername($username)
        {
            $query =
                "SELECT *"
                    . " FROM user"
                    . " WHERE username = :username"
                    . " LIMIT 0,1";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":username", $username);
            $statement->execute();
            $users = $this->getUsers($statement);
            return $this->getFirstResult($users);
        }

        /**
         * Read one user from the database using the username and password.
         *
         * @param $username of the logged user.
         * @param $password of the logged user.
         * @return a user if this exist, otherwise is going to return null.
         */
        public function readOneByUsernameAndPassword($username, $password)
        {
            $query =
                "SELECT *"
                    . " FROM user"
                    . " WHERE username = :username AND"
                    . " password = :password"
                    . " LIMIT 0, 1";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":username", $username);
            $statement->bindParam(":password", $password);
            $statement->execute();
            $users = $this->getUsers($statement);
            return $this->getFirstResult($users);
        }

        /**
         * Allows to change the password for a specific username.
         *
         * @param $username of the logged user.
         * @param $newPassword of the logged user.
         * @return bool true if the password was changed correctly, otherwise is going to return
         * false
         */
        public function changePassword($username, $newPassword)
        {
            $query =
                "UPDATE user"
                    . " SET password = :password"
                    . " WHERE username = :username";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":password", $newPassword);
            $statement->bindParam(":username", $username);
            $statement->execute();

            return $statement->rowCount() > 0;
        }

        private function getUsers($statement)
        {
            $totalRows = $statement->rowCount();
            $users = array();
            if ($totalRows > 0)
            {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC))
                {
                    extract($row);
                    $role = $this->roleDao->readOne($idRole);
                    $userByAgent = $this->userByAgentDao->readOne($id);

                    $user = new User();
                    $user->id = intval($id);
                    $user->username = $username;
                    $user->password = $password;
                    $user->plaqueAgent = $userByAgent != null ? $userByAgent->plaqueAgent : "";
                    $user->idRole = $idRole;
                    $user->role = $role;
                    $user->userByAgent = $userByAgent;
                    array_push($users, $user);
                }
            }
            return $users;
        }
    }