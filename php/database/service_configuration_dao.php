<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('database.php');
    include_once('../../model/service_configuration.php');

    class ServiceConfigurationDao
    {
        private $conn;
        private $database;

        // constructor
        public function __construct()
        {
            $database= new Database();
            $this->conn = $database->getConnection();
        }

        private function buildServiceConfiguration($id, $idService, $estimateAgentsQty, $startService, $endService)
        {
            $serviceConfiguration = new ServiceConfiguration();
            $serviceConfiguration->id = $id;
            $serviceConfiguration->idService = $idService;
            $serviceConfiguration->estimateAgentsQty = $estimateAgentsQty;
            $serviceConfiguration->startService = $startService;
            $serviceConfiguration->endService = $endService;
            return $serviceConfiguration;
        }

        // read service configuration
        public function readAll()
        {
            // select all query
            $query = "SELECT * FROM serviceConfiguration";
            // prepare query statement
            $statement = $this->conn->prepare($query);
            // execute query
            $statement->execute();

            return $this->getServiceConfigurations($statement);
        }

        private function executeStatement($statement)
        {
            if ($statement->execute())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private function getServiceConfigurations($statement)
        {
            $totalRows = $statement->rowCount();

            $serviceConfigurations = array();

            if ($totalRows > 0)
            {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC))
                {
                    extract($row);
                    $serviceConfiguration = $this->buildServiceConfiguration($id, $idService, $estimateAgentsQty, $startService, $endService);
                    array_push($serviceConfigurations, $serviceConfiguration);
                }
            }
            return $serviceConfigurations;
        }
    }
?>