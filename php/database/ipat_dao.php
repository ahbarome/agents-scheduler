<?php

    /*
    * Copyright (c) 2018 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('base_dao.php');
    include_once('../../model/ipat.php');

    class IpatDao extends BaseDao
    {
        public function __construct()
        {
            parent::__construct();
        }

        public function readAll()
        {
            $query = "SELECT * FROM ipat";
            $statement = $this->connection->prepare($query);
            $statement->execute();
            return $this->getIpats($statement);
        }

        public function readOne($id)
        {
            $query = "SELECT * FROM ipat WHERE id = :id";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":id", $id);
            $statement->execute();
            $ipats = $this->getIpats($statement);
            return $this->getFirstResult($ipats);
        }

        public function readCurrent()
        {
            $query = "SELECT * FROM ipat WHERE isCurrent = 1 ORDER BY finalNumber DESC LIMIT 0, 1";
            $statement = $this->connection->prepare($query);
            $statement->execute();
            $ipats = $this->getIpats($statement);
            return $this->getFirstResult($ipats);
        }

        public function readFirstInitialNumberGreatherOrEqualTo($searchedNumber)
        {
            $query =
                "SELECT * FROM ipat"
                    ." WHERE initialNumber >= :searchedNumber"
                    ." ORDER BY initialNumber ASC"
                    ." LIMIT 0, 1";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":searchedNumber", $searchedNumber);
            $statement->execute();
            $ipats = $this->getIpats($statement);
            return $this->getFirstResult($ipats);
        }

        public function readFirstFinalNumberLessThan($searchedNumber)
        {
            $query =
                "SELECT * FROM ipat"
                    ." WHERE finalNumber < :searchedNumber"
                    ." ORDER BY finalNumber DESC"
                    ." LIMIT 0, 1";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":searchedNumber", $searchedNumber);
            $statement->execute();
            $ipats = $this->getIpats($statement);
            return $this->getFirstResult($ipats);
        }

        public function readLastTwoIpatRanges()
        {
            $query =
                "SELECT *"
                    . " FROM ipat"
                    . " ORDER BY isCurrent DESC, finalNumber DESC"
                    . " LIMIT 0, 2";
            $statement = $this->connection->prepare($query);
            $statement->execute();
            return $this->getIpats($statement);
        }

        private function getIpats($statement)
        {
            $totalRows = $statement->rowCount();
            $ipats = array();
            if ($totalRows > 0)
            {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC))
                {
                    extract($row);
                    $ipat = new Ipat();
                    $ipat->id = intval($id);
                    $ipat->initialNumber = intval($initialNumber);
                    $ipat->finalNumber = intval($finalNumber);
                    $ipat->assignmentDate = $assignmentDate;
                    $ipat->isCurrent = $isCurrent;
                    $ipat->prefix = $prefix;
                    array_push($ipats, $ipat);
                }
            }
            return $ipats;
        }
    }
