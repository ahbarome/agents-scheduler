<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('base_dao.php');
    include_once('../../model/ipat_classification.php');

    class IpatClassificationDao extends BaseDao
    {
        public function __construct()
        {
            parent::__construct();
        }

        public function readOne($id)
        {
            $query = "SELECT *
                        FROM ipatClassification
                        WHERE id =:id
                        LIMIT 0,1";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":id", $id);
            $statement->execute();
            $classifications = $this->getClassifications($statement);
            return $this->getFirstResult($classifications);
        }

        public function readAll()
        {
            $query = "SELECT * FROM ipatClassification";
            $statement = $this->connection->prepare($query);
            $statement->execute();
            return $this->getClassifications($statement);
        }

        private function getClassifications($statement)
        {
            $totalRows = $statement->rowCount();
            $ipatClassifications = array();
            if ($totalRows > 0)
            {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC))
                {
                    extract($row);
                    $classification = new IpatClassification();
                    $classification->id = $id;
                    $classification->name = $name;
                    $classification->createdAt = $createdAt;
                    $classification->createdBy = $createdBy;
                    $classification->modifiedAt = $modifiedAt;
                    $classification->modifiedBy = $modifiedBy;
                    array_push($ipatClassifications, $classification);
                }
            }
            return $ipatClassifications;
        }
    }
?>