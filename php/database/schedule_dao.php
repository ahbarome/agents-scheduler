<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('database.php');
    include_once('../../model/schedule_detail.php');
    include_once('../../builder/schedule_builder.php');

    class ScheduleDao
    {
        private $conn;
        private $database;
        private $scheduleBuilder;
        // constructor with $db as database connection
        public function __construct()
        {
            $database= new Database();
            $this->conn = $database->getConnection();
            $this->scheduleBuilder = new ScheduleBuilder();
        }

        // create schedule
        function createSchedule($schedule)
        {
            // query to insert record
            $query =
                "INSERT INTO schedule SET    scheduleDate=:scheduleDate, registerDate=:registerDate";

            // prepare query
            $statement = $this->conn->prepare($query);

            // sanitize
            $schedule->scheduleDate=htmlspecialchars(strip_tags($schedule->scheduleDate));
            $schedule->registerDate=htmlspecialchars(strip_tags($schedule->registerDate));

            // bind values
            $statement->bindParam(":scheduleDate", $schedule->scheduleDate);
            $statement->bindParam(":registerDate", $schedule->registerDate);

            return $this->executeStatement($statement);
        }

        // create schedule detail
        function createScheduleDetail($scheduleDetail)
        {
            // query to insert record
            $query =
                "INSERT INTO scheduleDetail SET    scheduleDate=:scheduleDate, idServiceConfiguration=:idServiceConfiguration, plaqueAgent=:plaqueAgent";

            // prepare query
            $statement = $this->conn->prepare($query);

            // sanitize
            $scheduleDetail->scheduleDate=htmlspecialchars(strip_tags($scheduleDetail->scheduleDate));
            $scheduleDetail->idServiceConfiguration=htmlspecialchars(strip_tags($scheduleDetail->idServiceConfiguration));
            $scheduleDetail->plaqueAgent=htmlspecialchars(strip_tags($scheduleDetail->plaqueAgent));

            // bind values
            $statement->bindParam(":scheduleDate", $scheduleDetail->scheduleDate);
            $statement->bindParam(":idServiceConfiguration", $scheduleDetail->idServiceConfiguration);
            $statement->bindParam(":plaqueAgent", $scheduleDetail->plaqueAgent);

            return $this->executeStatement($statement);
        }

        public function readAllSchedules()
        {
            // select all query
            $query = "SELECT * FROM schedule ORDER BY scheduleDate DESC, registerDate DESC ";
            // prepare query statement
            $statement = $this->conn->prepare($query);
            // execute query
            $statement->execute();

            return $this->getSchedules($statement);
        }

        public function readAllScheduleDetails($scheduleDate)
        {
            // select all query
            $query = "SELECT scheduledetail.*,
                                location.description locationName,
                                sector.name sectorName,
                                agent.firstName agentFirstName,
                                agent.lastName agentLastName,
                                serviceconfiguration.startService,
                                serviceconfiguration.endService
                                FROM scheduledetail
                                LEFT JOIN serviceconfiguration ON serviceconfiguration.id = scheduledetail.idServiceConfiguration
                                LEFT JOIN service ON service.id = serviceconfiguration.idService
                                LEFT JOIN location ON location.id = service.id
                                LEFT JOIN sector ON location.idSector = sector.id
                                LEFT JOIN agent ON agent.plaque = scheduledetail.plaqueAgent
                                WHERE scheduleDate=:scheduleDate
                                ORDER BY sector.name";
            // prepare query statement

            $statement = $this->conn->prepare($query);

            $statement->bindParam(":scheduleDate", $scheduleDate);

            // execute query
            $statement->execute();

            return $this->getScheduleDetails($statement);
        }

        public function readOneScheduleDetail($scheduleDate)
        {
            // select all query
            $query = "SELECT * FROM scheduleDetail WHERE scheduleDate=:scheduleDate ORDER BY scheduleDate DESC, registerDate DESC LIMIT 0,1";
            // prepare query statement
            $statement = $this->conn->prepare($query);

            $statement->bindParam(":scheduleDate", $scheduleDate);

            // execute query
            $statement->execute();

            return $this->getSchedules($statement);
        }

        private function getSchedules($statement)
        {
            $totalRows = $statement->rowCount();

            $schedules = array();

            if ($totalRows > 0)
            {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC))
                {
                    extract($row);
                    $schedule =
                        $this->scheduleBuilder->buildSchedule(
                            $scheduleDate,
                            $registerDate);
                    array_push($schedules, $schedule);
                }
            }
            return $schedules;
        }

        private function getScheduleDetails($statement)
        {
            $totalRows = $statement->rowCount();

            $scheduleDetails = array();

            if ($totalRows > 0)
            {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC))
                {
                    extract($row);
                    $scheduleDetail =
                        $this->scheduleBuilder->buildScheduleDetailComplete(
                            $scheduleDate,
                            $idServiceConfiguration,
                            $plaqueAgent,
                        $agentFirstName,
                            $agentLastName,
                            $locationName,
                            $sectorName,
                            $startService,
                            $endService);
                    array_push($scheduleDetails, $scheduleDetail);
                }
            }
            return $scheduleDetails;
        }

        private function executeStatement($statement)
        {
            if ($statement->execute())
            {
                return true;
            }
            return false;
        }
    }
?>