<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('agent_dao.php');
    include_once('base_dao.php');
    include_once('transit_ticket_transaction_novelty_dao.php');
    include_once('../../model/transit_ticket_transaction.php');

    class TransitTicketTransactionDao extends BaseDao
    {
        private $agentDao;
        private $noveltyDao;

        public function __construct()
        {
            parent::__construct();
            $this->agentDao = new AgentDao();
            $this->noveltyDao = new TransitTicketTransactionNoveltyDao();
        }

        public function save($transaction)
        {
            $query =
                "INSERT INTO transitTicketTransaction"
                    . " SET"
                    . " talonaryNumber = :talonaryNumber,"
                    . " ticketNumber = :ticketNumber,"
                    . " diligenceDate = :diligenceDate,"
                    . " idTransactionState = :idTransactionState,"
                    . " infractorCode = :infractorCode,"
                    . " plaqueAgent = :plaqueAgent,"
                    . " createdBy = :createdBy,"
                    . " createdAt = :createdAt";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":talonaryNumber", $transaction->talonaryNumber);
            $statement->bindParam(":ticketNumber", $transaction->ticketNumber);
            $statement->bindParam(":diligenceDate", $transaction->diligenceDate);
            $statement->bindParam(":idTransactionState", $transaction->idTransactionState);
            $statement->bindParam(":infractorCode", $transaction->infractorCode);
            $statement->bindParam(":plaqueAgent", $transaction->plaqueAgent);
            $statement->bindParam(":createdBy", $transaction->createdBy);
            $statement->bindParam(":createdAt", $transaction->createdAt);

            return $statement->execute();
        }

        public function update($transaction)
        {
            $query =
                "UPDATE transitTicketTransaction"
                    . " SET"
                    . " diligenceDate = :diligenceDate,"
                    . " idTransactionState = :idTransactionState,"
                    . " infractorCode = :infractorCode,"
                    . " immobilized = :immobilized,"
                    . " immobilizedSite = :immobilizedSite,"
                    . " immobilizedSubject = :immobilizedSubject,"
                    . " immobilizedSubject = :immobilizedSubject,"
                    . " plaqueAgent = :plaqueAgent,"
                    . " modifiedBy = :modifiedBy,"
                    . " modifiedAt = :modifiedAt"
                    . " WHERE talonaryNumber = :talonaryNumber"
                    . " AND ticketNumber = :ticketNumber";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":talonaryNumber", $transaction->talonaryNumber);
            $statement->bindParam(":ticketNumber", $transaction->ticketNumber);
            $statement->bindParam(":diligenceDate", $transaction->diligenceDate);
            $statement->bindParam(":idTransactionState", $transaction->idTransactionState);
            $statement->bindParam(":infractorCode", $transaction->infractorCode);
            $statement->bindParam(":immobilized", $transaction->immobilized);
            $statement->bindParam(":immobilizedSite", $transaction->immobilizedSite);
            $statement->bindParam(":immobilizedSubject", $transaction->immobilizedSubject);
            $statement->bindParam(":plaqueAgent", $transaction->plaqueAgent);
            $statement->bindParam(":modifiedBy", $transaction->modifiedBy);
            $statement->bindParam(":modifiedAt", $transaction->modifiedAt);

            return $statement->execute();
        }

        public function readOne($talonaryNumber, $ticketNumber)
        {
            $query =
                "SELECT *"
                    . " FROM transitTicketTransaction"
                    . " WHERE talonaryNumber = :talonaryNumber"
                    . " AND ticketNumber = :ticketNumber"
                    . " LIMIT 0, 1";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":talonaryNumber", $talonaryNumber);
            $statement->bindParam(":ticketNumber", $ticketNumber);
            $statement->execute();
            $tickets = $this->getTransactions($statement);
            return $this->getFirstResult($tickets);
        }

        public function readAll()
        {
            $query = "SELECT * FROM transitTicketTransaction ORDER BY ticketNumber DESC";
            $statement = $this->connection->prepare($query);
            $statement->execute();
            return $this->getTransactions($statement);
        }

        public function readAllByCriteria($filter)
        {
            $query =
                "SELECT * "
                    . "FROM transitTicketTransaction"
                    . " WHERE " . $filter->filterBy . " LIKE CONCAT('%',:valueFilter,'%')";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":valueFilter", $filter->valueFilter);
            $statement->execute();
            return $this->getTransactions($statement);
        }

        public function readLast()
        {
            $query = "SELECT * FROM transitTicketTransaction ORDER BY ticketNumber DESC LIMIT 0, 500";
            $statement = $this->connection->prepare($query);
            $statement->execute();
            return $this->getTransactions($statement);
        }

        public function countByState($idTransactionState)
        {
            $query =
                "SELECT COUNT(talonaryNumber) AS Count"
                . " FROM transitTicketTransaction"
                . " WHERE idTransactionState = :idTransactionState";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":idTransactionState", $idTransactionState);
            $statement->execute();
            return $this->getCount($statement);
        }

        public function countByAssignedAndState($assignedTo, $idTransactionState)
        {
            $query =
                "SELECT COUNT(talonaryNumber) AS Count"
                    . " FROM transitTicketTransaction"
                    . " WHERE plaqueAgent = :plaqueAgent"
                    . " AND idTransactionState = :idTransactionState";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":plaqueAgent", $assignedTo);
            $statement->bindParam(":idTransactionState", $idTransactionState);
            $statement->execute();
            return $this->getCount($statement);
        }

        public function countByTalonary($talonaryNumber)
        {
            $query =
                "SELECT COUNT(talonaryNumber) AS Count"
                    . " FROM transitTicketTransaction"
                    . " WHERE talonaryNumber = :talonaryNumber";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":talonaryNumber", $talonaryNumber);
            $statement->execute();
            return $this->getCount($statement);
        }

        public function countByTalonaryAndState($talonaryNumber, $idTransactionState)
        {
            $query =
                "SELECT COUNT(talonaryNumber) AS Count"
                    . " FROM transitTicketTransaction"
                    . " WHERE talonaryNumber = :talonaryNumber"
                    . " AND idTransactionState = :idTransactionState";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":talonaryNumber", $talonaryNumber);
            $statement->bindParam(":idTransactionState", $idTransactionState);
            $statement->execute();
            return $this->getCount($statement);
        }

        public function countAll()
        {
            $query =
                "SELECT COUNT(talonaryNumber) AS Count"
                    . " FROM transitTicketTransaction";
            $statement = $this->connection->prepare($query);
            $statement->execute();
            return $this->getCount($statement);
        }

        public function readDistinctAgentPlaques()
        {
            $query =
                "SELECT DISTINCT(plaqueAgent) AS plaqueAgent"
                    . " FROM transitTicketTransaction";
            $statement = $this->connection->prepare($query);
            $statement->execute();
            $totalRows = $statement->rowCount();
            $plaques = array();
            if ($totalRows > 0)
            {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC))
                {
                    extract($row);
                    array_push($plaques, $plaqueAgent);
                }
            }
            return $plaques;
        }

        private function getTransactions($statement)
        {
            $totalRows = $statement->rowCount();
            $transactions = array();
            if ($totalRows > 0)
            {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC))
                {
                    extract($row);
                    $transaction = new TransitTicketTransaction();
                    $transaction->talonaryNumber = intval($talonaryNumber);
                    $transaction->ticketNumber = intval($ticketNumber);

                    $transaction->diligenceDate = $diligenceDate;
                    $transaction->infractorCode = $infractorCode;
                    
                    $transaction->plaqueAgent = $plaqueAgent;
                    $transaction->agent = $this->agentDao->readOne($plaqueAgent);

                    $transaction->immobilized = $immobilized;
                    $transaction->immobilizedSubject = $immobilizedSubject;
                    $transaction->immobilizedSite = $immobilizedSite;
                    $transaction->createdBy = $createdBy;
                    $transaction->createdAt = $createdAt;
                    $transaction->modifiedBy = $modifiedBy;
                    $transaction->modifiedAt = $modifiedAt;

                    $transaction->totalNovelties =
                        $this->noveltyDao->count($talonaryNumber, $ticketNumber);

                    array_push($transactions, $transaction);
                }
            }
            return $transactions;
        }
    }
