<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ('Confidential Information').
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('database.php');

    class BaseDao
    {
        protected $connection;
        protected $database;

        public function __construct()
        {
            $this->database = new Database();
            $this->connection = $this->database->getConnection();
        }

        protected function execute($statement)
        {
            $executed = $statement->execute();
            $this->database->closeConnection();
            return $executed;
        }

        protected function bindProperties($statement, $entity)
        {
            if (is_array($entity))
            {
                foreach ($entity as $property => $value)
                {
                    $statement->bindParam(':' . $property, $value);
                }
            } else
            {
                throw new Exception('Not possible to bind entity');
            }
        }

        protected function getCount($statement)
        {
            $count = 0;
            if ($statement->rowCount() > 0)
            {
                $row = $statement->fetch(PDO::FETCH_ASSOC);
                $count = intval($row['Count']);
            }
            return $count;
        }

        /**
         * Get the first result of the collection.
         *
         * @param $collection with the result of the execution of the query.
         * @return null if the length of collection is less or equal to 0, first object in
         * the collection otherwise.
         */
        protected function getFirstResult($collection)
        {
            return $collection != null
                && count($collection) > 0 ? array_values($collection)[0] : null;
        }
    }