<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('agent_dao.php');
    include_once('base_dao.php');
    include_once('../../model/ipat_incharge.php');

    class IpatInChargeDao extends BaseDao
    {
        private $agentDao;

        public function __construct()
        {
            parent::__construct();
            $this->agentDao = new AgentDao();
        }

        public function readAll()
        {
            $query = "SELECT * FROM ipatInCharge";
            $statement = $this->connection->prepare($query);
            $statement->execute();
            return $this->getIpatInCharges($statement);
        }

        public function readOne($idUser)
        {
            $query = "SELECT * FROM ipatInCharge
                    WHERE idUser = :idUser LIMIT 0, 1";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":idUser", $idUser);
            $statement->execute();
            $inCharges = $this->getIpatInCharges($statement);
            return $this->getFirstResult($inCharges);
        }

        public function readOneById($id)
        {
            $query =
                "SELECT * FROM ipatInCharge"
                    . " WHERE id = :id LIMIT 0, 1";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":id", $id);
            $statement->execute();
            $inCharges = $this->getIpatInCharges($statement);
            return $this->getFirstResult($inCharges);
        }

        public function readOneByIdUserAndPlaque($idUser, $plaque)
        {
            $query =
                "SELECT * FROM ipatInCharge"
                    . " WHERE idUser = :idUser"
                    . " AND plaqueAgent = :plaque LIMIT 0, 1";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":idUser", $idUser);
            $statement->bindParam(":plaque", $plaque);
            $statement->execute();
            $inCharges = $this->getIpatInCharges($statement);
            return $this->getFirstResult($inCharges);
        }

        public function readOneByUsername($username)
        {
            $query =
                "SELECT * FROM ipatInCharge incharge"
                    . " WHERE idUser = (SELECT id FROM user WHERE username = :username LIMIT 0, 1)";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":username", $username);
            $statement->execute();
            $inCharges = $this->getIpatInCharges($statement);
            return $this->getFirstResult($inCharges);
        }

        private function getIpatInCharges($statement)
        {
            $totalRows = $statement->rowCount();
            $ipatInCharges = array();
            if ($totalRows > 0)
            {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC))
                {
                    extract($row);
                    $inCharge = new IpatInCharge();
                    $inCharge->idUser = $idUser;
                    $inCharge->plaqueAgent = $plaqueAgent;
                    $inCharge->createdAt = $createdAt;
                    $inCharge->createdBy = $createdBy;
                    $inCharge->modifiedAt = $modifiedAt;
                    $inCharge->modifiedBy = $modifiedBy;
                    $inCharge->agent = $this->agentDao->readOne($plaqueAgent);
                    array_push($ipatInCharges, $inCharge);
                }
            }
            return $ipatInCharges;
        }
    }
?>