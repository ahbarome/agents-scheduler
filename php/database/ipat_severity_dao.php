<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('base_dao.php');
    include_once('../../model/ipat_severity.php');

    class IpatSeverityDao extends BaseDao
    {
        public function __construct()
        {
            parent::__construct();
        }

        public function readAll()
        {
            $query = "SELECT * FROM ipatSeverity";
            $statement = $this->connection->prepare($query);
            $statement->execute();
            return $this->getIpatSeverities($statement);
        }

        public function readOne($id)
        {
            $query = "SELECT *
                        FROM ipatSeverity
                        WHERE id =:id
                        LIMIT 0,1";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":id", $id);
            $statement->execute();
            $transactions = $this->getIpatSeverities($statement);
            return $this->getFirstResult($transactions);
        }

        private function getIpatSeverities($statement)
        {
            $totalRows = $statement->rowCount();
            $ipatSeverities = array();
            if ($totalRows > 0)
            {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC))
                {
                    extract($row);
                    $ipatSeverity = new IpatSeverity();
                    $ipatSeverity->id = intval($id);
                    $ipatSeverity->description = $description;
                    array_push($ipatSeverities, $ipatSeverity);
                }
            }
            return $ipatSeverities;
        }
    }
?>