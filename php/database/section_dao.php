<?php

    /*
    * Copyright (c) 2018 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('base_dao.php');
    include_once('../../model/section.php');

    final class SectionDao extends BaseDao
    {
        public function __construct()
        {
            parent::__construct();
        }

        public function export_CSV()
        {
            $query = "SELECT * FROM section";
            $statement = $this->connection->prepare($query);
            $this->execute($statement);
            $out = "# Sección,Nombre\n";

            if ($statement->rowCount() > 0)
            {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC))
                {
                    extract($row);
                    $out.="{$id},\"{$name}\n";
                }
            }
            return $out;
        }

        public function save($section)
        {
            $query = "INSERT INTO section SET id=:id, name=:name";
            $statement = $this->connection->prepare($query);
            $section->id = htmlspecialchars(strip_tags($section->id));
            $section->name = htmlspecialchars(strip_tags($section->name));
            $statement->bindParam(":id", $section->id);
            $statement->bindParam(":name", $section->name);

            return $this->execute($statement);
        }

        public function readAll()
        {
            $query = "SELECT * FROM section";
            $statement = $this->connection->prepare($query);
            $statement->execute();
            return $this->getSections($statement);
        }

        public function readOne($id)
        {
            $query = "SELECT * FROM section WHERE id = :id LIMIT 0,1";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":id", $id);
            $this->execute($statement);
            $sections = $this->getSections($statement);
            return $this->getFirstResult($sections);
        }

        public function update($section)
        {
            $query = "UPDATE section SET name = :name WHERE id = :id";

            $statement = $this->connection->prepare($query);
            $section->id = htmlspecialchars(strip_tags($section->id));
            $section->name = htmlspecialchars(strip_tags($section->name));
            $statement->bindParam(":id", $section->id);
            $statement->bindParam(":name", $section->name);

            return $this->execute($statement);
        }

        public function delete($id)
        {
            $query = "DELETE FROM section WHERE id = ?";
            $statement = $this->connection->prepare($query);
            $id = htmlspecialchars(strip_tags($id));

            $statement->bindParam(1, $id);
            return $this->execute($statement);
        }

        public function deleteSelected($ids)
        {
            $in_ids = str_repeat('?,', count($ids) - 1) . '?';
            $query = "DELETE FROM section WHERE id IN ({$in_ids})";
            $statement = $this->connection->prepare($query);

            $counter = 0;
            while($counter < count($ids))
            {
                $statement->bindParam($counter + 1 , $ids[$counter]);
                $counter++;
            }

            return $this->execute($statement);
        }

        private function getSections($statement)
        {
            $sections = array(); 
            if ($statement->rowCount())
            {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC))
                {
                    extract($row);
                    $section = new Section();
                    $section->id = $id;
                    $section->name = $name;
                    array_push($sections, $section);
                }
            }
            return $sections;
        }
    }
?>