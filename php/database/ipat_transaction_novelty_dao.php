<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('base_dao.php');
    include_once('../../model/ipat_transaction_novelty.php');

    class IpatTransactionNoveltyDao extends BaseDao
    {
        public function __construct()
        {
            parent::__construct();
        }

        public function save($novelty)
        {
            $query =
                "INSERT INTO ipatTransactionNovelty"
                    . " SET"
                    . " ipatNumber = :ipatNumber,"
                    . " diligenceDate = :diligenceDate,"
                    . " pstDeliveryDate = :pstDeliveryDate,"
                    . " observation = :observation";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":ipatNumber", $novelty->ipatNumber);
            $statement->bindParam(":diligenceDate", $novelty->diligenceDate);
            $statement->bindParam(":pstDeliveryDate", $novelty->pstDeliveryDate);
            $statement->bindParam(":observation", $novelty->observation);

            return $statement->execute();
        }

        public function count($ipatNumber)
        {
            $query =
                "SELECT COUNT(idNovelty) AS Count"
                    . " FROM ipatTransactionNovelty"
                    . " WHERE ipatNumber = :ipatNumber";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":ipatNumber", $ipatNumber);
            $statement->execute();
            $count = 0;
            if ($statement->rowCount() > 0)
            {
                $row = $statement->fetch(PDO::FETCH_ASSOC);
                $count = intval($row['Count']);
            }
            return $count;
        }
    }
