<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('base_dao.php');

    class SupportDao extends BaseDao
    {
        /**
         * SupportDao constructor.
         */
        public function __construct()
        {
            parent::__construct();
        }

        public function executeQuery($query)
        {
            $statement = $this->connection->prepare($query);
            $this->execute($statement);
            return $this->getData($statement);
        }

        /**
         * Generate an array with all the data in the current statement.
         *
         * @param $statement with the query to be executed.
         * @return array with all the data.
         */
        private function getData($statement)
        {
            $rows = array();
            if ($statement->rowCount() > 0)
            {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC))
                {
                    extract($row);
                    array_push($rows, $row);
                }
            }
            return $rows;
        }
    }