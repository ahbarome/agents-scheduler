<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once(__DIR__ . '/base_dao.php');
    include_once(__DIR__ . '/database.php');
    include_once(__DIR__ . '/../model/agent.php');

    class AgentDao extends BaseDao
    {
        /**
         * AgentDao constructor.
         */
        public function __construct()
        {
           parent::__construct();
        }

        /**
         * Create an agent in the database.
         *
         * @param $agent to create.
         * @return bool true if the agent was created successfully, false otherwise.
         */
        public function create($agent)
        {
            $query =
                "INSERT INTO agent"
                    . " SET plaque = :plaque,"
                    . " firstName = :firstName,"
                    . " lastName = :lastName,"
                    . " idSection = :idSection,"
                    . " isActive = :isActive";

            $statement = $this->connection->prepare($query);
            $agent->plaque=htmlspecialchars(strip_tags($agent->plaque));
            $agent->firstName=htmlspecialchars(strip_tags($agent->firstName));
            $agent->lastName=htmlspecialchars(strip_tags($agent->lastName));
            $agent->idSection=htmlspecialchars(strip_tags($agent->idSection));
            $agent->isActive=htmlspecialchars(strip_tags($agent->isActive));

            $statement->bindParam(":plaque", $agent->plaque);
            $statement->bindParam(":firstName", $agent->firstName);
            $statement->bindParam(":lastName", $agent->lastName);
            $statement->bindParam(":idSection", $agent->idSection);
            $statement->bindParam(":isActive", $agent->isActive);

            return $statement->execute();
        }

        /**
         * Read all the agents from the database.
         *
         * @return array of agents.
         */
        public function readAll()
        {
            $query =
                "SELECT AGE.*,"
                    . " SEC.Name AS sectionName"
                    . " FROM agent AGE"
                    . " LEFT JOIN section SEC ON SEC.Id = AGE.IdSection";
            $statement = $this->connection->prepare($query);
            $this->execute($statement);
            $agents = $this->getAgents($statement);
            return $agents;
        }

        /**
         * Read all the active agents from the database.
         *
         * @return array of active agents.
         */
        function readAllActive()
        {
            $query =
                "SELECT AGE.*,"
                    . " SEC.Name AS sectionName"
                    . " FROM agent AGE"
                    . " LEFT JOIN section SEC ON SEC.Id = AGE.IdSection"
                    . " WHERE AGE.isActive = 1";

            $statement = $this->connection->prepare($query);
            $statement->execute();

            return $this->getAgents($statement);
        }

        public function readAllActiveByPlaques($plaques)
        {
            $agents = array();
            if ($plaques != null)
            {
                $inStatement = str_repeat('?,', count($plaques) - 1) . '?';
                $query =
                    "SELECT AGE.*,"
                        . " SEC.Name AS sectionName"
                        . " FROM agent AGE"
                        . " LEFT JOIN section SEC ON SEC.Id = AGE.IdSection"
                        . " WHERE AGE.isActive = 1"
                        . " AND AGE.plaque IN ({$inStatement})";
                $statement = $this->connection->prepare($query);
                $counter = 0;
                while ($counter < count($plaques))
                {
                    $statement->bindParam($counter + 1 , $plaques[$counter]);
                    $counter++;
                }
                $statement->execute();
                $agents = $this->getAgents($statement);
            }
            return $agents;
        }

        /**
         * Read one agent from the database.
         *
         * @param $plaque of the agent.
         * @return Agent according to the plaque.
         */
        function readOne($plaque)
        {
            $query =
                "SELECT AGE.*"
                    . " FROM agent AGE"
                    . " WHERE AGE.plaque = :plaque"
                    . " LIMIT 0,1";

            $statement = $this->connection->prepare( $query );
            $statement->bindParam(":plaque", $plaque);
            $statement->execute();
            $row = $statement->fetch(PDO::FETCH_ASSOC);

            $agent =
                $this->buildAgent(
                    $row['plaque'],
                    $row['firstName'],
                    $row['lastName'],
                    $row['idSection'],
                    $row['isActive'],
                    null);

            return $agent;
        }

        /**
         * Update an agent.
         *
         * @param $agent to be updated.
         * @return bool true if the agent was updated successfully, false otherwise.
         */
        function update($agent)
        {
            $query =
                "UPDATE agent"
                    . " SET firstName = :firstName,"
                    . " lastName = :lastName,"
                    . " idSection = :idSection,"
                    . " isActive = :isActive"
                    . " WHERE plaque = :plaque";

            $statement = $this->connection->prepare($query);

            $agent->plaque = htmlspecialchars(strip_tags($agent->plaque));
            $agent->firstName = htmlspecialchars(strip_tags($agent->firstName));
            $agent->lastName = htmlspecialchars(strip_tags($agent->lastName));
            $agent->idSection = htmlspecialchars(strip_tags($agent->idSection));
            $agent->isActive = htmlspecialchars(strip_tags($agent->isActive));

            $statement->bindParam(":plaque", $agent->plaque);
            $statement->bindParam(":firstName", $agent->firstName);
            $statement->bindParam(":lastName", $agent->lastName);
            $statement->bindParam(":idSection", $agent->idSection);
            $statement->bindParam(":isActive", $agent->isActive);

            return $statement->execute();
        }

        /**
         * Delete an agent from the database.
         *
         * @param $plaque of the agent to be deleted.
         * @return bool true if the agent was deleted successfully, false otherwise.
         */
        function delete($plaque)
        {
            $query = "DELETE FROM agent WHERE plaque = ?";
            $statement = $this->connection->prepare($query);
            $plaque = htmlspecialchars(strip_tags($plaque));
            $statement->bindParam(1, $plaque);
            return $statement->execute();
        }

        /**
         * Delete several agents by ids.
         *
         * @param $ids of the agents to be deleted.
         * @return bool true if the agents were deleted successfully, false otherwise.
         */
        public function deleteSelected($ids)
        {
            $in_ids = str_repeat('?,', count($ids) - 1) . '?';
            $query = "DELETE FROM agent WHERE plaque IN ({$in_ids})";
            $statement = $this->connection->prepare($query);

            $counter = 0;
            while($counter < count($ids))
            {
                $statement->bindParam($counter + 1 , $ids[$counter]);
                $counter++;
            }

            return $statement->execute();
        }

        /**
         * Export all the agents from the database.
         *
         * @return string with all the agents from the database.
         */
        public function export_CSV()
        {
            $query =
                "SELECT AGE.*,"
                    . " SEC.Name AS sectionName"
                    . " FROM agent AGE"
                    . " LEFT JOIN section SEC ON SEC.Id = AGE.IdSection";
            $statement = $this->connection->prepare($query);
            $statement->execute();
            $num = $statement->rowCount();
            $out = utf8_decode("Placa,Nombre(s),Apellido(s),Sección,Activo?\n");

            if ($num > 0)
            {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC))
                {
                    extract($row);
                    $out.=
                        utf8_decode(
                            "{$plaque},\"{$firstName}\",\"{$lastName}\",{$sectionName},{$isActive}\n");
                }
            }
            return $out;
        }

        public function readOneByUserId($userId)
        {
            $query =
                "SELECT AGE.*,"
                . " SEC.Name AS sectionName"
                . " FROM agent AGE"
                . " LEFT JOIN section SEC ON SEC.Id = AGE.IdSection"
                . " WHERE AGE.plaque = ( SELECT plaqueAgent "
                    . " FROM userByAgent"
                    . " WHERE idUser = :idUser"
                    . " LIMIT 0,1)"
                . " LIMIT 0,1";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":idUser", $userId);
            $statement->execute();
            $agents = $this->getAgents($statement);
            return $this->getFirstResult($agents);
        }

        private function getAgents($statement)
        {
            $totalRows = $statement->rowCount();
            $agents = array();

            if ($totalRows > 0)
            {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC))
                {
                    extract($row);
                    $agent =
                        $this->buildAgent(
                            $plaque, $firstName, $lastName, $idSection, $isActive, $sectionName);
                    array_push($agents, $agent);
                }
            }
            return $agents;
        }

        private function buildAgent(
            $plaque, $firstName, $lastName, $idSection, $isActive, $sectionName)
        {
            $agent = new Agent($this->database);
            $agent->plaque = $plaque;
            $agent->firstName = htmlentities($firstName);
            $agent->lastName = htmlentities($lastName);
            $agent->idSection = $idSection;
            $agent->isActive = $isActive;
            $agent->sectionName = $sectionName;
            return $agent;
        }
    }