<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('agent_dao.php');
    include_once('base_dao.php');
    include_once('../../model/transit_ticket_talonary.php');

    class TransitTicketTalonaryDao extends BaseDao
    {
        private $agentDao;

        public function __construct()
        {
            parent::__construct();
            $this->agentDao = new AgentDao();
        }

        public function save($talonary)
        {
            $query =
                "INSERT INTO transitTicketTalonary"
                    . " SET"
                    . " talonaryNumber = :talonaryNumber,"
                    . " initialNumber = :initialNumber,"
                    . " finalNumber = :finalNumber,"
                    . " assignedTo = :assignedTo,"
                    . " deliveryDate = :deliveryDate,"
                    . " createdBy = :createdBy,"
                    . " createdAt = :createdAt";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":talonaryNumber", $talonary->talonaryNumber);
            $statement->bindParam(":initialNumber", $talonary->initialNumber);
            $statement->bindParam(":finalNumber", $talonary->finalNumber);
            $statement->bindParam(":assignedTo", $talonary->assignedTo);
            $statement->bindParam(":deliveryDate", $talonary->deliveryDate);
            $statement->bindParam(":createdBy", $talonary->createdBy);
            $statement->bindParam(":createdAt", $talonary->createdAt);

            return $statement->execute();
        }

        public function readOne($talonaryNumber)
        {
            $query =
                "SELECT *"
                    . " FROM transitTicketTalonary"
                    . " WHERE talonaryNumber = :talonaryNumber"
                    . " LIMIT 0, 1";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":talonaryNumber", $talonaryNumber);
            $statement->execute();
            $tickets = $this->getTransitTicketTalonaries($statement);
            return $this->getFirstResult($tickets);
        }

        public function readAll()
        {
            $query = "SELECT * FROM transitTicketTalonary ORDER BY talonaryNumber DESC";
            $statement = $this->connection->prepare($query);
            $statement->execute();
            return $this->getTransitTicketTalonaries($statement);
        }

        public function readLast()
        {
            $query =
                "SELECT * FROM transitTicketTalonary ORDER BY talonaryNumber DESC LIMIT 0, 500";
            $statement = $this->connection->prepare($query);
            $statement->execute();
            return $this->getTransitTicketTalonaries($statement);
        }

        public function existTalonary($talonaryNumber)
        {
            $query =
                "SELECT COUNT(talonaryNumber) AS Count"
                    . " FROM transitTicketTalonary"
                    . " WHERE talonaryNumber = :talonaryNumber";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":talonaryNumber", $talonaryNumber);
            $statement->execute();
            return $this->getCount($statement) > 0;
        }

        public function existTalonaryRange($initialNumber, $finalNumber)
        {
            $query =
                "SELECT COUNT(talonaryNumber) AS Count"
                    . " FROM transitTicketTalonary"
                    . " WHERE initialNumber BETWEEN :initialNumber AND :finalNumber"
                    . " OR finalNumber BETWEEN :initialNumber AND :finalNumber";
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":initialNumber", $initialNumber);
            $statement->bindParam(":finalNumber", $finalNumber);
            $statement->execute();
            
            return $this->getCount($statement) > 0;
        }

        private function getTransitTicketTalonaries($statement)
        {
            $totalRows = $statement->rowCount();
            $talonaries = array();
            if ($totalRows > 0)
            {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC))
                {
                    extract($row);
                    $talonary = new TransitTicketTalonary();
                    $talonary->talonaryNumber = intval($talonaryNumber);

                    $talonary->initialNumber = $initialNumber;
                    $talonary->finalNumber = $finalNumber;
                    $talonary->assignedTo = $assignedTo;
                    $talonary->deliveryDate = $deliveryDate;
                    $talonary->createdBy = $createdBy;
                    $talonary->createdAt = $createdAt;
                    $talonary->modifiedBy = $modifiedBy;
                    $talonary->modifiedAt = $modifiedAt;
                    
                    $talonary->agentAssignedTo = $this->agentDao->readOne($assignedTo);

                    array_push($talonaries, $talonary);
                }
            }
            return $talonaries;
        }

    }

?>