<?php

    /*
    * Copyright (c) 2018 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../json_response_collection.php');
    include_once('../guid_manager.php');
    include_once('../../model/agent.php');
    include_once('../../model/role.php');
    include_once('../../model/user.php');
    include_once('../../model/ipat_delivery.php');
    include_once('../../business_layer/ipat_delivery_business_layer.php');
    include_once('../../business_layer/ipat_transaction_state_business_layer.php');
    include_once('../../action/session/session_init.php');

    error_reporting(E_ALL ^ E_WARNING);

    function getInMemoryData()
    {
        $data = array();
        for ($i = 0; $i < 10000; $i++)
        { 
            $transaction = new IpatTransaction();
            $transaction->deliveredBy = new Agent();
            $transaction->deliveredTo = new Agent();
            $transaction->partiallyReceivedTo = new Agent();
            $transaction->partiallyReceivedBy = new Agent();
            $transaction->receivedTo = new Agent();
            $transaction->receivedBy = new Agent();
            array_push($data, $transaction);
        }
        return $data;
    }

    function getDataFromBL()
    {
        $businessLayer = new IpatDeliveryBusinessLayer();
        $data = $businessLayer->readLastDeliveries();
        return $data;
    }

    function gzipCompression()
    {   
        $data = getDataFromBL();
        $compressed = JsonResponseCollection::encodeCollectionCompressed($data);
        print_r($compressed);
    }

    print(GuidManager::getGuid());
    gzipCompression();