<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    class AmericaDateTime
    {
        const DEFAULT_DATETIME_FORMAT = "Y-m-d H:i:s";
        const DEFAULT_DATE_FORMAT = "Y-m-d";
        const DEFAULT_TIME_FORMAT = "H:i:s";
        const YEAR_FORMAT = "Y";
        const MONTH_FORMAT = "m";
        const DAY_FORMAT = "d";
        const HOUR_FORMAT = "H";
        const MINUTE_FORMAT = "m";
        const SECOND_FORMAT = "s";
        const CURRENT_DATETIME = "now";
        const DAYS_DIFF = "%a";
        const TIME_ZONE = "America/Bogota";

        private $currentDateTime;

        function __construct()
        {
             $this->currentDateTime =
                new DateTime(
                    AmericaDateTime::CURRENT_DATETIME,
                    new DateTimeZone(AmericaDateTime::TIME_ZONE));
        }

        public function getCurrentDateTime()
        {
            return $this->currentDateTime;
        }

        public function getCurrentDate()
        {
            return $this->currentDateTime->format(AmericaDateTime::DEFAULT_DATE_FORMAT);
        }

        public function getCurrentTime()
        {
            return $this->currentDateTime->format(AmericaDateTime::DEFAULT_TIME_FORMAT);
        }
    }
