<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('gzip_compression.php');
    include_once('../../model/api_response.php');

    final class JsonResponseCollection
    {
        const RESPONSE_FORMAT = '{"count": %d, "records": %s}';
        const DEFAULT_RESPONSE = '{"count": 0, "records":[]}';

        public function encodeCollection($collection)
        {
            $response = new APIResponse();
            $response->count = 0;
            $count = count($collection);
            if ($count > 0)
            {
                $response->count = $count;
                $response->records = $collection;
                return json_encode($response);
            }
            return json_encode($response);
        }

        public static function encodeCollectionCompressed($collection)
        {
            $response = new APIResponse();
            $response->count = 0;
            $count = count($collection);
            if ($count > 0)
            {
                $response->count = $count;
                $response->records = $collection;
                $compressed =
                    GZipCompression::compress(
                        json_encode($response), GZipCompression::MAX_COMPRESSION);
                return $compressed;
            }
            return json_encode($response);
        }
    }
?>