<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    header('Content-Type: application/pdf');
    header('Content-Disposition: inline; filename=file.pdf');
    header('Cache-Control: private, max-age=0, must-revalidate');
    header('Pragma: public');
