<?php

    /*
    * Copyright (c) 2018 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    final class GuidManager
    {
        private function __construct()
        {

        }

        //  Set to true/false as your default way to do this.
        static function getGuid( $brackets = false )
        {
            if(function_exists('com_create_guid'))
            {
                if($brackets)
                {
                    return com_create_guid();
                }
                else
                {
                    return trim( com_create_guid(), '{}' );
                }
            }
            else
            {
                mt_srand( (double)microtime() * 10000 );
                $charid = strtoupper( md5(uniqid(rand(), true)) );
                $hyphen = chr( 45 );    // "-"
                $left_curly = $brackets ? chr(123) : "";     //  "{"
                $right_curly = $brackets ? chr(125) : "";    //  "}"
                $uuid = $left_curly
                    . substr( $charid, 0, 8 ) . $hyphen
                    . substr( $charid, 8, 4 ) . $hyphen
                    . substr( $charid, 12, 4 ) . $hyphen
                    . substr( $charid, 16, 4 ) . $hyphen
                    . substr( $charid, 20, 12 )
                    . $right_curly;
                return $uuid;
            }
        }
    }
?>