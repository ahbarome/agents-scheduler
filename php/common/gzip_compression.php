<?php

    /*
    * Copyright (c) 2018 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    final class GZipCompression
    {
        const MIN_COMPRESSION = 1;
        const MAX_COMPRESSION = 9;

        private function __construct()
        {
        }

        public static function compress($data, $level = null)
        {
            if ($level == null)
            {
                $level = GZipCompression::MIN_COMPRESSION;
            }

            error_reporting(E_ALL ^ E_WARNING);
            $supportsGzip = strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') !== false;
            
            $compressedContent = '';
            if ($supportsGzip)
            {
                ini_set('memory_limit', '1024M');
                header('Content-Encoding: gzip');
                $compressedContent = gzencode($data, $level, FORCE_GZIP);
            }
            else
            {
                $compressedContent = $data;
            }

            return $compressedContent;
        }
    }
?>