<?php
    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('interfaces/ivalidator.php');
    include_once('../../model/ipat_severity.php');

    class ReceivedIpatTransactionValidator implements IValidator
    {
        const MESSAGE_WHEN_IPAT_NUMBER_IS_NULL_OR_ZERO =
            "El número de Ipat es requerido.";
        const MESSAGE_WHEN_ACCIDENT_DATE_IS_NULL =
            "La fecha del accidente es requerida.";
        const MESSAGE_WHEN_DILIGENCE_DATE_IS_NULL =
            "La fecha de diligenciamiento es requerida.";
        const MESSAGE_WHEN_ADDRESS_IS_NULL_OR_EMPTY =
            "La dirección es requerida.";
        const MESSAGE_WHEN_RECEIVED_BY_IS_NULL_OR_EMPTY =
            "La placa del  agente que realizó la recepción es requerida.";
        const MESSAGE_WHEN_RECEIVED_TO_IS_NULL_OR_EMPTY =
            "La placa del  agente al que se le realizó la recepción es requerida.";
        const MESSAGE_WHEN_ID_SEVERITY_IS_NULL_OR_ZERO =
            "La gravedad es requerida.";
        const MESSAGE_WHEN_CRIMINAL_NEWS_IS_NULL_OR_EMPTY =
            "La noticia criminal es requerida.";

        /**
         * Perform a validation of all the properties that are required for the transaction state:
         * IpatTransactionState::RECEIVED.
         *
         * @param $transaction to perform the validation.
         * @return bool true if all the validations pass, false otherwise.
         */
        public function isValid($transaction)
        {
            if ($transaction->idTransactionState == IpatTransactionState::RECEIVED)
            {
                $areFieldsValid = $this->areFieldsValid($transaction);
                $isCriminalNewsValid =
                    $this->isCriminalNewsValid(
                        $transaction->criminalNews, $transaction->idSeverity);
                return $areFieldsValid && $isCriminalNewsValid;
            }
            return false;
        }

        private function areFieldsValid($transaction)
        {
            return $transaction->ipatNumber != null
                && $transaction->ipatNumber > 0
                && $transaction->accidentDate != null
                && $transaction->diligenceDate != null
                && $transaction->address != null
                && $transaction->address != ''
                && $transaction->receivedBy != null
                && $transaction->receivedBy != ''
                && $transaction->receivedTo != null
                && $transaction->receivedTo != ''
                && $transaction->idSeverity != null
                && $transaction->idSeverity > 0;
        }

        private function isCriminalNewsValid($criminalNews, $idSeverity)
        {
            if($idSeverity == IpatSeverity::DAMAGE)
            {
                return true;
            }
            return $criminalNews != null
                && $criminalNews != '';
        }
    }
