<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('interfaces/ivalidator.php');
    include_once('../../common/america_datetime.php');
    include_once('../../model/ipat_transaction_state.php');

    class DeliveredDateIpatTransactionValidator implements IValidator
    {
        private $allowedInterval;
        private $americaDateTime;

        const INTERVAL_SPEC_ALLOWED = 'P15D';

        /**
         * DeliveredDateIpatTransactionValidator constructor.
         */
        public function __construct()
        {
            $this->americaDateTime = new AmericaDateTime();
            $this->allowedInterval =
                new DateInterval(DeliveredDateIpatTransactionValidator::INTERVAL_SPEC_ALLOWED);
        }

        /**
         * Validate if the transaction is valid.
         *
         * @param $transaction to be validated.
         * @return bool true if the transaction is valid, false otherwise.
         */
        public function isValid($transaction)
        {
            $isValid = false;
            if ($transaction->idTransactionState != IpatTransactionState::DELIVERED)
            {
                $isValid = true;
            }
            else
            {
                $isValid = $this->isValidDeliveredDate($transaction);
            }
            return $isValid;
        }

        private function isValidDeliveredDate($transaction)
        {
            $deliveryDateTime =
                new DateTime(
                    $transaction->deliveryDate, new DateTimeZone(AmericaDateTime::TIME_ZONE));
            $diffInterval =
                $this->americaDateTime->getCurrentDateTime()->diff($deliveryDateTime);
            $isValid = ((array) $diffInterval) < ((array) $this->allowedInterval);
            return $isValid;
        }
    }
