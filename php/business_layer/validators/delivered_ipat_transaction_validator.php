<?php
    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('base_validator.php');
    include_once('interfaces/ivalidator.php');
    include_once('delivered_date_ipat_transaction_validator.php');
    include_once('partial_reception_date_ipat_transaction_validator.php');
    include_once(__DIR__ . '\..\ipat_business_layer.php');
    include_once('../../common/message_printer.php');
    include_once('../../database/ipat_delivery_dao.php');
    include_once('../../model/ipat_transaction_state.php');
    include_once('../../model/role.php');

    class DeliveredIpatTransactionValidator extends BaseValidator implements IValidator
    {
        const MAX_QUANTITY_IPATS_PER_AGENT = 5;
        const MAX_DAYS_ALLOWED_TO_DELIVER_IPATS = 15;
        const MAX_DAYS_ALLOWED_TO_DELIVER_IPATS_PREVIOUS_RANGE = 15;
        const MAX_DAYS_ALLOWED_TO_RECEIVE_PARTIALLY_IPATS = 1;
        const MESSAGE_WHEN_QUANTITY_IS_NULL_OR_ZERO =
            "La cantidad de ipats a entregar es requerida.";
        const MESSAGE_WHEN_DELIVERED_TO_IS_NULL_OR_ZERO =
            "El agente al que se le desea entregar es requerido.";
        const MESSAGE_WHEN_DELIVERED_BY_IS_NULL_OR_ZERO =
            "El agente al que entregó es requerido, por favor válide la configuración del"
                . " sistema.";
        const MESSAGE_WHEN_FIELDS_ARE_MISSING =
            "La información suministrada se encuentra incompleta.";
        const MESSAGE_FORMAT_WHEN_AGENT_HAS_MORE_IPATS_IN_DELIVERED_STATE_THAN_THE_CONFIGURED =
            "NO SE PUEDE. Al agente %s se le han entregado %s IPATs. Por configuración máximo"
                . " se pueden entregar %s IPATs.";
        const MESSAGE_FORMAT_WHEN_AGENT_HAS_IPATS_IN_DELIVERED_STATE_WITH_MORE_DAYS_THAN_THE_ALLOWED =
            "El agente %s tiene %s (%s) IPATs pendientes por entregar con más de los %s"
                . " días permitidos.";
        const MESSAGE_FORMAT_WHEN_AGENT_HAS_IPATS_IN_RECEIVED_WITH_PENDING_STATE_WITH_MORE_DAYS_THAN_THE_ALLOWED =
            "El agente %s tiene %s (%s) IPATs parcialmente recibido(s) con más de los %s"
                . " días permitidos.";
        const MESSAGE_FORMAT_WHEN_AGENT_HAS_IPATS_PENDING_FROM_PREVIOUS_RANGE_WITH_MORE_DAYS_THAN_THE_ALLOWED =
            "NO SE PUEDE. El agente %s tiene %s (%s) IPATs pendientes por entregar del rango"
                . " anterior con mas de los %s permitidos" ;

        private $dao;
        private $ipatBL;

        /**
         * DeliveredIpatTransactionValidator constructor.
         */
        public function __construct()
        {
            parent::__construct();
            $this->dao = new IpatDeliveryDao();
            $this->ipatBL = new IpatBusinessLayer();
        }

        public function isValid($transaction)
        {
            $fieldsAreComplete = $this->fieldsAreComplete($transaction);

            $role = $this->getRole();
            if($fieldsAreComplete && $role != null && intval($role->id) == Role::ADMINISTRATOR_ROLE_ID)
            {
                return true;
            }

            $canReceive = $this->canReceive($transaction->deliveredTo);

            $hasPartiallyIpats = $this->hasPartiallyIpats($transaction->deliveredTo);

            $hasPendingIpatsFromPreviousRange =
                $this->hasPendingIpatsFromPreviousRange($transaction->deliveredTo);

            $doesNotHaveIpatsInDeliveredState =
                $this->agentDoesNotHaveIpatsInDeliveredStateWithMoreDaysThanAllowed(
                    $transaction->deliveredTo);

            return $fieldsAreComplete
                && $canReceive
                && !$hasPartiallyIpats
                && !$hasPendingIpatsFromPreviousRange
                && $doesNotHaveIpatsInDeliveredState;
        }

        private function fieldsAreComplete($transaction)
        {
            $hasQuantity =
                $transaction->quantity != null && $transaction->quantity > 0;

            if (!$hasQuantity)
            {
                MessagePrinter::printMessage(
                    DeliveredIpatTransactionValidator::MESSAGE_WHEN_QUANTITY_IS_NULL_OR_ZERO);
            }

            $hasDeliveredBy =
                $transaction->deliveredBy != null && $transaction->deliveredBy != '';

            if (!$hasDeliveredBy)
            {
                MessagePrinter::printMessage(
                    DeliveredIpatTransactionValidator::MESSAGE_WHEN_DELIVERED_BY_IS_NULL_OR_ZERO);
            }

            $hasDeliveredTo =
                $transaction->deliveredTo != null && $transaction->deliveredTo != '';

            if (!$hasDeliveredTo)
            {
                MessagePrinter::printMessage(
                    DeliveredIpatTransactionValidator::MESSAGE_WHEN_DELIVERED_TO_IS_NULL_OR_ZERO);
            }

            $fieldsAreComplete = $hasQuantity && $hasDeliveredBy && $hasDeliveredTo;

            if (!$fieldsAreComplete)
            {
                MessagePrinter::printMessage(
                    DeliveredIpatTransactionValidator::MESSAGE_WHEN_FIELDS_ARE_MISSING);
            }

            return $fieldsAreComplete;
        }

        private function canReceive($deliveredTo)
        {
            $canReceive = false;
            $totalDelivered = $this->readTotalIpatsDelivered($deliveredTo);
            if ($totalDelivered < DeliveredIpatTransactionValidator::MAX_QUANTITY_IPATS_PER_AGENT)
            {
                $canReceive = true;
            }
            else
            {
                MessagePrinter::printMessage(
                    sprintf(
                        DeliveredIpatTransactionValidator::MESSAGE_FORMAT_WHEN_AGENT_HAS_MORE_IPATS_IN_DELIVERED_STATE_THAN_THE_CONFIGURED,
                        $deliveredTo,
                        $totalDelivered,
                        DeliveredIpatTransactionValidator::MAX_QUANTITY_IPATS_PER_AGENT));
            }
            return $canReceive;
        }

        private function readTotalIpatsDelivered($deliveredTo)
        {
            $totalPending = 0;
            if($deliveredTo != null)
            {
                $totalPending =
                    $this->dao->getTotalByAgentAndTransactionState(
                        $deliveredTo, IpatTransactionState::DELIVERED);
            }
            return $totalPending;
        }

        private function agentDoesNotHaveIpatsInDeliveredStateWithMoreDaysThanAllowed($deliveredTo)
        {
            $transactions =
                $this->dao->readAllWhenDeliveredDateIsGreaterThanMaxDays(
                    $deliveredTo,
                    DeliveredIpatTransactionValidator::MAX_DAYS_ALLOWED_TO_DELIVER_IPATS);

            $invalid = array();
            $counterInvalid = 0;
            if ($transactions != null && count($transactions) > 0)
            {
                $dateValidator = new DeliveredDateIpatTransactionValidator();

                foreach ($transactions as $key => $transaction)
                {
                    $isValid = $dateValidator->isValid($transaction);
                    if (!$isValid)
                    {
                        array_push($invalid, $transaction->ipatNumber);
                        $counterInvalid = $counterInvalid + 1;
                    }
                }
            }
            else
            {
                $counterInvalid = 0;
            }

            if($counterInvalid > 0)
            {
                MessagePrinter::printMessage(
                    sprintf(
                        DeliveredIpatTransactionValidator::MESSAGE_FORMAT_WHEN_AGENT_HAS_IPATS_IN_DELIVERED_STATE_WITH_MORE_DAYS_THAN_THE_ALLOWED,
                        $deliveredTo,
                        count($invalid),
                        join(' , ', $invalid),
                        DeliveredIpatTransactionValidator::MAX_DAYS_ALLOWED_TO_DELIVER_IPATS));
            }
            return $counterInvalid == 0;
        }

        private function hasPartiallyIpats($partiallyReceivedTo)
        {
            $transactions =
                $this->dao->readAllWhenPartialReceptionDateIsGreaterThanMaxDays(
                    $partiallyReceivedTo,
                    DeliveredIpatTransactionValidator::MAX_DAYS_ALLOWED_TO_RECEIVE_PARTIALLY_IPATS);

            $invalid = array();
            $counterInvalid = 0;
            if ($transactions != null && count($transactions) > 0)
            {
                $dateValidator = new PartialReceptionDateIpatTransactionValidator();

                foreach ($transactions as $key => $transaction)
                {
                    $isValid = $dateValidator->isValid($transaction);
                    if (!$isValid)
                    {
                        array_push($invalid, $transaction->ipatNumber);
                        $counterInvalid = $counterInvalid + 1;
                    }
                }
            }
            else
            {
                $counterInvalid = 0;
            }

            if($counterInvalid > 0)
            {
                MessagePrinter::printMessage(
                    sprintf(
                        DeliveredIpatTransactionValidator::MESSAGE_FORMAT_WHEN_AGENT_HAS_IPATS_IN_RECEIVED_WITH_PENDING_STATE_WITH_MORE_DAYS_THAN_THE_ALLOWED,
                        $partiallyReceivedTo,
                        count($invalid),
                        join(' , ', $invalid),
                        DeliveredIpatTransactionValidator::MAX_DAYS_ALLOWED_TO_RECEIVE_PARTIALLY_IPATS));
            }
            return $counterInvalid > 0;
        }

        private function hasPendingIpatsFromPreviousRange($plaque)
        {
            $currentRange = $this->ipatBL->readCurrent();
            if($currentRange == null)
            {
                return false;
            }

            $previousRange =
                    $this->ipatBL->readFirstFinalNumberLessThan($currentRange->initialNumber);
            if($previousRange == null)
            {
                return false;
            }

            $transactions =
                $this->dao->readAllBetweenRangeWhenDeliveredDateIsGreaterThanMaxDays(
                    $plaque,
                    $previousRange->initialNumber,
                    $previousRange->finalNumber,
                    DeliveredIpatTransactionValidator::MAX_DAYS_ALLOWED_TO_DELIVER_IPATS_PREVIOUS_RANGE);

            if($transactions == null || count($transactions) == 0)
            {
                return false;
            }

            $invalid = array();
            $counterInvalid = 0;
                $dateValidator = new DeliveredDateIpatTransactionValidator();
            foreach ($transactions as $key => $transaction)
            {
                $isValid = $dateValidator->isValid($transaction);
                if (!$isValid)
                {
                    array_push($invalid, $transaction->ipatNumber);
                    $counterInvalid = $counterInvalid + 1;
                }
            }

            if($counterInvalid > 0)
            {
                MessagePrinter::printMessage(
                    sprintf(
                        DeliveredIpatTransactionValidator::MESSAGE_FORMAT_WHEN_AGENT_HAS_IPATS_PENDING_FROM_PREVIOUS_RANGE_WITH_MORE_DAYS_THAN_THE_ALLOWED,
                        $plaque,
                        count($invalid),
                        join(' , ', $invalid),
                        DeliveredIpatTransactionValidator::MAX_DAYS_ALLOWED_TO_DELIVER_IPATS_PREVIOUS_RANGE));
            }
            return $counterInvalid <= 1;
        }
    }
