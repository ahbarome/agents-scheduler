<?php

    /*
    * Copyright (c) 2018 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('/../role_business_layer.php');
    include_once('/../user_business_layer.php');

    class BaseValidator
    {
        protected $sessionManager;
        private $americaDateTime;
        private $roleBL;
        private $userBL;

        function __construct()
        {
            $this->sessionManager = new SessionManager();
            $this->americaDateTime = new AmericaDateTime();
            $this->userBL = new UserBusinessLayer();
            $this->roleBL = new RoleBusinessLayer();
        }

        protected function getUserName()
        {
            return $this->sessionManager->getUserName();
        }

        protected function getPassword()
        {
            return $this->sessionManager->getPassword();
        }

        protected function getCurrentDateTime()
        {
            return $this->americaDateTime->getCurrentDateTime()->format(
                AmericaDateTime::DEFAULT_DATETIME_FORMAT);
        }

        protected function getRole()
        {
            $username = $this->getUserName();
            $password = $this->getPassword();

            if ($username == null || $password == null)
            {
                return null;
            }

            $user = $this->userBL->readOneByUsernameAndPassword($username, $password);
            if ($user == null)
            {
                return;
            }

            $role = $this->roleBL->readOne($user->idRole);
            return $role;
        }
    }
?>