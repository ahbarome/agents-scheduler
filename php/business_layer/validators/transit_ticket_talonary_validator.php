<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('interfaces/ivalidator.php');
    include_once('../../common/message_printer.php');
    include_once('../../database/transit_ticket_talonary_dao.php');

    class TransitTicketTalonaryValidator implements IValidator
    {
        private $dao;
        const MAX_TALONARY_RANGE = 25;
        const MESSAGE_WHEN_TALONARY_EXIST = "El talonario ya existe";
        const MESSAGE_WHEN_TALONARY_RANGE_EXIST = "El rango del talonario ya existe";
        const MESSAGE_WHEN_TALONARY_NUMBER_IS_NOT_VALID = "El # de talonario no es válido";
        const MESSAGE_WHEN_TALONARY_INITIAL_NUMBER_IS_NOT_VALID =
            "El # inicial del talonario no es válido";
        const MESSAGE_WHEN_TALONARY_FINAL_NUMBER_IS_NOT_VALID =
            "El # final del talonario no es válido";
        const MESSAGE_WHEN_TALONARY_RANGE_IS_NOT_VALID =
            "La cantidad máxima de comparendos por talonario es inválida, máximo son 25"
                . " comparendos";

        function __construct()
        {
            $this->dao = new TransitTicketTalonaryDao();
        }

        public function isValid($talonary)
        {
            $existTalonary = $this->existTalonary($talonary->talonaryNumber);
            $existTalonaryRange =
                $this->existTalonaryRange($talonary->initialNumber, $talonary->finalNumber);
            $isValid = !$existTalonary && !$existTalonaryRange;
            return $isValid;
        }

        private function fieldsAreValid($talonary)
        {
            $talonaryNumberIsValid = $talonary->talonaryNumber > 0;
            if (!$talonaryNumberIsValid) {
                MessagePrinter::printMessage(
                    TransitTicketTalonaryValidator::
                    MESSAGE_WHEN_TALONARY_NUMBER_IS_NOT_VALID);
            }

            $talonaryInitialNumberIsValid = $talonary->initialNumber > 0;
            if (!$talonaryInitialNumberIsValid)
            {
                MessagePrinter::printMessage(
                    TransitTicketTalonaryValidator::
                    MESSAGE_WHEN_TALONARY_INITIAL_NUMBER_IS_NOT_VALID);
            }

            $talonaryFinalNumberIsValid = $talonary->finalNumber > 0;
            if ($talonaryFinalNumberIsValid)
            {
                MessagePrinter::printMessage(
                    TransitTicketTalonaryValidator::
                    MESSAGE_WHEN_TALONARY_FINAL_NUMBER_IS_NOT_VALID);
            }

            $talonaryRangeIsValid =
                abs($talonary->finalNumber - $talonary->initialNumber)
                    == TransitTicketTalonaryValidator::MAX_TALONARY_RANGE;
            if (!$talonaryRangeIsValid)
            {
                MessagePrinter::printMessage(
                    TransitTicketTalonaryValidator::
                    MESSAGE_WHEN_TALONARY_RANGE_EXIST);
            }

            return $talonaryNumberIsValid
                && $talonaryInitialNumberIsValid
                && $talonaryFinalNumberIsValid
                && $talonaryRangeIsValid;
        }

        private function existTalonary($talonaryNumber)
        {
            $existTalonary = $this->dao->existTalonary($talonaryNumber);
            if ($existTalonary)
            {
                MessagePrinter::printMessage(
                    TransitTicketTalonaryValidator::
                    MESSAGE_WHEN_TALONARY_EXIST);
            }
            return $existTalonary;
        }

        private function existTalonaryRange($initialNumber, $finalNumber)
        {
            $existTalonaryRange =
                $this->dao->existTalonaryRange($initialNumber, $finalNumber);
            if ($existTalonaryRange)
            {
                MessagePrinter::printMessage(
                    TransitTicketTalonaryValidator::
                    MESSAGE_WHEN_TALONARY_RANGE_EXIST);
            }
            return $existTalonaryRange;
        }
    }
?>