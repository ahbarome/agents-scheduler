<?php
    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('base_validator.php');
    include_once('interfaces/ivalidator.php');
    include_once('../../common/message_printer.php');
    include_once('../../database/ipat_delivery_dao.php');
    include_once('../../model/ipat_transaction_state.php');
    include_once('../../model/role.php');

    /**
     * Class ReceivedWithPendingIpatTransactionValidator allows to validate ipats in state
     * RECEIVED_WITH_PENDING
     */
    class ReceivedWithPendingIpatTransactionValidator extends BaseValidator implements IValidator
    {
        const MESSAGE_WHEN_IPAT_NUMBER_IS_NULL_OR_ZERO =
            "El número de Ipat es requerido.";
        const MESSAGE_WHEN_PARTIALLY_RECEIVED_TO_IS_NULL_OR_ZERO =
            "El agente al que se le desea recibir parcialmente es requerido.";
        const MESSAGE_WHEN_PARTIALLY_RECEIVED_BY_IS_NULL_OR_ZERO =
            "El agente al que recibió parcialmente es requerido, por favor válide la"
                . " configuración del sistema.";
        const MESSAGE_FORMAT_WHEN_IPATS_PER_AGENT_IS_GREATER_THAN_CONFIGURED =
            "Al agente %s han recibido parcialmente %s IPATs. Por configuración máximo se le puede"
                . " recibir parcialmente %s IPATs";
        const MAX_QUANTITY_IPATS_PER_AGENT = 5;

        private $ipatDeliveryDao;

        /**
         * ReceivedWithPendingIpatTransactionValidator constructor.
         */
        public function __construct()
        {
            parent::__construct();
            $this->ipatDeliveryDao = new IpatDeliveryDao();
        }

        /**
         * Perform a validation of all the properties that are required for the transaction state:
         * IpatTransactionState::RECEIVED_WITH_PENDING.
         *
         * @param $transaction to be validated.
         * @return bool true if the transaction is valid, otherwise is going to return false.
         */
        public function isValid($transaction)
        {
            $isValid = false;
            if ($transaction->idTransactionState == IpatTransactionState::RECEIVED_WITH_PENDING)
            {
                $fieldsAreComplete = $this->fieldsAreComplete($transaction);
                $canReceive = $this->canReceive($transaction->deliveredTo);

                $role = $this->getRole();
                if ($role != null && intval($role->id) == Role::ADMINISTRATOR_ROLE_ID)
                {
                    $isValid = $fieldsAreComplete;
                }
                else
                {
                    $isValid = $fieldsAreComplete && $canReceive;
                }
            }
            return $isValid;
        }

        private function fieldsAreComplete($transaction)
        {
            $hasIpatNumber =
                $transaction->ipatNumber != null && $transaction->ipatNumber > 0;

            if (!$hasIpatNumber)
            {
                MessagePrinter::printMessage(
                    ReceivedWithPendingIpatTransactionValidator::
                        MESSAGE_WHEN_IPAT_NUMBER_IS_NULL_OR_ZERO);
            }

            $hasPartiallyReceivedTo =
                $transaction->partiallyReceivedTo != null
                    && $transaction->partiallyReceivedTo > 0;

            if (!$hasPartiallyReceivedTo)
            {
                MessagePrinter::printMessage(
                    ReceivedWithPendingIpatTransactionValidator::
                    MESSAGE_WHEN_PARTIALLY_RECEIVED_TO_IS_NULL_OR_ZERO);
            }

            $hasPartiallyReceivedBy =
                $transaction->partiallyReceivedBy != null
                    && $transaction->partiallyReceivedBy > 0;

            if (!$hasPartiallyReceivedBy)
            {
                MessagePrinter::printMessage(
                    ReceivedWithPendingIpatTransactionValidator::
                    MESSAGE_WHEN_PARTIALLY_RECEIVED_BY_IS_NULL_OR_ZERO);
            }

            return $hasIpatNumber && $hasPartiallyReceivedTo && $hasPartiallyReceivedBy;
        }

        private function canReceive($deliveredTo)
        {
            $canReceive = false;
            $totalReceivedWithPending = $this->readTotalIpatsReceivedWithPendings($deliveredTo);

            if ($totalReceivedWithPending
                < ReceivedWithPendingIpatTransactionValidator::MAX_QUANTITY_IPATS_PER_AGENT)
            {
                $canReceive = true;
            }

            if (!$canReceive)
            {
                MessagePrinter::printMessage(
                    sprintf(
                        ReceivedWithPendingIpatTransactionValidator::
                            MESSAGE_FORMAT_WHEN_IPATS_PER_AGENT_IS_GREATER_THAN_CONFIGURED,
                        $deliveredTo,
                        $totalReceivedWithPending,
                        IpatDeliveryBusinessLayer::MAX_QUANTITY_IPATS_PER_AGENT));
            }
            return $canReceive;
        }

        private function readTotalIpatsReceivedWithPendings($deliveredTo)
        {
            $totalReceivedWithPending = 0;
            if($deliveredTo != null)
            {
                $totalReceivedWithPending =
                    $this->ipatDeliveryDao->getTotalByAgentAndTransactionState(
                        $deliveredTo, IpatTransactionState::RECEIVED_WITH_PENDING);
            }
            return $totalReceivedWithPending;
        }
    }
