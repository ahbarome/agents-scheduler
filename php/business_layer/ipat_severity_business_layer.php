<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../database/ipat_severity_dao.php');
    include_once('../../model/ipat_transaction_state.php');

    class IpatSeverityBusinessLayer
    {
        private $ipatSeverityDao;

        function __construct()
        {
            $this->ipatSeverityDao = new IpatSeverityDao();
        }

        public function readAll()
        {
            return $this->ipatSeverityDao->readAll();
        }
    }
?>