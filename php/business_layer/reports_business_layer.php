<?php
  /*
   * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
   * This software is the confidential and proprietary information of the
   * Secretaria de Transito. ("Confidential Information").
   * You may not disclose such Confidential Information, and may only
   * use such Confidential Information in accordance with the terms of
   * the license agreement you entered into with the Secretaria de Transito.
   */

    include_once('exporters/ipats_received_with_pending_state_exporter.php');
    include_once('../../database/reports_dao.php');

    /**
     * Class ReportsBusinessLayer that provide all the methods for all the reports in the
     * application
     */
    class ReportsBusinessLayer
    {
        private $reportsDao;

        /**
         * ReportsBusinessLayer constructor.
         */
        function __construct()
        {
            $this->reportsDao = new ReportsDao();
        }

        /**
         * Read all the ipats data in state received with pending.
         *
         * @return array with the data of all the ipats in state received with pending.
         */
        public function readAllIpatsReceivedWithPendingState()
        {
            $reportData = $this->reportsDao->readAllIpatsReceivedWithPendingState();
            return $reportData;
        }

        /**
         * Generate the content to be exported with the ipats data in state received with pending.
         *
         * @return string with the content to be exported.
         */
        public function exportCSVIpatsReceivedWithPendingState()
        {
            $reportData = $this->reportsDao->readAllIpatsReceivedWithPendingState();
            $exporter = new IpatsReceivedWithPendingStateExporter($reportData);
            $content = $exporter->exportContent();
            return $content;
        }
    }