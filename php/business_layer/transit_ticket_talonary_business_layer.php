<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('agent_business_layer.php');
    include_once('base_business_layer.php');
    include_once('../../database/transit_ticket_talonary_dao.php');
    include_once('validators/transit_ticket_talonary_validator.php');

    class TransitTicketTalonaryBusinessLayer extends BaseBusinessLayer
    {
        private $dao;
        private $agentBL;

        function __construct()
        {
            parent::__construct();
            $this->dao = new TransitTicketTalonaryDao();
            $this->agentBL = new AgentBusinessLayer();
        }

        public function save($talonary)
        {
            $validator = new TransitTicketTalonaryValidator();
            $saved = false; 
            if ($validator->isValid($talonary))
            {
                $talonary->createdBy = $this->getUserName();
                $talonary->createdAt = $this->getCurrentDateTime();
                $saved = $this->dao->save($talonary);
            }
            return $saved;
        }

        public function getAgent($talonaryNumber)
        {
            $talonary = $this->readOne($talonaryNumber);
            $agent = array();
            if ($talonary != null)
            {
                $agent = $this->agentBL->readOne($talonary->assignedTo);
            }
            return $agent;
        }

        public function readAll()
        {
            return $this->dao->readAll();
        }

        public function readLast()
        {
            return $this->dao->readLast();
        }

        public function readOne($talonaryNumber)
        {
            return $this->dao->readOne($talonaryNumber);
        }
    }
