<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('interfaces/iexporter.php');
    include_once('../../business_layer/agent_business_layer.php');
    include_once('../../common/america_datetime.php');
    include_once('../../common/pdf/fpdf_protection.php');
    include_once('../../model/ipat_transaction_state.php');
    include_once('../../session/session_manager.php');

    /**
     * Class IpatTransactionMinistryPDFExporter allows to create PDf file with the data needed for
     * the ministry according to the exported date.
     */
    class IpatTransactionMinistryPDFExporter implements IExporter
    {
        /** Prefix for all the ipat numbers. */
        const IPAT_PREFIX = "A000";

        private $agentBL;
        private $transactions;
        private $exportDate;

        /**
         * IpatTransactionMinistryPDFExporter constructor.
         *
         * @param $transactions contains all the ipats data without filters
         * @param $exportDate is the date to export the data
         */
        function __construct($transactions, $exportDate)
        {
            $this->agentBL = new AgentBusinessLayer();
            $this->transactions = $transactions;
            $this->exportDate = $exportDate;
        }

        /**
         * Prepare and generate the content to be exported.
         *
         * @return string content to be exported.
         */
        public function exportContent()
        {
            $transactionsByExportedDate = $this->getTransactionsByExportedDate();
            $pdf = new MinistryPDF();
            $pdf->setTransactions($transactionsByExportedDate);
            $pdf->exportContent();
        }

        /**
         * Get the transactions that match with the exported date.
         *
         * @return array with all the transactions that match with the exported date.
         */
        private function getTransactionsByExportedDate()
        {
            $data = array();
            $agentInCurrentSession = $this->agentBL->readAgentByCurrentSessionUser();
            $plaqueAgentInCurrentSession =
                $agentInCurrentSession != null ? $agentInCurrentSession->plaque : "";
            foreach ($this->transactions as $key => $transaction)
            {
                if ( $transaction->transactionState != null
                    && $transaction->idTransactionState == IpatTransactionState::RECEIVED
                    && $this->isSameDate($transaction->receptionDate)
                    && $transaction->receivedBy == $plaqueAgentInCurrentSession)
                {
                    array_push(
                        $data,
                        array(
                            $transaction->agentReceivedTo != null
                            ? $transaction->agentReceivedTo->plaque : "",
                            IpatTransactionMinistryExporter::IPAT_PREFIX
                                . strval($transaction->ipatNumber),
                            $transaction->accidentDate != null
                                ? $transaction->accidentDate
                                : "",
                            $transaction->address != null ? $transaction->address : "",
                            $transaction->vehiclePlaque != null ? $transaction->vehiclePlaque : "",
                            $transaction->agentReceivedBy != null
                                ? $transaction->agentReceivedBy->plaque : ""));
                }
            }
            return $data;
        }

        /**
         * Validate if the reception date is equals to the exported date.
         *
         * @param $receptionDate of the ipat.
         * @return bool True if the reception date is equals to the exported date selected by the
         * user otherwise is going to return false.
         */
        private function isSameDate($receptionDate)
        {
            $receptionDateTime =
                new DateTime($receptionDate, new DateTimeZone(AmericaDateTime::TIME_ZONE));
            $isSameDate =
                $this->exportDate->format(AmericaDateTime::DEFAULT_DATE_FORMAT)
                    == $receptionDateTime->format(AmericaDateTime::DEFAULT_DATE_FORMAT);
            return $isSameDate;
        }
    }

    /**
     * Class MinistryPDF allows to generate the PDF.
     */
    class MinistryPDF extends FPDF_Protection
    {
        /** Header for the table. */
        const TITLE = "DATOS PARA EL MINISTERIO";
        const TITLE_FONT_SIZE = 20;
        const TITLE_STYLE = "B";
        const FONT_FAMILY = "Arial";
        const HEADER_FONT_SIZE = 9;
        const DETAIL_FONT_SIZE = 7;
        const HEADER_COLUMN_TITLES =
            array(
                "#",
                "Recibido A",
                "# IPAT",
                "Accidente",
                "Dirección",
                "Placa Vehículo(s)",
                "Recibido Por");
        const HEADER_CELL_WIDTH_SIZES = array(5, 18, 18, 18, 70, 45, 20);
        const MESSAGE_WHEN_TRANSACTIONS_ARE_NULL =
            "No se encontraron transacciones para la fecha seleccionada.";

        private $transactions;

        /**
         * Set the transactions
         *
         * @param $transactions to generate the PDF.
         */
        function setTransactions($transactions)
        {
            $this->transactions = $transactions;
        }

        /**
         * Allows to generate all the content to be exported base on the set of transactions.
         */
        function exportContent()
        {
            $this->SetProtection(array('print'));
            $this->AddPage();
            $this->addHeader();
            $this->Ln();
            $this->addTitle();
            $this->Ln();

            if ($this->transactions == null)
            {
                $this->addDefaultMessage();
            }
            else
            {
                $this->addTable();
            }

            ob_start();
            $this->Output('D', 'file.pdf', true);
        }

        private function addHeader()
        {
            $americaDateTime = new AmericaDateTime();
            $currentDate =
                $americaDateTime->getCurrentDateTime()->format(
                    AmericaDateTime::DEFAULT_DATETIME_FORMAT);

            $sessionManager = new SessionManager();
            $username = $sessionManager->getUserName();

            $this->SetFont(
                MinistryPDF::FONT_FAMILY, '', MinistryPDF::HEADER_FONT_SIZE);
            $this->Multicell(
                100,
                4,
                sprintf(
                    utf8_decode("Fecha generación: %s\nUsuario exporta: %s\n"),
                    $currentDate,
                    $username),
                0,
                FPDF::ALIGN_LEFT);
        }

        private function addTitle()
        {
            $this->SetFont(
                MinistryPDF::FONT_FAMILY, MinistryPDF::TITLE_STYLE, MinistryPDF::TITLE_FONT_SIZE);
            $this->Multicell(
                200,
                5,
                MinistryPDF::TITLE,
                0,
                FPDF::ALIGN_CENTER);
        }

        private function addDefaultMessage()
        {
            $this->SetFont(
                MinistryPDF::FONT_FAMILY, '', MinistryPDF::DETAIL_FONT_SIZE);
            $this->Multicell(
                100,
                4,
                MinistryPDF::MESSAGE_WHEN_TRANSACTIONS_ARE_NULL,
                0,
                FPDF::ALIGN_LEFT);
        }

        private function addTable()
        {
            $this->SetFont(MinistryPDF::FONT_FAMILY, '', MinistryPDF::DETAIL_FONT_SIZE);
            // Colors, line width and bold font
            $this->SetFillColor(0,0,0);
            $this->SetTextColor(255);
            $this->SetDrawColor(0,0,0);
            $this->SetLineWidth(.3);
            $this->SetFont('','B');

            // Header
            for ($index = 0; $index < count(MinistryPDF::HEADER_COLUMN_TITLES); $index++)
            {
                $this->Cell(
                    MinistryPDF::HEADER_CELL_WIDTH_SIZES[$index], // width of the cell
                    10, // height of the cell
                    utf8_decode(MinistryPDF::HEADER_COLUMN_TITLES[$index]),
                    1,
                    0,
                    FPDF::ALIGN_CENTER,
                    true);
            }

            $this->Ln();

            // Color and font restoration
            $this->SetFillColor(224,235,255);
            $this->SetTextColor(0);
            $this->SetFont('');

            // Data
            $fill = false;
            $fontHeight = 7;
            $receivedToCell = 0;
            $ipatNumberCell = 1;
            $accidentDateCell = 2;
            $addressCell = 3;
            $vehiclesPlaque = 4;
            $receivedByCell = 5;
            $rowsCounter = 0;
            foreach ($this->transactions as $transaction)
            {
                $rowsCounter = $rowsCounter + 1;
                $this->Cell(
                    MinistryPDF::HEADER_CELL_WIDTH_SIZES[0],
                    $fontHeight,
                    $rowsCounter,
                    'LR',
                    0,
                    FPDF::ALIGN_CENTER,
                    $fill);
                $this->Cell(
                    MinistryPDF::HEADER_CELL_WIDTH_SIZES[$receivedToCell + 1],
                    $fontHeight,
                    $transaction[$receivedToCell],
                    'LR',
                    0,
                    FPDF::ALIGN_CENTER,
                    $fill);
                $this->Cell(
                    MinistryPDF::HEADER_CELL_WIDTH_SIZES[$ipatNumberCell + 1],
                    $fontHeight,
                    $transaction[$ipatNumberCell],
                    'LR',
                    0,
                    FPDF::ALIGN_LEFT,
                    $fill);
                $this->Cell(
                    MinistryPDF::HEADER_CELL_WIDTH_SIZES[$accidentDateCell + 1],
                    $fontHeight,
                    $transaction[$accidentDateCell],
                    'LR',
                    0,
                    FPDF::ALIGN_LEFT,
                    $fill);
                $address =
                    $transaction[$addressCell] != null
                        ? substr(utf8_decode($transaction[$addressCell]), 0, 42)
                        : "";
                $this->Cell(
                    MinistryPDF::HEADER_CELL_WIDTH_SIZES[$addressCell + 1],
                    $fontHeight,
                    utf8_decode($address),
                    'LR',
                    0,
                    FPDF::ALIGN_LEFT,
                    $fill);
                $this->Cell(
                    MinistryPDF::HEADER_CELL_WIDTH_SIZES[$vehiclesPlaque + 1],
                    $fontHeight,
                    utf8_decode($transaction[$vehiclesPlaque]),
                    'LR',
                    0,
                    FPDF::ALIGN_LEFT,
                    $fill);
                $this->Cell(
                    MinistryPDF::HEADER_CELL_WIDTH_SIZES[$receivedByCell + 1],
                    $fontHeight,
                    $transaction[$receivedByCell],
                    'LR',
                    0,
                    FPDF::ALIGN_CENTER,
                    $fill);
                $this->Ln();
                $fill = !$fill;
            }
            // Closing line
            $this->Cell(array_sum(MinistryPDF::HEADER_CELL_WIDTH_SIZES),0,'','T');
        }
    }
