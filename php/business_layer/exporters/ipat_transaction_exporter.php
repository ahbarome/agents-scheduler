<?php
    include_once('interfaces/iexporter.php');

    class IpatTransactionExporter implements IExporter
    {
        const EXPORT_FORMAT = "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n";
        const IPAT_PREFIX = "A000";

        private $transactions;

        function __construct($transactions)
        {
            $this->transactions = $transactions;
        }

        public function exportContent()
        {
            $content =
                sprintf(
                    IpatTransactionExporter::EXPORT_FORMAT,
                    "# IPAT",
                    "Fecha Entrega",
                    "Entregado Por",
                    "Entregado A",
                    "Fecha Recepcion",
                    "Recibido Por",
                    "Recibido A",
                    "Recibido Parcialmente Por",
                    "Recibido Parcialmente A",
                    "Estado",
                    "Direccion",
                    "Placa Vehiculo(s)",
                    "Fecha Accidente",
                    "Fecha Diligenciamiento",
                    "Gravedad",
                    "Estado del IPAT",
                    "Noticia Criminal",
                    "Observaciones",
                    "Dias Demora Entrega Oficina",
                    "Dias Demora Diligenciar IPAT");
            foreach ($this->transactions as $key => $transaction)
            {
                if ( $transaction->transactionState != null)
                {
                    $content.=
                        sprintf(
                            IpatTransactionExporter::EXPORT_FORMAT,
                            IpatTransactionExporter::IPAT_PREFIX . strval($transaction->ipatNumber),
                            $transaction->deliveryDate != null ? $transaction->deliveryDate : "",
                            $transaction->agentDeliveredBy != null
                                ? $transaction->agentDeliveredBy->plaque : "",
                            $transaction->agentDeliveredTo != null
                                ? $transaction->agentDeliveredTo->plaque : "",
                            $transaction->receptionDate != null ? $transaction->receptionDate : "",
                            $transaction->agentReceivedBy != null
                                ? $transaction->agentReceivedBy->plaque : "",
                            $transaction->agentReceivedTo != null
                                ? $transaction->agentReceivedTo->plaque : "",
                            $transaction->agentPartiallyReceivedBy != null
                                ? $transaction->agentPartiallyReceivedBy->plaque : "",
                            $transaction->agentPartiallyReceivedTo != null
                                ? $transaction->agentPartiallyReceivedTo->plaque : "",
                            $transaction->transactionState != null
                                ? $transaction->transactionState->name : "",
                            $transaction->address != null ? $transaction->address : "",
                            $transaction->vehiclePlaque != null ? $transaction->vehiclePlaque : "",
                            $transaction->accidentDate != null ? $transaction->accidentDate : "",
                            $transaction->diligenceDate != null ? $transaction->diligenceDate : "",
                            $transaction->severity != null ? $transaction->severity->description : "",
                            $transaction->classification != null
                                ? $transaction->classification->name : "",
                            $transaction->criminalNews,
                            $transaction->observation,
                            $transaction->daysOfDelayDelivery != null ?
                                $transaction->daysOfDelayDelivery : "",
                            $transaction->daysOfDelayReception != null ?
                                $transaction->daysOfDelayReception : "");
                }
            }
            return $content;
        }
    }
