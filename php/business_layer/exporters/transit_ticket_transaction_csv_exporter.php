<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('interfaces/iexporter.php');

    class TransitTicketTransactionExporter implements IExporter
    {
        const EXPORT_FORMAT = "%s,%s,%s,%s,%s,%s,%s\n";

        private $transactions;

        function __construct($transactions)
        {
            $this->transactions = $transactions;
        }

        public function exportContent()
        {
            $content =
                sprintf(
                    TransitTicketTransactionExporter::EXPORT_FORMAT,
                    "# Talonario",
                    "# Comparendo",
                    "Fecha Diligenciamiento",
                    "Codigo Infraccion",
                    "Inmovilizado en",
                    "Motivo Inmovilizacion",
                    "Agente Realizo Procedimiento");
            foreach ($this->transactions as $key => $transaction)
            {
                $content.=
                    sprintf(
                        TransitTicketTransactionExporter::EXPORT_FORMAT,
                        $transaction->talonaryNumber != null ? $transaction->talonaryNumber : "",
                        $transaction->ticketNumber != null ? $transaction->ticketNumber : "",
                        $transaction->diligenceDate != null ? $transaction->diligenceDate : "",
                        $transaction->infractorCode != null ? $transaction->infractorCode : "",
                        $transaction->immobilizedSite != null ? $transaction->immobilizedSite : "",
                        $transaction->immobilizedSubject != null ? $transaction->immobilizedSubject : "",
                        $transaction->plaqueAgent != null ? $transaction->plaqueAgent : "");
            }
            return $content;
        }
    }
