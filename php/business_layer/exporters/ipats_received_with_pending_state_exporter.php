<?php
    /*
     * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
     * This software is the confidential and proprietary information of the
     * Secretaria de Transito. ("Confidential Information").
     * You may not disclose such Confidential Information, and may only
     * use such Confidential Information in accordance with the terms of
     * the license agreement you entered into with the Secretaria de Transito.
     */

    include_once('interfaces/iexporter.php');

    /**
     * Class IpatsReceivedWithPendingStateExporter Allows to generate the data needed with all the
     * agents that already have ipats in state received with pendings and with the total of ipats
     * with that state
     */
    class IpatsReceivedWithPendingStateExporter implements IExporter
    {
        private $transactions;

        const EXPORT_FORMAT = "%s,%s,%s,%s\n";
        const IPAT_SEPARATOR = "|";

        /**
         * IpatsReceivedWithPendingStateExporter constructor.
         *
         * @param $transactions collectoin
         */
        function __construct($transactions)
        {
            $this->transactions = $transactions;
        }

        /**
         * Generate the content to be exported.
         *
         * @return string content to be exported.
         */
        public function exportContent()
        {
            $content =
                sprintf(
                    IpatsReceivedWithPendingStateExporter::EXPORT_FORMAT,
                    "Placa",
                    "Entregado A",
                    "Total",
                    "Detalle");
            foreach ($this->transactions as $key => $transaction)
            {
                $content .= sprintf(
                    IpatsReceivedWithPendingStateExporter::EXPORT_FORMAT,
                    $transaction->agent != null ? $transaction->agent->plaque : "",
                    $transaction->agent != null ?
                        $transaction->agent->firstName . " " . $transaction->agent->lastName : "",
                    $transaction->numberOfIpats != null ? $transaction->numberOfIpats : "",
                    $transaction->transactions != null ?
                        $this->getContentDetail($transaction->transactions) : "");
            }
            return $content;
        }

        private function getContentDetail($transactionsDetail)
        {
            $contentDetail = "";
            foreach ($transactionsDetail as $key => $detail)
            {
                $contentDetail .=
                    $detail->ipatNumber
                        . IpatsReceivedWithPendingStateExporter::IPAT_SEPARATOR
                        . $detail->agentDeliveredBy->firstName
                        . $detail->agentDeliveredBy->lastName
                        . IpatsReceivedWithPendingStateExporter::IPAT_SEPARATOR
                        . $detail->partialReceptionDate
                        . "*";
            }
            return $contentDetail;
        }
    }