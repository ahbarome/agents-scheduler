<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('interfaces/iexporter.php');
    include_once('../../common/america_datetime.php');
    include_once('../../common/pdf/fpdf_protection.php');

    class IpatTransactionPeaceAndSavePDFExporter implements IExporter
    {
        private $pdfGenerator;

        function __construct($agent, $ipat, $total, $totalPending, $totalPartiallyReceived)
        {
            $this->pdfGenerator = new PeaceAndSavePDF();
            $this->pdfGenerator->setData(
                $agent, $ipat, $total, $totalPending, $totalPartiallyReceived);
        }

        /**
         * Prepare and generate the content to be exported.
         *
         * @return string content to be exported.
         */
        public function exportContent()
        {
            $this->pdfGenerator->exportContent();
        }
    }

    /**
     * Class PeaceAndSavePDF allows to generate the PDF.
     */
    class PeaceAndSavePDF extends FPDF_Protection
    {
        /** Header for the table. */
        const TITLE = "PAZ Y SALVO DE IPATS";
        const TITLE_FONT_SIZE = 20;
        const TITLE_STYLE = "B";
        const FONT_FAMILY = "Arial";
        const HEADER_FONT_SIZE = 9;
        const DETAIL_FONT_SIZE = 12;
        const HEADER_CELL_WIDTH_SIZES = array(5, 18, 18, 18, 70, 45, 20);
        const MESSAGE_WHEN_TRANSACTIONS_ARE_NULL =
            "No se encontraron transacciones para el  agente %s rango de ipats %s seleccionado.";
        const MESSAGE_FORMAT_PEACE_AND_SAVE =
            "El agente %s se encuentra al día con el rango de ipats #%s seleccionado";
        
        private $agent;
        private $ipat;
        private $total;
        private $totalPending;
        private $totalPartiallyReceived;

        function setData($agent, $ipat, $total, $totalPending, $totalPartiallyReceived)
        {
            $this->agent = $agent;
            $this->ipat = $ipat;
            $this->total = $total;
            $this->totalPending = $totalPending;
            $this->totalPartiallyReceived = $totalPartiallyReceived;
        }

        /**
         * Allows to generate all the content to be exported base on the set of transactions.
         */
        function exportContent()
        {

            $this->SetProtection(array('print'));
            $this->AddPage();
            $this->addHeader();
            $this->Ln();
            $this->Ln();
            $this->addTitle();
            $this->Ln();

            if ($this->ipat == null || $this->agent == null || $this->total == 0)
            {
                $this->addDefaultMessage();
            }
            else
            {
                $this->addPeaceAndSaveContent();
            }

            ob_start();
            $this->Output('D', 'file.pdf', true);

        }

        private function addHeader()
        {
            $americaDateTime = new AmericaDateTime();
            $currentDate =
                $americaDateTime->getCurrentDateTime()->format(
                    AmericaDateTime::DEFAULT_DATETIME_FORMAT);

            $sessionManager = new SessionManager();
            $username = $sessionManager->getUserName();

            $this->SetFont(
                PeaceAndSavePDF::FONT_FAMILY, '', PeaceAndSavePDF::HEADER_FONT_SIZE);
            $this->Multicell(
                100,
                4,
                sprintf(
                    utf8_decode("Fecha generación: %s\nUsuario exporta: %s\n"),
                    $currentDate,
                    $username),
                0,
                FPDF::ALIGN_LEFT);
        }

        private function addTitle()
        {
            $this->SetFont(
                PeaceAndSavePDF::FONT_FAMILY,
                PeaceAndSavePDF::TITLE_STYLE,
                PeaceAndSavePDF::TITLE_FONT_SIZE);
            $this->Multicell(
                200,
                5,
                PeaceAndSavePDF::TITLE,
                0,
                FPDF::ALIGN_CENTER);
        }

        private function addDefaultMessage()
        {
            $this->SetFont(
                PeaceAndSavePDF::FONT_FAMILY, '', PeaceAndSavePDF::DETAIL_FONT_SIZE);
            $this->Write(
                PeaceAndSavePDF::DETAIL_FONT_SIZE,
                sprintf(
                    PeaceAndSavePDF::MESSAGE_WHEN_TRANSACTIONS_ARE_NULL,
                    $this->agent->firstName . ' ' . $this->agent->lastName,
                    '[' . $this->ipat->initialNumber . ' - '. $this->ipat->finalNumber . ']'));
        }

        private function addPeaceAndSaveContent()
        {
            $this->SetFont(
                PeaceAndSavePDF::FONT_FAMILY, '', PeaceAndSavePDF::DETAIL_FONT_SIZE);
            
            $content = '';
            if ($this->totalPending == 0 && $this->totalPartiallyReceived == 0)
            {
                $content =
                'El agente '
                    . $this->agent->plaque
                    . ' '
                    . $this->agent->firstName
                    . ' '
                    . $this->agent->lastName
                    . ' se encuentra al dia con el rango de ipats ['
                    . $this->ipat->initialNumber . ' - '. $this->ipat->finalNumber . ']';
            }
            else
            {
                $content =
                'El agente '
                    . $this->agent->plaque
                    . ' '
                    . $this->agent->firstName
                    . ' '
                    . $this->agent->lastName
                    . ' NO se encuentra al dia con el rango de ipats ['
                    . $this->ipat->initialNumber 
                    . ' - '
                    . $this->ipat->finalNumber . '].'
                    . ' El agente cuenta con '
                    . $this->totalPending
                    . ' en estado Entregado(s) y cuenta con '
                    . $this->totalPartiallyReceived
                    . ' en estado Recibido(s) Parcialmente.';
            }
            
            $this->Write(
                PeaceAndSavePDF::DETAIL_FONT_SIZE,
                $content);
        }
    }
