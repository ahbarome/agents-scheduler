<?php
    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('interfaces/iexporter.php');
    include_once('../../common/america_datetime.php');
    include_once('../../model/ipat_transaction_state.php');

    /**
     * Class IpatTransactionMinistryExporter allows to create the data to be exported to the
     * ministry by a specific export date.
     */
    class IpatTransactionMinistryExporter implements IExporter
    {
        /** Format in which the data is going to be export. */
        const EXPORT_FORMAT = "%s,%s,%s,%s,%s,%s,%s\r\n";

        /** Prefix for all the ipat numbers. */
        const IPAT_PREFIX = "A000";

        private $transactions;
        private $americaDateTime;
        private $exportDate;

        /**
         * IpatTransactionMinistryExporter constructor.
         *
         * @param $transactions contains all the ipats data without filters
         * @param $exportDate is the date to export the data
         */
        function __construct($transactions, $exportDate)
        {
            $this->transactions = $transactions;
            $this->exportDate = $exportDate;
            $this->americaDateTime = new AmericaDateTime();
        }

        /**
         * Prepare and generate the content to be exported.
         *
         * @return string contnt to be exported.
         */
        public function exportContent()
        {
            $content =
                utf8_decode(
                    sprintf(
                        IpatTransactionMinistryExporter::EXPORT_FORMAT,
                        "Recibido A",
                        "# IPAT",
                        "Fecha Accidente",
                        "Dirección",
                        "Placa Vehículo(s)",
                        "Recibido Por",
                        "Días Demora Entrega Oficina"));
            foreach ($this->transactions as $key => $transaction)
            {
                if ( $transaction->transactionState != null
                    && $transaction->idTransactionState == IpatTransactionState::RECEIVED
                    && $this->isSameDate($transaction->receptionDate))
                {
                    $content.=
                        utf8_decode(
                            sprintf(
                        IpatTransactionMinistryExporter::EXPORT_FORMAT,
                            $transaction->agentReceivedTo != null
                                ? $transaction->agentReceivedTo->plaque : "",
                            IpatTransactionMinistryExporter::IPAT_PREFIX . strval(
                            $transaction->ipatNumber),
                            $transaction->accidentDate != null ? $transaction->accidentDate : "",
                            $transaction->address != null ? $transaction->address : "",
                            $transaction->vehiclePlaque != null ? $transaction->vehiclePlaque : "",
                            $transaction->agentReceivedBy != null
                                ? $transaction->agentReceivedBy->plaque : "",
                            $transaction->daysOfDelayDelivery != null
                                ? $transaction->daysOfDelayDelivery : ""));
                }
            }
            return $content;
        }

        /**
         * Validate if the reception date is equals to the exported date.
         *
         * @param $receptionDate of the ipat.
         * @return bool True if the reception date is equals to the exported date selected by the
         * user otherwise is going to return false.
         */
        private function isSameDate($receptionDate)
        {
            $receptionDateTime =
                new DateTime($receptionDate, new DateTimeZone(AmericaDateTime::TIME_ZONE));

            if ($this->exportDate->format(AmericaDateTime::DEFAULT_DATE_FORMAT)
                == $receptionDateTime->format(AmericaDateTime::DEFAULT_DATE_FORMAT))
            {
                return true;
            }
            return false;
        }
    }
?>
