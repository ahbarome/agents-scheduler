<?php

    /*
    * Copyright (c) 2018 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('base_business_layer.php');
    include_once('../../database/ipat_dao.php');

    class IpatBusinessLayer extends BaseBusinessLayer
    {
        private $dao;

        function __construct()
        {
            parent::__construct();
            $this->dao = new IpatDao();
        }

        public function readAll()
        {
            return $this->dao->readAll();
        }

        public function readOne($id)
        {
            return $this->dao->readOne($id);
        }

        public function readCurrent()
        {
            return $this->dao->readCurrent();
        }

        public function readFirstInitialNumberGreatherOrEqualTo($searchedNumber)
        {
            return $this->dao->readFirstInitialNumberGreatherOrEqualTo($searchedNumber);
        }

        public function readFirstFinalNumberLessThan($searchedNumber)
        {
            return $this->dao->readFirstFinalNumberLessThan($searchedNumber);
        }
    }