<?php

    /*
    * Copyright (c) 2018 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('base_business_layer.php');
    include_once('ipat_business_layer.php');
    include_once('../../database/ipat_delivery_statistic_dao.php');

    /**
     * Class IpatTransactionStatisticBusinessLayer manage the business logic for the statistics
     * of ipat transactions.
     */
    final class IpatTransactionStatisticBusinessLayer extends BaseBusinessLayer
    {
        private $dao;
        private $ipatBL;

        /**
         * IpatTransactionStatisticBusinessLayer constructor.
         */
        function __construct()
        {
            parent::__construct();
            $this->dao = new IpatTransactionStatisticDao();
            $this->ipatBL = new IpatBusinessLayer();
        }

        /**
         * Read all the statistics base on each range of ipats configured in the DB.
         *
         * @param $startDate initial date to filter the data.
         * @param $endDate final date to filter the data.
         * @return array with all the ipat statistics.
         */
        public function readAll($startDate = null, $endDate = null)
        {
            $statistics = array();
            if ($startDate == null || $endDate == null)
            {
                $statistic = $this->dao->readAllSeveritiesStatistics();
                $statistics = $this->addStatistic($statistics, $statistic);
            }
            else
            {
                $statistic =
                    $this->dao->readAllSeveritiesStatisticsByDateRange($startDate, $endDate);
                $statistics = $this->addStatistic($statistics, $statistic);
            }
            return $statistics;
        }

        private function addStatistic($statistics, $statistic)
        {
            if ($statistic != null)
            {
                $statistic->total =
                    $statistic->totalDamages
                        + $statistic->totalInjuries
                        + $statistic->totalHomicides;
                array_push($statistics, $statistic);
            }
            return $statistics;
        }
    }
?>