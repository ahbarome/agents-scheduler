<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('base_business_layer.php');
    include_once('exporters/transit_ticket_transaction_csv_exporter.php');
    include_once('exporters/transit_ticket_transaction_peace_and_save_pdf_exporter.php');
    include_once('validators/filter_criteria_validator.php');
    include_once('../../business_layer/agent_business_layer.php');
    include_once('../../business_layer/transit_ticket_talonary_business_layer.php');
    include_once('../../business_layer/transit_ticket_transaction_novelty_business_layer.php');
    include_once('../../database/transit_ticket_transaction_dao.php');
    include_once('../../model/filter_criteria.php');
    include_once('../../model/transit_ticket_transaction.php');
    include_once('../../model/transit_ticket_transaction_state.php');

    class TransitTicketTransactionBusinessLayer extends BaseBusinessLayer
    {
        private $dao;
        private $agentBL;
        private $talonaryBL;
        private $noveltyBL;
        private $filterCriteriaValidator;

        function __construct()
        {
            parent::__construct();
            $this->dao = new TransitTicketTransactionDao();
            $this->agentBL = new AgentBusinessLayer();
            $this->talonaryBL = new TransitTicketTalonaryBusinessLayer();
            $this->noveltyBL = new TransitTicketTransactionNoveltyBusinessLayer();
            $this->filterCriteriaValidator = new FilterCriteriaValidator();
        }

        public function save($talonary)
        {
            $createdTalonary = $this->talonaryBL->save($talonary);
            if ($createdTalonary)
            {
                $transientTransaction = new TransitTicketTransaction();
                $transientTransaction->talonaryNumber = intval($talonary->talonaryNumber);
                $transientTransaction->plaqueAgent = $talonary->assignedTo;
                $transientTransaction->createdBy = $this->getUserName();
                $transientTransaction->createdAt = $this->getCurrentDateTime();
                $transientTransaction->idTransactionState =
                    intval(TransitTicketTransactionState::DELIVERED);
                $quantityOfTransactions = abs($talonary->finalNumber) - abs($talonary->initialNumber);
                for ($i = 0; $i <= $quantityOfTransactions; $i++)
                {
                    $transientTransaction->ticketNumber = abs($talonary->initialNumber) + $i;
                    $this->dao->save($transientTransaction);
                }
            }
            return $createdTalonary;
        }

        public function receive($transaction)
        {
            $received = false;
            $storedTransaction =
                $this->readOne($transaction->talonaryNumber, $transaction->ticketNumber);
            if ($storedTransaction != null)
            {
                $transaction->modifiedBy = $this->getUserName();
                $transaction->modifiedAt = $this->getCurrentDateTime();
                $transaction->idTransactionState =
                    intval(TransitTicketTransactionState::RECEIVED);
                $received = $this->dao->update($transaction);
            }
            return $received;
        }

        public function addNovelty($novelty)
        {
            $saved = $this->noveltyBL->save($novelty);
            return $saved;
        }

        public function readOne($talonaryNumber, $ticketNumber)
        {
            return $this->dao->readOne($talonaryNumber, $ticketNumber);
        }

        public function readAll($filter = null)
        {
            if ($this->filterCriteriaValidator->isValid($filter))
            {
                return $this->dao->readAllByCriteria($filter);
            }
            else
            {
                return $this->readLast();
            }
        }

        public function readLast()
        {
            return $this->dao->readLast();
        }

        public function getCountDeliveredTransactions($assignedTo = null)
        {
            if ($assignedTo != null)
            {
                return $this->dao->countByAssignedAndState(
                    $assignedTo, TransitTicketTransactionState::DELIVERED);
            }
            else
            {
                return $this->dao->countByState(TransitTicketTransactionState::DELIVERED);
            }
        }

        public function getCountReceivedTransactions($assignedTo = null)
        {
            if ($assignedTo != null)
            {
                return $this->dao->countByAssignedAndState(
                    $assignedTo, TransitTicketTransactionState::RECEIVED);
            }
            else
            {
                return $this->dao->countByState(TransitTicketTransactionState::RECEIVED);
            }
        }

        public function getCountPendingForDeliveryTransactions($assignedTo = null)
        {
            $totalDelivered = $this->getCountDeliveredTransactions($assignedTo);
            $totalReceived = $this->getCountReceivedTransactions($assignedTo);
            $pending = abs($totalReceived - $totalDelivered);
            return $pending;
        }

        public function getCountNovelties($assignedTo = null)
        {
            return $this->noveltyBL->countAllByAgent($assignedTo);
        }

        public function exportCSV()
        {
            $transactions = $this->readAll();
            $exporter = new TransitTicketTransactionExporter($transactions);
            $content = $exporter->exportContent();
            return $content;
        }

        public function exportPDFPeaceAndSave($talonaryNumber)
        {
            $agent = $this->talonaryBL->getAgent($talonaryNumber);
            $totalTransitTickets = $this->dao->countByTalonary($talonaryNumber);
            $totalReceived =
                $this->dao->countByTalonaryAndState(
                    $talonaryNumber, TransitTicketTransactionState::RECEIVED);
            $isUpToDate = $totalTransitTickets == $totalReceived;
            $exporter =
                new TransitTicketTransactionPeaceAndSavePDFExporter(
                    $isUpToDate, $talonaryNumber, $agent);
            $exporter->exportContent();
        }

        public function readAllActiveAgentsWithTransactions()
        {
            $plaques = $this->dao->readDistinctAgentPlaques();
            $agents = $this->agentBL->readAllActiveByPlaques($plaques);
            return $agents;
        }

        public function getFiltersCriteria()
        {
            $talonaryNumberFilter = new FilterCriteria();
            $talonaryNumberFilter->filterId = 1;
            $talonaryNumberFilter->filterBy = "talonaryNumber";
            $talonaryNumberFilter->filterByAlias = "# Talonario";
            $ticketNumberFilter = new FilterCriteria();
            $ticketNumberFilter->filterId = 2;
            $ticketNumberFilter->filterBy = "ticketNumber";
            $ticketNumberFilter->filterByAlias = "# Comparendo";
            return array($talonaryNumberFilter, $ticketNumberFilter);
        }
    }