<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../database/ipat_incharge_dao.php');
    include_once('user_business_layer.php');

    class IpatInChargeBusinessLayer
    {
        private $ipatInChargeDao;
        private $userBusinessLayer;

        function __construct()
        {
            $this->ipatInChargeDao = new IpatInChargeDao();
            $this->userBusinessLayer = new UserBusinessLayer();
        }

        public function readAll()
        {
            return $this->ipatInChargeDao->readAll();
        }

        public function readOne($username)
        {
            $user = $this->userBusinessLayer->readOneByUsername($username);
            if ($user != null)
            {
                return $this->ipatInChargeDao->readOne($user->id);
            }
            return null;
        }
    }
?>
