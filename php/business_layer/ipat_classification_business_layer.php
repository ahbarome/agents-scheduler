<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../database/ipat_classification_dao.php');

    class IpatClassificationBusinessLayer
    {
        private $ipatClassificationDao;

        function __construct()
        {
            $this->ipatClassificationDao = new IpatClassificationDao();
        }

        public function readAll()
        {
            return $this->ipatClassificationDao->readAll();
        }
    }
?>