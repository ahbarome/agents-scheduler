<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('base_business_layer.php');
    include_once('../../database/transit_ticket_transaction_novelty_dao.php');

    class TransitTicketTransactionNoveltyBusinessLayer extends BaseBusinessLayer
    {
        private $dao;

        function __construct()
        {
            parent::__construct();
            $this->dao = new TransitTicketTransactionNoveltyDao();
        }

        public function save($novelty)
        {
            $novelty->createdBy = $this->getUserName();
            $novelty->createdAt = $this->getCurrentDateTime();
            return $this->dao->save($novelty);
        }

        public function countAll()
        {
            return $this->dao->countAll();
        }

        public function countAllByAgent($plaqueAgent = null)
        {
            if ($plaqueAgent != null)
            {
                return $this->dao->countAllByAgent($plaqueAgent);
            }
            else
            {
                return $this->countAll();
            }
        }
    }