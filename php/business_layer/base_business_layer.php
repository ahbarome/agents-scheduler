<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../common/america_datetime.php');
    include_once('../../session/session_manager.php');

    class BaseBusinessLayer
    {
        protected $sessionManager;
        private $americaDateTime;

        function __construct()
        {
            $this->sessionManager = new SessionManager();
            $this->americaDateTime = new AmericaDateTime();
        }

        protected function getUserName()
        {
            return $this->sessionManager->getUserName();
        }

        protected function getPassword()
        {
            return $this->sessionManager->getPassword();
        }

        protected function getCurrentDateTime()
        {
            return $this->americaDateTime->getCurrentDateTime()->format(
                AmericaDateTime::DEFAULT_DATETIME_FORMAT);
        }
    }
?>