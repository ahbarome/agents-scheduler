<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('base_business_layer.php');
    include_once('user_business_layer.php');
    include_once('../../database/agent_dao.php');
    include_once('../../session/session_manager.php');

    class AgentBusinessLayer extends BaseBusinessLayer
    {
        private $dao;

        /**
         * AgentBusinessLayer constructor.
         */
        public function __construct()
        {
            parent::__construct();
            $this->dao = new AgentDao();
        }

        /**
         * Create a new agent.
         *
         * @param $agent to be created.
         * @return bool true if the agent was created successfully, false otherwise.
         */
        public function create($agent)
        {
            if (isset($agent) && $this->exist($agent->plaque))
            {
                // Print message when the user already exist.
                return false;
            }

            $created = $this->dao->create($agent);
            return $created;
        }

        /**
         * Validates if an agent already exist.
         *
         * @param $plaque of the agent to validate.
         * @return bool True if the agent exist, false otherwise.
         */
        public function exist($plaque)
        {
            if (!isset($plaque))
            {
                return false;
            }

            $agent = $this->readOne($plaque);

            return isset($agent) && $agent != null && $agent->plaque != null;
        }

        /**
         * Read one agent by plaque.
         *
         * @param $plaque of the agent.
         * @return Agent according it's plaque.
         */
        public function readOne($plaque)
        {
            $agent = $this->dao->readOne($plaque);
            return $agent;
        }

        /**
         * Read all the agents.
         *
         * @return array of agents.
         */
        public function readAll()
        {
            $agents = $this->dao->readAll();
            return $agents;
        }

        /**
         * Read all the active agents.
         *
         * @return array of active agents.
         */
        public function readAllActive()
        {
            $agents = $this->dao->readAllActive();
            return $agents;
        }

        public function readAllActiveByPlaques($plaques)
        {
            $agents = $this->dao->readAllActiveByPlaques($plaques);
            return $agents;
        }

        /**
         * Update a specific agent.
         *
         * @param $agent to be updated.
         * @return bool true if the update was performed successfully, false otherwise.
         */
        public function update($agent)
        {
            $updated = $this->dao->update($agent);
            return $updated;
        }

        /**
         * Delete a specific agent by it's plaque.
         *
         * @param $plaque of the agent to be deleted.
         * @return bool true if the agent was deleted successfully, false otherwise.
         */
        public function delete($plaque)
        {
            $deleted = $this->dao->delete($plaque);
            return $deleted;
        }

        /**
         * Delete agents by their ids.
         *
         * @param $ids of the agents to be deleted.
         * @return bool true if the agents were deleted successfully, false otherwise.
         */
        public function deleteSelected($ids)
        {
            $deleted = $this->dao->deleteSelected($ids);
            return $deleted;
        }

        /**
         * Read an agent by the current user in the session.
         *
         * @return an agent if the user exist in the userByAgent table, null otherwise.
         */
        public function readAgentByCurrentSessionUser()
        {
            $userBL = new UserBusinessLayer();
            $user =
                $userBL->readOneByUsernameAndPassword(
                    $this->getUserName(), $this->getPassword());
            $agent = $this->dao->readOneByUserId($user->id);
            return $agent;
        }

        /**
         * Export all the agents into a string which is going to be outputted in a CSV file.
         *
         * @return mixed content with all the agents.
         */
        public function export_CSV()
        {
            $content = $this->dao->export_CSV();
            return $content;
        }
    }
