<?php

    /*
    * Copyright (c) 2018 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('base_business_layer.php');
    include_once('../../database/section_dao.php');
    
    final class SectionBusinessLayer extends BaseBusinessLayer
    {
        private $dao;
        
        public function __construct()
        {
            parent::__construct();
            $this->dao = new SectionDao();
        }

        public function exportCSV()
        {
            return $this->dao->export_CSV();
        }

        public function save($section)
        {
            return $this->dao->save($section);
        }

        public function readAll()
        {
            return $this->dao->readAll();
        }

        public function readOne($id)
        {
            return $this->dao->readOne($id);
        }

        public function update($section)
        {
            return $this->dao->update($section);
        }
        
        public function delete($id)
        {
            return $this->dao->delete($id);
        }

        public function deleteSelected($ids)
        {
            return $this->dao->deleteSelected($ids);
        }
    }
?>