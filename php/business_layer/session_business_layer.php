<?php

    /*
    * Copyright (c) 2018 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('base_business_layer.php');
    include_once('user_business_layer.php');
    include_once('../../common/guid_manager.php');
    include_once('../../database/session_dao.php');

    final class SessionBusinessLayer extends BaseBusinessLayer
    {
        private $dao;
        private $userBL;

        public function __construct()
        {
            parent::__construct();
            $this->dao = new SessionDao();
            $this->userBL = new UserBusinessLayer();
        }

        public function readAll()
        {
            $sessions = $this->dao->readAll();
            return $sessions;
        }

        public function exist()
        {
            $username = $this->getUserName();
            $password = $this->getPassword();
            if ($username == null || $password == null)
            {
                return false;
            }

            $user = $this->userBL->readOneByUsernameAndPassword($username, $password);
            if ($user == null)
            {
                return false;
            }

            $storedSession = $this->dao->readOneByUserId($user->id);
            if ($storedSession == null)
            {
                $this->sessionManager->closeSession();
                return false;
            }

            return true;
        }

        public function refresh()
        {
            $username = $this->getUserName();
            $password = $this->getPassword();
            if ($username == null || $password == null)
            {
                return;
            }

            $user = $this->userBL->readOneByUsernameAndPassword($username, $password);
            if ($user == null)
            {
                return;
            }

            $storedSession = $this->dao->readOneByUserId($user->id);
            if ($storedSession == null)
            {
                $session = new Session();
                $session->id = GuidManager::getGuid();
                $session->idUser = $user->id;
                $session->createdAt = $this->getCurrentDateTime();
                $session->sessionTimeout = 0;
                $this->dao->save($session);
            }
            else
            {
                $this->dao->update($user->id, $this->getCurrentDateTime());
            }
        }
    }
?>