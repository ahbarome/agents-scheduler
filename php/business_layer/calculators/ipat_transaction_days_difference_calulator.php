<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../common/america_datetime.php');
    include_once('../../model/ipat_transaction_state.php');

    /**
     * Class IpatTransactionDaysDifferenceCalculator
     */
    class IpatTransactionDaysDifferenceCalculator
    {
        /**
         * @var IpatTransactionDaysDelayDeliveryCalculator allows to get the difference between
         * the delivery date and the current date.
         */
        private $delayDeliveryDateCalculator;
        /**
         * @var IpatTransactionDaysOfDelayReceptionCalculator allows to get the difference between
         * the accident date date and the reception date.
         */
        private $delayReceptionDateCalculator;

        /**
         * IpatTransactionDaysDifferenceCalculator constructor.
         * @param $transactions collection.
         */
        function __construct($transactions)
        {
            $this->delayDeliveryDateCalculator =
                new IpatTransactionDaysDelayDeliveryCalculator($transactions);
            $this->delayReceptionDateCalculator =
                new IpatTransactionDaysOfDelayReceptionCalculator($transactions);
        }

        /**
         * Allows to calculate and set the difference in days between dates.
         */
        public function calculateAndSetDifference()
        {
            $this->delayDeliveryDateCalculator->calculateAndSetDifference();
            $this->delayReceptionDateCalculator->calculateAndSetDifference();
        }
    }

    /**
     * @internal Allow to calculate the difference in days between the ipats delivery date
     * and the current date. This class must be used internally not in other classes.
     */
    class IpatTransactionDaysDelayDeliveryCalculator
    {
        /**
         * @var collection ipats transactions.
         */
        private $transactions;
        /**
         * @var AmericaDateTime manage the datetime according to the american timezone.
         */
        private $americaDateTime;

        /**
         * IpatTransactionDaysDelayDeliveryCalculator constructor.
         * @param $transactions collection.
         */
        function __construct($transactions)
        {
            $this->transactions = $transactions;
            $this->americaDateTime = new AmericaDateTime();
        }

        /**
         * Calculate and set the difference in days between the ipats delivery date and the
         * current date.
         */
        public function calculateAndSetDifference()
        {
            $currentDateTime = $this->americaDateTime->getCurrentDateTime();

            foreach ($this->transactions as $key => $transaction)
            {
                if ($transaction->idTransactionState == IpatTransactionState::DELIVERED
                    && $transaction->deliveryDate != null)
                {
                    $deliveryDateTime =
                        new DateTime(
                            $transaction->deliveryDate,
                            new DateTimeZone(AmericaDateTime::TIME_ZONE));
                    $diffInterval =
                        $currentDateTime->diff($deliveryDateTime);

                    $transaction->daysOfDelayReception =
                        $diffInterval->format(AmericaDateTime::DAYS_DIFF);
                }
            }
        }
    }

    /**
     * @internal Allow to calculate the difference in days between the accident date
     * and the ipats reception date. This class must be used internally not in other classes.
     */
    class IpatTransactionDaysOfDelayReceptionCalculator
    {
        /**
         * @var collection of ipats transactions.
         */
        private $transactions;

        /**
         * IpatTransactionDaysOfDelayReceptionCalculator constructor.
         * @param $transactions collection.
         */
        function __construct($transactions)
        {
            $this->transactions = $transactions;
        }

        /**
         * Calculate and set the difference in days between the diligence date and the
         * reception date.
         */
        public function calculateAndSetDifference()
        {
            foreach ($this->transactions as $key => $transaction)
            {
                if ($transaction->idTransactionState == IpatTransactionState::RECEIVED
                    && $transaction->diligenceDate != null)
                {
                    $accidentDateTime =
                        new DateTime(
                            $transaction->diligenceDate,
                            new DateTimeZone(AmericaDateTime::TIME_ZONE));
                    $receptionDateTime =
                        new DateTime(
                            $transaction->receptionDate,
                            new DateTimeZone(AmericaDateTime::TIME_ZONE));
                    $diffInterval =
                        $receptionDateTime->diff($accidentDateTime);

                    $transaction->daysOfDelayDelivery =
                        $diffInterval->format(AmericaDateTime::DAYS_DIFF);
                }
            }
        }
    }
