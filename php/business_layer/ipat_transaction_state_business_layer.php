<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../database/ipat_transaction_state_dao.php');

    class IpatTransactionStateBusinessLayer
    {
        private $ipatTransactionStateDao;

        function __construct()
        {
            $this->ipatTransactionStateDao = new IpatTransactionStateDao();
        }

        public function readAll()
        {
            return $this->ipatTransactionStateDao->readAll();
        }

        public function readOne($id)
        {
            return $this->ipatTransactionStateDao->readOne($id);
        }

        public function readAllDifferentToDelivered()
        {
            return $this->ipatTransactionStateDao->readAllDifferentTo(IpatTransactionState::DELIVERED);
        }
    }
?>
    