<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('agent_business_layer.php');
    include_once('base_business_layer.php');
    include_once('ipat_business_layer.php');
    include_once('role_business_layer.php');
    include_once('user_business_layer.php');
    include_once('ipat_transaction_novelty_business_layer.php');
    include_once('calculators/ipat_transaction_days_difference_calulator.php');
    include_once('exporters/ipat_transaction_exporter.php');
    include_once('exporters/ipat_transaction_ministry_exporter.php');
    include_once('exporters/ipat_transaction_ministry_pdf_exporter.php');
    include_once('exporters/ipat_transaction_peace_and_save_pdf_exporter.php');
    include_once('validators/delivered_ipat_transaction_validator.php');
    include_once('validators/delivered_date_ipat_transaction_validator.php');
    include_once('validators/received_ipat_transaction_validator.php');
    include_once('validators/received_with_pending_ipat_transaction_validator.php');
    include_once('../../cache/ipat_transaction_cache.php');
    include_once('../../common/america_datetime.php');
    include_once('../../common/message_printer.php');
    include_once('../../database/ipat_dao.php');
    include_once('../../database/ipat_delivery_dao.php');
    include_once('../../model/filter_criteria.php');
    include_once('../../model/ipat_transaction_state.php');
    include_once('../../model/ipat_transaction_statistic.php');
    include_once('../../model/role.php');

    /**
     * Class IpatDeliveryBusinessLayer manage the business logic for ipat transactions
     */
    final class IpatDeliveryBusinessLayer extends BaseBusinessLayer
    {
        const MAX_QUANTITY_IPATS_PER_AGENT = 5;
        const MESSAGE_WHEN_IPAT_IS_EXPIRED = 'Expiró';
        const EMPTY_STRING = '';

        private $ipatDao;
        private $dao;
        private $agentBL;
        private $ipatBL;
        private $roleBL;
        private $userBL;
        private $validators;
        private $ipatTransactionNoveltyBusinessLayer;

        /**
         * IpatDeliveryBusinessLayer constructor.
         */
        function __construct()
        {
            parent::__construct();
            $this->ipatDao = new IpatDao();
            $this->dao = new IpatDeliveryDao();
            $this->agentBL = new AgentBusinessLayer();
            $this->ipatBL = new IpatBusinessLayer();
            $this->userBL = new UserBusinessLayer();
            $this->roleBL = new RoleBusinessLayer();
            $this->validators = array();
            $this->validators[IpatTransactionState::DELIVERED] =
                new DeliveredIpatTransactionValidator();
            $this->validators[IpatTransactionState::RECEIVED_WITH_PENDING] =
                new ReceivedWithPendingIpatTransactionValidator();
            $this->validators[IpatTransactionState::RECEIVED] =
                new ReceivedIpatTransactionValidator();
            $this->ipatTransactionNoveltyBusinessLayer =
                new IpatTransactionNoveltyBusinessLayer();
        }

        /**
         * Read all the transactions.
         *
         * @return array with all the ipat transactions.
         */
        public function readAllDeliveries()
        {
            $deliveries = $this->dao->readAllSorted();
            $this->markExpiredDeliveries($deliveries);
            return $deliveries;
        }

        public function readDeliveries($filterCriteria)
        {
            $deliveries = array();
            if ($filterCriteria != null
                && $filterCriteria->valueFilter != null
                && $filterCriteria->valueFilter != "")
            {
                $deliveries = $this->dao->readAllUsingFilterCriteria($filterCriteria);
            }
            return $deliveries;
        }

         /**
         * Read the last the transactions.
         *
         * @return array with all the ipat transactions.
         */
        public function readLastDeliveries()
        {
            $cache = IpatTransactionCache::getInstance()->getTransactionsCache();
            if (!is_null($cache))
            {
                return $cache;
            }

            $deliveries = $this->dao->readLastSorted();
            $this->markExpiredDeliveries($deliveries);

            return $deliveries;
        }

        /**
         * Read one specific transaction.
         *
         * @param $ipatNumber to be read.
         * @return null if the ipat was not found, otherwise is going to return the transaction.
         */
        public function readOneIpatDelivery($ipatNumber)
        {
            $ipatDeliveries = $this->dao->readOne($ipatNumber);
            if (count($ipatDeliveries) > 0)
            {
                return array_values($ipatDeliveries)[0];
            }
            return null;
        }

        /**
         * Read all the transactions for a specific delivered agent.
         *
         * @param $deliveredTo plaque of the agent which was delivered the ipat.
         * @return array|null if were not found the transactions is going to return null, otherwise
         * is going to return all the transactions for the specific agent.
         */
        public function readIpatsDelivered($deliveredTo)
        {
            $ipatsDelivered = null;
            if($deliveredTo != null)
            {
                 $ipatsDelivered =
                    $this->dao->readAllByAgentAndTransactionState(
                        $deliveredTo, IpatTransactionState::DELIVERED);
            }
            return $ipatsDelivered;
        }

        public function readTotalIpatsDelivered($deliveredTo)
        {
            $totalPending = 0;
            if($deliveredTo != null)
            {
                $totalPending =
                    $this->dao->getTotalByAgentAndTransactionState(
                        $deliveredTo, IpatTransactionState::DELIVERED);
            }
            return $totalPending;
        }

        public function deliverIpat($transaction)
        {
            $isValid = $this->validators[IpatTransactionState::DELIVERED]->isValid($transaction);

            if ($isValid)
            {
                $totalPending = $this->readTotalIpatsDelivered($transaction->deliveredTo);

                $currentIpat = $this->ipatDao->readCurrent();

                $lastDelivery = $this->dao->getLastIpatNumberDelivered();
                $initialIpatNumber = $currentIpat->initialNumber;

                $currentIpatNumber =
                    $lastDelivery >= $initialIpatNumber ? $lastDelivery + 1 : $initialIpatNumber;

                $ipatNumbersDelivered = array();
                $quantity = $transaction->quantity;

                $this->validateQuantitiesAgaintsTotalPending($quantity, $totalPending);

                $role = $this->getRole();
                if ($role != null && intval($role->id) == Role::ADMINISTRATOR_ROLE_ID)
                {
                    while ($quantity > 0)
                    {
                        $transaction->ipatNumber = $currentIpatNumber;
                        $transaction->idTransactionState = IpatTransactionState::DELIVERED;

                        $this->dao->create($transaction);
                        $currentIpatNumber = $currentIpatNumber + 1;
                        $quantity = $quantity - 1;
                        $totalPending = $totalPending + 1;

                        array_push($ipatNumbersDelivered, $transaction->ipatNumber);
                    }
                }
                else
                {
                    while ($quantity > 0 
                        && $totalPending < IpatDeliveryBusinessLayer::MAX_QUANTITY_IPATS_PER_AGENT)
                    {
                        $transaction->ipatNumber = $currentIpatNumber;
                        $transaction->idTransactionState = IpatTransactionState::DELIVERED;

                        $this->dao->create($transaction);
                        $currentIpatNumber = $currentIpatNumber + 1;
                        $quantity = $quantity - 1;
                        $totalPending = $totalPending + 1;

                        array_push($ipatNumbersDelivered, $transaction->ipatNumber);
                    }
                }

                MessagePrinter::printMessage(
                    sprintf('Se generaron los IPATs %s', implode(",", $ipatNumbersDelivered)));

                return true;
            }
            return false;
        }

        public function receiveIpat($transaction)
        {
            $received = false;
            $validator = $this->validators[$transaction->idTransactionState];
            $isValid = $validator->isValid($transaction);
            if ($isValid)
            {
                $transaction->modifiedBy = $this->getUserName();
                switch ($transaction->idTransactionState)
                {
                    case IpatTransactionState::RECEIVED_WITH_PENDING:
                    {
                        $partialReceptionDate =
                            $this->getPartialReceptionDate($transaction->ipatNumber);
                        $transaction->partialReceptionDate = $partialReceptionDate;
                        $received = $this->dao->updateTransactionState($transaction);
                        break;
                    }
                    case IpatTransactionState::RECEIVED:
                    {
                        $receptionDate = $this->getReceptionDate($transaction->ipatNumber);
                        $transaction->receptionDate = $receptionDate;
                        $received = $this->dao->updateTransaction($transaction);
                        break;
                    }
                    default:
                    {
                        $received = false;
                        break;
                    }
                }
            }
            return $received;
        }

        /**
         * Update all the field for the transaction.
         *
         * @param $transaction to be updated.
         * @return bool true if the transaction was modified, false otherwise.
         */
        public function modify($transaction)
        {
            if (!isset($transaction))
            {
                return false;
            }

            $transaction->modifiedBy = $this->getUserName();
            $transaction->modifiedAt = $this->getCurrentDateTime();

            $modified = $this->dao->updateAll($transaction);
            return $modified;
        }

        public function editIpat($ipatDelivery)
        {
            $validator = $this->validators[$ipatDelivery->idTransactionState];
            $isValid = $validator->isValid($ipatDelivery);
            $edited = false;
            if ($isValid && $ipatDelivery->idTransactionState == IpatTransactionState::DELIVERED)
            {
                $edited = $this->dao->updateIpatDelivery($ipatDelivery);
            }
            return $edited;
        }

        public function readAllStatistics($deliveredTo)
        {
            $statistics = array();
            if ($deliveredTo == null)
            {
                $statistics = $this->dao->readAllStatistics();
            }
            else
            {
                $statistics = $this->dao->readAllStatisticsByAgent($deliveredTo);
            }
            $remainingIpats = $this->getIpatsRemainingTransactionStatistic();
            array_push($statistics, $remainingIpats);
            return $statistics;
        }

        public function readLastTwoIpatRanges()
        {
            $ranges = $this->readIpatRanges();
            if (count($ranges) > 0)
            {
                foreach ($ranges as $key => $range)
                {
                    $totalInRange = abs($range->finalNumber - $range->initialNumber);
                    $totalProcessed =
                        $this->dao->countTransactionsBetweenRange(
                            $range->initialNumber, $range->finalNumber);

                    $totalDelivered =
                        $this->dao->countTransactionsBetweenRangeAndInState(
                            $range->initialNumber,
                            $range->finalNumber,
                            IpatTransactionState::DELIVERED);

                    $totalProcessed = $totalProcessed > $totalInRange ? $totalInRange : $totalProcessed;
                    $totalMissing = abs($totalInRange - $totalProcessed);

                    // Properties added on execution time
                    $range->totalInRange = $totalInRange;
                    $range->totalProcessed = $totalProcessed;
                    $range->totalPendingEntry = $totalDelivered;
                    $range->totalPendingEntryPercentage =
                        number_format(($totalDelivered / $totalInRange) * 100, 2, ',', ' ');
                    $range->totalMissing = $totalMissing;
                    $range->percentageMissing =
                        number_format(($totalMissing / $totalInRange) * 100, 2, ',', ' ');
                }
            }
            else
            {
                $ranges = array();
            }
            return $ranges;
        }

         /**
         * Read all the transactions of the agent based on the ipatId and the specific states that
         * represent a pending transaction like transactions in delivered state and
         * partially received.
         *
         * @param $plaque of the agent to search the transactions.
         * @param $ipatId identification of the ipat that allows to get the specific range of
         * ipats.
         * @return array|null if were not found ipats is going to return null, otherwise
         * is going to return all the delivered transactions and partially received.
         */
        public function readPendingIpatsPeaceAndSave($plaque, $ipatId)
        {
            $transactions = array();

            $agent = $this->agentBL->readOne($plaque);
            $ipat = $this->ipatBL->readOne($ipatId);

            if ($agent != null && $ipat != null)
            {
                $delivered =
                    $this->dao->getTransactionsByAgentAndIpatRangeAndIdTransactionState(
                        $plaque,
                        $ipat->initialNumber,
                        $ipat->finalNumber,
                        IpatTransactionState::DELIVERED);

                if ($delivered != null)
                {
                    array_push($transactions, $delivered);
                }

                $partiallyReceived =
                    $this->dao->getTransactionsByAgentAndIpatRangeAndIdTransactionState(
                        $plaque,
                        $ipat->initialNumber,
                        $ipat->finalNumber,
                        IpatTransactionState::RECEIVED_WITH_PENDING);

                if ($partiallyReceived != null)
                {
                    array_push($transactions, $partiallyReceived);
                }
            }

            return $transactions;
        }

        /**
         * Read the current, next and previous ipat range and load those values into an array.
         *
         * @return array|null if were not found ipats is going to return null, otherwise
         * is going to return the current, next and previous ipat range.
         */
        public function readIpatRanges()
        {
            $range = array();
            $currentIpat = $this->ipatBL->readCurrent();
            if ($currentIpat != null)
            {
                array_push($range, $currentIpat);

                $nextIpat =
                    $this->ipatBL->readFirstInitialNumberGreatherOrEqualTo(
                        $currentIpat->finalNumber);
                if ($nextIpat != null)
                {
                    array_push($range, $nextIpat);
                }

                $previousIpat =
                    $this->ipatBL->readFirstFinalNumberLessThan($currentIpat->initialNumber);
                if ($previousIpat != null)
                {
                    array_push($range, $previousIpat);
                }
            }
            return $range;
        }

        /**
         * Generate the content to be exported for all the ipats stored
         *
         * @return string with content to be exported
         */
        public function exportCSV()
        {
            $transactions = $this->dao->readAll();
            $diffDatesCalculator = new IpatTransactionDaysDifferenceCalculator($transactions);
            $diffDatesCalculator->calculateAndSetDifference();
            $exporter = new IpatTransactionExporter($transactions);
            $content = $exporter->exportContent();
            return $content;
        }

        /**
         * Generate the content to be exported for the ministry
         *
         * @param $exportDate to export the data
         * @return string with content to be exported for the ministry
         */
        public function exportCSVMinistry($exportDate)
        {
            $transactions = $this->dao->readAllSortedByReceptionDate($exportDate);
            $diffDatesCalculator = new IpatTransactionDaysDifferenceCalculator($transactions);
            $diffDatesCalculator->calculateAndSetDifference();
            $exporter = new IpatTransactionMinistryExporter($transactions, $exportDate);
            $content = $exporter->exportContent();
            return $content;
        }

        /**
         * Export a PDF with all the received transactions in a specific date.
         *
         * @param $exportDate to export the data.
         */
        public function exportPDFMinistry($exportDate)
        {
            $transactions = $this->dao->readAllSortedByReceptionDate($exportDate);
            $diffDatesCalculator = new IpatTransactionDaysDifferenceCalculator($transactions);
            $diffDatesCalculator->calculateAndSetDifference();
            $exporter = new IpatTransactionMinistryPDFExporter($transactions, $exportDate);
            $exporter->exportContent();
        }

        /**
         * Generate the Peace and Save PDF based on the plaque of the agent and the ipat
         * identification. The Peace and Save PDF with a positive information is going to
         * be generated if the agent does not have ipat transactions in stata delivery or
         * partially received, a negative PDF otherwise.
         *
         * @param $plaque of the agent to export the data.
         * @param $ipatId identification of the ipat that allows to get the specific range of
         * ipats.
         */
        public function exportPDFPeaceAndSave($plaque, $ipatId)
        {

            if (!isset($plaque) || isset($ipatId))
            {
                return;
            }

            $agent = $this->agentBL->readOne($plaque);
            $ipat = $this->ipatBL->readOne($ipatId);

            $total = 0;
            $totalPending = 0;
            $totalPartiallyReceived = 0;
            if($ipat != null)
            {
                $total =
                    $this->dao->getTotalByAgentAndIpatRange(
                        $plaque, $ipat->initialNumber, $ipat->finalNumber);

                $totalPending =
                    $this->dao->getTotalDeliveredByAgentAndIpatRange(
                        $plaque, $ipat->initialNumber, $ipat->finalNumber);

                $totalPartiallyReceived =
                    $this->dao->getTotalPartiallyReceivedByAgentAndIpatRange(
                        $plaque, $ipat->initialNumber, $ipat->finalNumber);
            }

            $exporter =
                new IpatTransactionPeaceAndSavePDFExporter(
                    $agent, $ipat,  $total, $totalPending, $totalPartiallyReceived);
            $exporter->exportContent();
        }

        public function isValidDeliveredDate($transaction)
        {
            $isValid = false;
            if ($transaction->deliveredTo != null)
            {
                $transactions =
                    $this->dao->readAllByDeliveredToAndTransactionState(
                        $transaction->deliveredTo, IpatTransactionState::DELIVERED);
                if (count($transactions) > 0)
                {
                    $validator = new DeliveredDateIpatTransactionValidator();
                    foreach ($transactions as $key => $transaction)
                    {
                        $isValid = $validator->isValid($transaction);
                        if (!$isValid)
                        {
                            break;
                        }
                    }
                }
                else
                {
                    $isValid = true;
                }
            }
            else
            {
                $isValid = true;
            }
            return $isValid;
        }

        public function addNovelty($novelty)
        {
            return $this->ipatTransactionNoveltyBusinessLayer->save($novelty);
        }

        public function getFiltersCriteria()
        {
            $ipatNumberFilter = new FilterCriteria();
            $ipatNumberFilter->filterId = 1;
            $ipatNumberFilter->filterBy = "ipatNumber";
            $ipatNumberFilter->filterByAlias = "# IPAT";
            $vehiclePlaqueFilter = new FilterCriteria();
            $vehiclePlaqueFilter->filterId = 2;
            $vehiclePlaqueFilter->filterBy = "vehiclePlaque";
            $vehiclePlaqueFilter->filterByAlias = "Placa Vehiculo(s)";
            $deliveredToFilter = new FilterCriteria();
            $deliveredToFilter->filterId = 3;
            $deliveredToFilter->filterBy = "deliveredTo";
            $deliveredToFilter->filterByAlias = "Entregado A";
            $criminalNewsFilter = new FilterCriteria();
            $criminalNewsFilter->filterId = 4;
            $criminalNewsFilter->filterBy = "criminalNews";
            $criminalNewsFilter->filterByAlias = "# Noticia Criminal";
            return array(
                $ipatNumberFilter,
                $vehiclePlaqueFilter,
                $deliveredToFilter,
                $criminalNewsFilter);
        }

        public function readAllActiveAgentsWithIpatTransactions()
        {
            $plaques = $this->dao->readDistinctAgentPlaques();
            $agents = array();
            if($plaques != null && count($plaques) > 0)
            {
                $agents = $this->agentBL->readAllActiveByPlaques($plaques);
            }
            return $agents;
        }

        public function reset($ipatNumber)
        {
            if (!isset($ipatNumber) || $ipatNumber == null)
            {
                return false;
            }

            $modifiedBy = $this->getUserName();
            $reseted = $this->dao->reset($ipatNumber, $modifiedBy);
            return $reseted;
        }

        private function getPartialReceptionDate($ipatNumber)
        {
            $ipatTransaction = $this->readOneIpatDelivery($ipatNumber);
            $dateTime = new AmericaDateTime();
            return $ipatTransaction->partialReceptionDate != null ?
                $ipatTransaction->partialReceptionDate
                : $dateTime->getCurrentDateTime()->format(
                    AmericaDateTime::DEFAULT_DATETIME_FORMAT);
        }

        private function getReceptionDate($ipatNumber)
        {
            $ipatTransaction = $this->readOneIpatDelivery($ipatNumber);
            $dateTime = new AmericaDateTime();
            return
                $ipatTransaction->receptionDate != null
                    ? $ipatTransaction->receptionDate
                    : $dateTime->getCurrentDateTime()->format(
                    AmericaDateTime::DEFAULT_DATETIME_FORMAT);
        }

        /**
         * Select and mark all the expired transactions
         *
         * @param $transactions to validate and mark as expired
         */
        private function markExpiredDeliveries($transactions)
        {
            $validator = new DeliveredDateIpatTransactionValidator();
            foreach ($transactions as $key => $transaction)
            {
                $isValid = $validator->isValid($transaction);
                $transaction->expired =
                    !$isValid
                        ? IpatDeliveryBusinessLayer::MESSAGE_WHEN_IPAT_IS_EXPIRED
                        : IpatDeliveryBusinessLayer::EMPTY_STRING;
            }
        }

        private function getIpatsRemainingTransactionStatistic()
        {
            $currentIpat = $this->ipatDao->readCurrent();
            $lastDelivery = $this->dao->getLastIpatNumberDelivered();
            $remaining = 0;

            if ($lastDelivery > 0
                && $lastDelivery < $currentIpat->finalNumber
                && $lastDelivery >= $currentIpat->initialNumber)
            {
                $remaining =
                    ($currentIpat->finalNumber - $currentIpat->initialNumber)
                    - (1 + $lastDelivery - $currentIpat->initialNumber);
            }
            else
            {
                $remaining = ($currentIpat->finalNumber - $currentIpat->initialNumber);
            }

            $transactionState = new IpatTransactionState();
            $transactionState->id = IpatTransactionState::REMAINING;
            $transactionState->name = IpatTransactionState::REMAINING_DESCRIPTION;

            $ipatTransactionRemaining = new IpatTransactionStatistic();
            $ipatTransactionRemaining->idTransactionState = $transactionState->id;
            $ipatTransactionRemaining->totalByTransactionState = $remaining < 0 ? 0 : $remaining;
            $ipatTransactionRemaining->transactionState = $transactionState;

            return $ipatTransactionRemaining;
        }

        private function validateQuantitiesAgaintsTotalPending($quantity, $totalPending)
        {
            $isQuantityAllowed =
                ($quantity + $totalPending)
                <= IpatDeliveryBusinessLayer::MAX_QUANTITY_IPATS_PER_AGENT;
            if (!$isQuantityAllowed)
            {
                $quantityAllowed =
                    IpatDeliveryBusinessLayer::MAX_QUANTITY_IPATS_PER_AGENT - $totalPending;
                MessagePrinter::printMessage(
                    sprintf('Por configuración se crearan %s IPATs', $quantityAllowed));
            }
        }

        private function getRole()
        {
            $username = $this->getUserName();
            $password = $this->getPassword();

            if ($username == null || $password == null)
            {
                return null;
            }

            $user = $this->userBL->readOneByUsernameAndPassword($username, $password);
            if ($user == null)
            {
                return;
            }

            $role = $this->roleBL->readOne($user->idRole);
            return $role;
        }
    }
