<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../builder/schedule_builder.php');
    include_once('../../database/schedule_dao.php');
    include_once('agent_business_layer.php');
    include_once('service_configuration_business_layer.php');

    class ScheduleBusinessLayer
    {
        private $agentBusinessLayer;
        private $serviceConfigurationBusinessLayer;
        private $scheduleDao;
        private $scheduleBuilder;

        public function __construct()
        {
            $this->agentBusinessLayer = new AgentBusinessLayer();
            $this->serviceConfigurationBusinessLayer = new ServiceConfigurationBusinessLayer();
            $this->scheduleDao = new ScheduleDao();
            $this->scheduleBuilder = new ScheduleBuilder();
        }

        public function create($schedule)
        {
            $success = $this->scheduleDao->createSchedule($schedule);
            return $success;
        }

        public function schedule($schedule)
        {
            $agents = $this->agentBusinessLayer->readAllActive();
            $serviceConfigurations = $this->serviceConfigurationBusinessLayer->readAll();

            foreach ($serviceConfigurations as $serviceConfiguration)
            {
                $randomAgents = $this->getRandomAgents($serviceConfiguration->estimateAgentsQty, $agents);
                $this->scheduleDao->createSchedule($schedule);
                foreach ($randomAgents as $randomAgent)
                {
                    $scheduleDetail =  $this->scheduleBuilder->buildScheduleDetail($schedule->scheduleDate, $serviceConfiguration->id, $randomAgent->plaque);
                    $this->scheduleDao->createScheduleDetail($scheduleDetail);
                }
            }

            return $serviceConfigurations;
        }

        public function readAllSchedules()
        {
            $schedules = $this->scheduleDao->readAllSchedules();
            return $schedules;
        }

        public function readAllScheduleDetails($scheduleDate)
        {
            $scheduleDetails = $this->scheduleDao->readAllScheduleDetails($scheduleDate);
            return $scheduleDetails;
        }

        public function readOneScheduleDetail($scheduleDate)
        {
            $scheduleDetails = $this->scheduleDao->readOneScheduleDetail($scheduleDate);
            return $scheduleDetail;
        }

        private function getRandomAgents($numberOfAgentsByService, $agents)
        {
            $randomAgents = array();
            $totalAgents = count($agents);
            $counterNumberOfAgentsByServiceAdded = 0;

            while ($counterNumberOfAgentsByServiceAdded < $numberOfAgentsByService)
            {
                $random = rand ( 0 ,  $totalAgents - 1 );
                $randomAgent = $agents[$random];
                if (!$this->existAgentByPlaque($randomAgents, $randomAgent->plaque ))
                {
                    array_push($randomAgents, $randomAgent);
                    $counterNumberOfAgentsByServiceAdded++;
                }
            }

            return $randomAgents;
        }

        private function existAgentByPlaque($agents, $plaque)
        {
            foreach ($agents as $agent)
            {
                if($agent->plaque == $plaque)
                {
                    return true;
                }
            }
            return false;
        }
    }
?>