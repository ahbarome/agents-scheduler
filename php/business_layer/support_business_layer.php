<?php

    /*
    * Copyright (c) 2018 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../database/support_dao.php');

    /**
     * Class SupportBusinessLayer manage the business logic for roles.
     */
    class SupportBusinessLayer
    {
        private $dao;

        /**
         * SupportBusinessLayer constructor.
         */
        function __construct()
        {
            $this->dao = new SupportDao();
        }

        public function execute($queryInput)
        {
            if (!isset($queryInput) || $queryInput == null)
            {
                return null;
            }

            $output = $this->dao->executeQuery($queryInput);
            return $output;
        }
    }
?>