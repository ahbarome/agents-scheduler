<?php

    /*
    * Copyright (c) 2018 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('base_business_layer.php');
    include_once('../../common/message_printer.php');
    include_once('../../model/user_by_agent.php');
    include_once('../../database/user_by_agent_dao.php');

    class UserByAgentBusinessLayer extends BaseBusinessLayer
    {

        const NOT_POSSIBLE_SAVE = 'No fue posible guardar el usuario por agente.';
        const NOT_POSSIBLE_UPDATE = 'No fue posible actualizar el usuario por agente.';

        private $dao;

        /**
         * UserByAgentBusinessLayer constructor.
         */
        function __construct()
        {
            parent::__construct();
            $this->dao = new UserByAgentDao();
        }

        public function save($idUser, $plaqueAgent)
        {
            if ($idUser === null || $plaqueAgent === null)
            {
                return false;
            }

            $userByAgent = new UserByAgent();
            $userByAgent->idUser = $idUser;
            $userByAgent->plaqueAgent = $plaqueAgent;

            $saved = $this->dao->save($userByAgent);
            if (!$saved)
            {
                MessagePrinter::printMessage(
                    UserBusinessLayer::NOT_POSSIBLE_SAVE);
            }
            return $saved;
        }

        public function update($idUser, $plaqueAgent)
        {
            if ($idUser === null || $plaqueAgent === null)
            {
                return false;
            }

            $userByAgent = new UserByAgent();
            $userByAgent->idUser = $idUser;
            $userByAgent->plaqueAgent = $plaqueAgent;

            $updated = $this->dao->update($userByAgent);
            if (!$updated)
            {
                MessagePrinter::printMessage(
                    UserBusinessLayer::NOT_POSSIBLE_UPDATE);
            }
            return $updated;
        }
    }
