<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('base_business_layer.php');
    include_once('user_by_agent_business_layer.php');
    include_once('../../common/message_printer.php');
    include_once('../../database/user_dao.php');

    class UserBusinessLayer extends BaseBusinessLayer
    {
        private $userDao;
        private $userByAgentBL;

        const MESSAGE_WHEN_PASSWORD_IS_INVALID = 'Contraseña inválida';
        const MESSAGE_WHEN_CURRENT_PASSWORD_AND_NEW_PASSWORD_ARE_DIFFERENT =
            'Por favor registre el password actual y nuevo password';
        const MESSAGE_WHEN_FOR_SHORT_PASSWORD =
            'La longitud de password debe ser de mínimo 6 caracteres y máximo 15';

        /**
         * UserBusinessLayer constructor.
         */
        function __construct()
        {
            parent::__construct();
            $this->userDao = new UserDao();
            $this->userByAgentBL = new UserByAgentBusinessLayer();
        }

        public function save($user)
        {
            if ($user == null)
            {
                return false;
            }

            $user->createdBy = $this->getUserName();
            $user->createdAt = $this->getCurrentDateTime();
            $user->password = $this->encryptPassword($user->password);

            $saved = $this->userDao->save($user);
            $storedUser = $this->readOneByUsername($user->username);

            if($storedUser !== null)
            {
                $this->userByAgentBL->save($storedUser->id, $user->plaqueAgent);
            }

            return $saved;
        }

        public function update($user)
        {
            if ($user == null)
            {
                return false;
            }

            $user->modifiedBy = $this->getUserName();
            $user->modifiedAt = $this->getCurrentDateTime();

            $storedUser = $this->readOneByUsername($user->username);
            if($storedUser->password !== $user->password)
            {
                $user->password = $this->encryptPassword($user->password);
            }

            $user->id = $storedUser->id;
            $updated = $this->userDao->update($user);

            $this->userByAgentBL->update($user->id, $user->plaqueAgent);

            return $updated;
        }

        public function readAll()
        {
            $users = $this->userDao->readAll();
            return $users;
        }

        public function exist($username, $password)
        {
            $user = $this->readOneByUsernameAndPassword($username, $password);
            return $user != null && $user->username != null;
        }

        public function readOne($id)
        {
            if ($id == null)
            {
                return $id;
            }

            $user = $this->userDao->readOne($id);
            return $user;
        }

        public function readOneByUsernameAndPassword($username, $password)
        {
            $encryptedPassword = $this->encryptPassword($password);
            $user = $this->userDao->readOneByUsernameAndPassword($username, $encryptedPassword);
            return $user;
        }

        public function readOneByUsername($username)
        {
            $user = $this->userDao->readOneByUsername($username);
            return $user;
        }

        public function changePassword($username, $password, $userToUpdate)
        {
            $existUser = $this->exist($userToUpdate->username, $userToUpdate->password);
            if (!$existUser)
            {
                MessagePrinter::printMessage(UserBusinessLayer::MESSAGE_WHEN_PASSWORD_IS_INVALID);
                return false;
            }
            if ( $userToUpdate->password == null
                || $userToUpdate->password == ''
                || $userToUpdate->newPassword == null
                || $userToUpdate->newPassword == '')
            {
                MessagePrinter::printMessage(
                    UserBusinessLayer::MESSAGE_WHEN_CURRENT_PASSWORD_AND_NEW_PASSWORD_ARE_DIFFERENT);
                return false;
            }
            if (strlen($userToUpdate->newPassword) < 5 
                || strlen($userToUpdate->newPassword) > 15)
            {
                MessagePrinter::printMessage(
                    UserBusinessLayer::MESSAGE_WHEN_FOR_SHORT_PASSWORD);
                return false;
            }
            $updated =
                $this->userDao->changePassword(
                    $username, $this->encryptPassword($userToUpdate->newPassword));
            return $updated;
        }

        private function encryptPassword($password)
        {
            return md5(base64_encode($password));
        }
    }
