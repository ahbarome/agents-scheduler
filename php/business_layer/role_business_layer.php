<?php
    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../common/message_printer.php');
    include_once('../../database/role_dao.php');

    /**
     * Class RoleBusinessLayer manage the business logic for roles.
     */
    class RoleBusinessLayer
    {
        private $roleDao;

        /**
         * RoleBusinessLayer constructor.
         */
        function __construct()
        {
            $this->roleDao = new RoleDao();
        }

        /**
         * Read all the roles.
         *
         * @return array|null with all the roles.
         */
        public function readAll()
        {
            $roles = $this->roleDao->readAll();
            return $roles;
        }

        /**
         * Read one role by it's id.
         *
         * @param $id of the role.
         * @return role with the id and name.
         */
        public function readOne($id)
        {
            $role = $this->roleDao->readOne($id);
            return $role;
        }
    }