<?php
    class IpatReception
    {
        public $ipatNumber;
        public $accidentDate;
        public $address;
        public $idSeverity;
        public $vehiclePlaque;
        public $receptionDate;
        public $recipientPlaqueAgent;
        public $deliveryPlaqueAgent;
    }
?>