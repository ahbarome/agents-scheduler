<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('auditable_entity.php');
   
    /**
     * Class TransitTicketTransaction model for transactions of transit's ticket.
     */
    class TransitTicketTransaction extends AuditableEntity
    {
        public $talonaryNumber;
        public $ticketNumber;
        public $diligenceDate;
        public $idTransactionState;
        public $infractorCode;
        public $plaqueAgent;
        public $immobilized;
        public $immobilizedSubject;
        public $immobilizedSite;

        public $agent;
        public $transactionState;
        public $totalNovelties;
    }