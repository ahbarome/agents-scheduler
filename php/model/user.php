<?php
    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('auditable_entity.php');

    /**
     * Class User define the structure for each user of the application.
     */
    class User extends AuditableEntity
    {
        /** Id of the user. */
        public $id;
        /** Id role of the user. */
        public $idRole;
        /** Plaque of the current agent. */
        public $plaqueAgent;
        /** User name. */
        public $username;
        /** Password of the user. */
        public $password;
        /** New password of the user. */
        public $newPassword;
        /** Role of the user. */
        public $role;
        /** Current agent assigned. */
        public $userByAgent;
    }
