<?php
   /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    /**
     * Model that represents the data for the report of ipats in state received with pending.
     * User: Anonymous
     * Date: 21/08/2017
     * Time: 10:43 AM
     */
    class ReportIpatsReceivedWithPendingState
    {
        public $agent;
        public $transactions;
        public $numberOfIpats;
    }