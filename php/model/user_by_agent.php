<?php

    /*
    * Copyright (c) 2018 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('auditable_entity.php');

    /**
     * Class UserByAgent define the structure for each user by agent of the application.
     */
    class UserByAgent extends AuditableEntity
    {
        /** Id of the user. */
        public $idUser;
        /** Plaque of the agent. */
        public $plaqueAgent;
    }
