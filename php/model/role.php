<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('auditable_entity.php');

    /**
     * Class Role represents a role for a user
     */
    class Role extends AuditableEntity
    {
        /** Id for the role. */
        public $id;
        /** Name for the role. */
        public $roleName;

        const ADMINISTRATOR_ROLE_ID = 1;
    }