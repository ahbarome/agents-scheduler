<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    class Ipat
    {
        public $id;
        public $initialNumber;
        public $finalNumber;
        public $assignmentDate;
        public $isCurrent;
        public $prefix;
        public $createdBy;
        public $createdAt;
        public $modifiedBy;
        public $modifiedAt;
    }
