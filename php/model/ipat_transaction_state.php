<?php

    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    class IpatTransactionState
    {
        public $id;
        public $name;

        const DELIVERED = 1;
        const RECEIVED_WITH_PENDING = 2;
        const RECEIVED = 3;
        const REMAINING = 4;
        const REMAINING_CURRENT_RANGE = 5;

        const REMAINING_DESCRIPTION = "Restante(s)";
    }
