<?php
class Inactivity
{
    // database connection and table name
    private $conn;
    private $table_name = "inactivity";

    // object properties
    public $id;
    public $description;
    public $plaqueAgent;
    public $startDate;
    public $endDate;

    // constructor with $db as database connection
    public function __construct($db)
    {
        $this->conn = $db;
    }

    // used to export records to csv
    public function export_CSV()
    {
        //select all data
        $query = "SELECT INA.*,
                            ITY.type description,
                            AGE.firstName agentFirstName,
                            AGE.lastName agentLastName,
                            SEC.name sectionName
                            FROM ". $this->table_name  ." INA
                            LEFT JOIN agent AGE ON AGE.plaque = INA.plaqueAgent
                            LEFT JOIN section SEC ON SEC.id = AGE.idSection
                            LEFT JOIN inactivityType ITY ON ITY.id = INA.idInactivityType";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        //this is how to get number of rows returned
        $num = $stmt->rowCount();
        $out = "# Inactividad,Descripción,Placa,Agente,Sección,Fecha Inicio,Fecha Fin\n";

        if($num>0)
        {
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
            {
                extract($row);
                $out.="{$id},\"{$description}\",\"{$plaqueAgent}\",{$agentFirstName} {$agentLastName},{$sectionName},{$startDate},{$endDate}\n";
            }
        }
        return $out;
    }

    // create agent
    function create()
    {
        // query to insert record
        $query =
            "INSERT INTO " . $this->table_name .
            "    SET    description=:description, startDate=:startDate, endDate=:endDate, plaqueAgent=:plaqueAgent";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->description=htmlspecialchars(strip_tags($this->description));
        $this->startDate=htmlspecialchars(strip_tags($this->startDate));
        $this->endDate=htmlspecialchars(strip_tags($this->endDate));
        $this->plaqueAgent=htmlspecialchars(strip_tags($this->plaqueAgent));

        // bind values
        $stmt->bindParam(":description", $this->description);
        $stmt->bindParam(":startDate", $this->startDate);
        $stmt->bindParam(":endDate", $this->endDate);
        $stmt->bindParam(":plaqueAgent", $this->plaqueAgent);

        // execute query
        if($stmt->execute()){
            return true;
        }else{
            echo "<pre>";
                print_r($stmt->errorInfo());
            echo "</pre>";

            return false;
        }
    }

    // read agents
    function readAll()
    {
        // select all query
        $query = "SELECT INA.*,
                            ITY.type description,
                            AGE.firstName agentFirstName,
                            AGE.lastName agentLastName,
                            SEC.name sectionName
                            FROM ". $this->table_name  ." INA
                            LEFT JOIN agent AGE ON AGE.plaque = INA.plaqueAgent
                            LEFT JOIN section SEC ON SEC.id = AGE.idSection
                            LEFT JOIN inactivityType ITY ON ITY.id = INA.idInactivityType";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        return $stmt;
    }

    // used when filling up the update inactivity form
    function readOne()
    {
        // query to read single record
        $query = "SELECT INA.*,
                            ITY.type description,
                            AGE.firstName agentFirstName,
                            AGE.lastName agentLastName,
                            SEC.name sectionName
                            FROM ". $this->table_name  ." INA
                            LEFT JOIN agent AGE ON AGE.plaque = INA.plaqueAgent
                            LEFT JOIN section SEC ON SEC.id = AGE.idSection
                            LEFT JOIN inactivityType ITY ON ITY.id = INA.idInactivityType
                            WHERE INA.id = :id
                            LIMIT 0,1";

        // prepare query statement
        $stmt = $this->conn->prepare( $query );
        // bind id of agent to be updated
        $stmt->bindParam(":id", $this->id);
        // execute query
        $stmt->execute();
        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // set values to object properties
        $this->description = $row['description'];
        $this->startDate = $row['startDate'];
        $this->endDate = $row['endDate'];
        $this->agentFirstName = $row['agentFirstName'];
        $this->agentLastName = $row['agentLastName'];
        $this->sectionName = $row['sectionName'];
    }

        // update the agent
    function update()
    {
        // update query
        $query = "UPDATE
                    " . $this->table_name . "
                SET
                    firstName = :firstName,
                    lastName = :lastName,
                    idSection = :idSection,
                    isActive = :isActive
                WHERE
                    plaque = :plaque";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->plaque=htmlspecialchars(strip_tags($this->plaque));
        $this->firstName=htmlspecialchars(strip_tags($this->firstName));
        $this->lastName=htmlspecialchars(strip_tags($this->lastName));
        $this->idSection=htmlspecialchars(strip_tags($this->idSection));
        $this->isActive=htmlspecialchars(strip_tags($this->isActive));

        // bind values
        $stmt->bindParam(":plaque", $this->plaque);
        $stmt->bindParam(":firstName", $this->firstName);
        $stmt->bindParam(":lastName", $this->lastName);
        $stmt->bindParam(":idSection", $this->idSection);
        $stmt->bindParam(":isActive", $this->isActive);

        // execute the query
        if($stmt->execute())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    // delete the agent
    function delete()
    {
        // delete query
        $query = "DELETE FROM " . $this->table_name . " WHERE plaque = ?";
        // prepare query
        $stmt = $this->conn->prepare($query);
        // sanitize
        $this->plaque=htmlspecialchars(strip_tags($this->plaque));
        // bind id of record to delete
        $stmt->bindParam(1, $this->plaque);
        // execute query
        if ($stmt->execute())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    // delete selected agents
    public function deleteSelected($ids)
    {
        $in_ids = str_repeat('?,', count($ids) - 1) . '?';
        // query to delete multiple records
        $query = "DELETE FROM " . $this->table_name . " WHERE plaque IN ({$in_ids})";
        $stmt = $this->conn->prepare($query);
        $counter = 0;
        while($counter < count($ids))
        {
            $stmt->bindParam($counter + 1 , $ids[$counter]);
            $counter++;
        }

        if ($stmt->execute())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
?>
