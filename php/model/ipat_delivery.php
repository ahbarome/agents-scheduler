<?php
    /*
    * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    /**
     * Class IpatTransaction model for ipats transaction
     */
    class IpatTransaction
    {
        public $ipatNumber;
        public $deliveryDate;
        public $deliveredBy;
        public $deliveredTo;
        public $partialReceptionDate;
        public $receptionDate;
        public $quantity;
        public $idSeverity;
        public $idClassification;
        public $accidentDate;
        public $address;
        public $vehiclePlaque;
        public $idTransactionState;
        public $partiallyReceivedBy;
        public $partiallyReceivedTo;
        public $receivedBy;
        public $receivedTo;
        public $criminalNews;
        public $observation;
        public $expired;
        public $daysOfDelayDelivery;
        public $daysOfDelayReception;
        public $diligenceDate;

        public $agentPartiallyReceivedBy;
        public $agentPartiallyReceivedTo;
        public $agentReceivedBy;
        public $agentReceivedTo;
        public $agentDeliveredBy;
        public $agentDeliveredTo;
        public $severity;
        public $classification;
        public $transactionState;
        public $totalNovelties;
    }
?>