<?php

    /*
    * Copyright (c) 2018 by Secretaria de Transito.  All Rights Reserved.
    * This software is the confidential and proprietary information of the
    * Secretaria de Transito. ("Confidential Information").
    * You may not disclose such Confidential Information, and may only
    * use such Confidential Information in accordance with the terms of
    * the license agreement you entered into with the Secretaria de Transito.
    */

    include_once('../../session/session_validator.php');
    include_once('../../session/session_manager.php');

    final class IpatTransactionCache
    {
        private static $sessionValidator;
        private static $sessionManager;

        private static $instance = null;
        const TRANSACTIONS_CACHE_KEY = 'TRANSACTIONS_CACHE_KEY';

        private function __construct()
        {
            self::$sessionValidator = new SessionValidator();
            self::$sessionManager = new SessionManager();
        }

        /**
        * Create instance of object if it is not exist or return existed.
        *
        * @return Singleton instance.
        */
        public static function getInstance()
        {
            if (is_null(self::$instance))
            {
                self::$instance = new self();
            }
            return self::$instance;
        }

        public function setTransactionsCache($transactions)
        {
            if (self::$sessionValidator->existSession())
            {
                self::$sessionManager->addSessionAttribute(
                    IpatTransactionCache::TRANSACTIONS_CACHE_KEY,
                    $transactions);
            }
        }

        public function getTransactionsCache()
        {
            $cache = array();
            if (self::$sessionValidator->existSession())
            {
                $cache =
                    self::$sessionManager->getSessionAttributeValue(
                        IpatTransactionCache::TRANSACTIONS_CACHE_KEY);
            }
            return $cache;
        }
    }