<?php
    include_once("php/session/session_validator.php");

    $validator = new SessionValidator();
    if ($validator->existSession())
    {
        header("location:index.php");
    }
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sistema de Transito</title>
    <!-- include material design CSS -->
    <link rel="stylesheet" href="libs/css/materialize-v0.97.3/css/materialize.min.css" />
    <!-- include material design icons -->
    <link rel="stylesheet" href="libs/css/materialize-icons/material-icons.css"/>
    <link rel="stylesheet" href="libs/css/layouts/page-center.css"/>
    <!-- custom CSS -->
    <link rel="stylesheet" href="libs/css/site.css" />
</head>
<body style="background-color: #30A499" ng-app="transitApp" ng-controller="loginController">
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <div id="login-page" class="row">
        <div class="col s12">
            <div
                class="z-depth-5 grey lighten-4 row"
                style="display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE;">
                <form
                    action="php/action/session/manage_session.php" 
                    class="login-form"
                    method="post"
                    name="loginForm"
                    novalidate>
                    <div class="row">
                        <div class="input-field col s12 center">
                            <p class="center login-form-text">SISTEMA DE TRANSITO</p>
                        </div>
                    </div>
                    <div class="row margin">
                        <div class="input-field col s12">
                            <i class="mdi-social-person-outline prefix"></i>
                            <input
                                class="validate"
                                id="username"
                                name="username"
                                ng-model="login.username"
                                ng-required="true"
                                type="text"/>
                            <label for="username">Nombre de usuario</label>
                            <span
                                class="text-danger"
                                ng-if="loginForm.username.$invalid && !loginForm.username.$pristine">
                                El 'nombre de usuario' es requerido.
                            </span>
                        </div>
                    </div>
                    <div class="row margin">
                        <div class="input-field col s12">
                            <i class="mdi-action-lock-outline prefix"></i>
                            <input
                                class="validate"
                                id="password"
                                name="password"
                                ng-model="login.password"
                                ng-required="true"
                                type="password"/>
                            <label for="password">Contraseña</label>
                            <span
                                class="text-danger"
                                ng-if="loginForm.password.$invalid && !loginForm.password.$pristine">
                                La 'contraseña' es requerida.
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <button
                                class="btn waves-effect waves-light col s12"
                                id="btn-login"
                                name="login"
                                ng-disabled="!loginForm.$valid"
                                ng-click="login()"
                                type="submit">Login
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="libs/js/jquery.js"></script>
    <script src="libs/css/materialize-v0.97.3/js/materialize.min.js"></script>
    <script src="libs/js/angular.min.js"></script>
    <script src="libs/js/dirPagination.js"></script>
    <script src="libs/js/initApp.js"></script>
    <script src="libs/js/common/siteHelper.js"></script>
    <script src="libs/js/modules/login/controllers/loginController.js"></script>

    <script>
    $(document).ready(function(){

        SiteHelper.displayLoader();

    });
    </script>

</body>
</html>
