<header id="header" class="page-topbar">
    <div class="navbar-fixed">
        <nav class="navbar-color">
            <div class="nav-wrapper" style="background-color: #30A499">
                <ul class="left">
                  <li><a href="#" class="brand-logo">{{ SYSTEM.APP_TITLE }}</a></li>
                </ul>
                <ul class="right hide-on-med-and-down">
                    <li>
                        <a ng-click="closeSession()"
                            class="waves-effect waves-block waves-light">
                            <i class="material-icons">close</i>
                        </a>
                    </li>
                </ul>
                <ul class="right hide-on-med-and-down">
                    <li><a ng-click="showUserChangePassword()" class="waves-effect waves-block waves-light">
                        <?php
                            include_once('../../php/session/session_validator.php');

                            $validator = new SessionValidator();
                            if ($validator->existSession())
                            {
                                echo 'BIENVENIDO: ' . strtoupper($_SESSION['USERNAME']);
                            }
                         ?>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>