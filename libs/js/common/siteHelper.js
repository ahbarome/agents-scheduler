/*
 * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
 * This software is the confidential and proprietary information of the
 * Secretaria de Transito. ("Confidential Information").
 * You may not disclose such Confidential Information, and may only
 * use such Confidential Information in accordance with the terms of
 * the license agreement you entered into with the Secretaria de Transito.
 */

var SiteHelper = {

    hideElement: function(elementId, hide, callback) {
        var element = $(elementId);
        if (element !== undefined && hide) {
            element.addClass('hide-element');
        }
        else if (element !== undefined) {
            element.removeClass('hide-element');
        }

        $.performCallback(callback);
    },

    displayLoader: function(timeout) {
        var defaultTimeOut = 200;
        if (timeout !== undefined) {
            defaultTimeOut = timeout;
        }

        setTimeout(function() {
            $('body').addClass('loaded');
        }, timeout);
    }

};