/*
 * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
 * This software is the confidential and proprietary information of the
 * Secretaria de Transito. ("Confidential Information").
 * You may not disclose such Confidential Information, and may only
 * use such Confidential Information in accordance with the terms of
 * the license agreement you entered into with the Secretaria de Transito.
 */

var ArrayHelper = {

    SEPARATOR: ',',

    getObject: function(collection, propertyNames, propertyValues) {

        if (Object.keys(propertyNames).length != Object.keys(propertyValues).length)
        {
            return null;
        }

        var searchedElement = {};
        for (var i = 0; i < Object.keys(collection).length; i++) {
            var element = collection[i];
            var matches = 0;
            for (var j = 0; j < Object.keys(propertyNames).length; j++) {
                var name = propertyNames[j];
                var value = propertyValues[j];
                if (element[name] === value)
                {
                    matches = matches + 1;
                }
            }

            if (matches === Object.keys(propertyNames).length) {
                searchedElement = element;
                break;
            }
        }
        return searchedElement;
    },

    getValuesAsString: function(collection, propertyName)
    {
        if (!ArrayHelper.hasData(collection)){
            return null;
        }

        var values = [];
        for (var i = 0; i < Object.keys(collection).length; i++) {
            var element = collection[i];
            if (typeof(element[propertyName]) !== 'undefined') {
                values.push(element[propertyName]);
            }
        }
        return values.join(ArrayHelper.SEPARATOR);
    },

    hasData: function(collection) {
        return collection != undefined && Object.keys(collection).length > 0;
    },

    count: function(collection) {
        let total = 0;
        if (ArrayHelper.hasData(collection)) {
            total = Object.keys(collection).length;
        }
        return total;
    }

};