/*
 * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
 * This software is the confidential and proprietary information of the
 * Secretaria de Transito. ("Confidential Information").
 * You may not disclose such Confidential Information, and may only
 * use such Confidential Information in accordance with the terms of
 * the license agreement you entered into with the Secretaria de Transito.
 */

var ModalHelper = {

    openModal: function(modalId, callback) {
        $(modalId).openModal();
        $.performCallback(callback);
    },

    closeModal: function(modalId, callback) {
        $(modalId).closeModal();
        $.performCallback(callback);
    }

};