/*
 * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
 * This software is the confidential and proprietary information of the
 * Secretaria de Transito. ("Confidential Information").
 * You may not disclose such Confidential Information, and may only
 * use such Confidential Information in accordance with the terms of
 * the license agreement you entered into with the Secretaria de Transito.
 */

var FormHelper =  {

    hideElementClass: 'ng-hide',

    doValidElements: function() {
        $('.text-danger').each(function(index, field) {
            $(field).addClass(this.hideElementClass);
        });
    },

    clearForm: function(id, callback) {
        $(id)[0].reset();
        this.doValidElements();
        $.performCallback(callback);
    }

};