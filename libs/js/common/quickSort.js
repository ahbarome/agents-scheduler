/*
 * Copyright (c) 2018 by Secretaria de Transito.  All Rights Reserved.
 * This software is the confidential and proprietary information of the
 * Secretaria de Transito. ("Confidential Information").
 * You may not disclose such Confidential Information, and may only
 * use such Confidential Information in accordance with the terms of
 * the license agreement you entered into with the Secretaria de Transito.
 */

/**
 * Sort an array of data using the QuickSort algorithm.
 * @type {{sort: (function(*=, *=, *=): *), partition: (function(*=, *, *, *=): *), swap: QuickSort.swap}}
 */
let QuickSort = {

    types: { integerType: 0 },

    /**
     * Sort data in an array of objects. Code used from
     * https://khan4019.github.io/front-end-Interview-Questions/sort.html#quickSort
     *
     * @param array to sort.
     * @param left first index of the array.
     * @param right last index of the array.
     * @returns {*} Array sorted.
     */
    sort: function(array, propertyName, left, right){
        try {
            let pivot, partitionIndex;

            if (left < right) {
                pivot = right;
                partitionIndex = QuickSort.partition(array, propertyName, pivot, left, right);

                QuickSort.sort(array, propertyName, left, partitionIndex - 1);
                QuickSort.sort(array, propertyName, partitionIndex + 1, right);
            }
        }
        catch (e) {
            console.log('Error trying to sort array');
        }
        return array;
    },

    /**
     * Get the index of each partition and call the swap event that the
     * left value is less than the right value;
     * @param array to sort.
     * @param propertyName which is going to be sorted.
     * @param pivot element.
     * @param left element.
     * @param right element.
     * @returns {*}
     */
    partition: function(array, propertyName, pivot, left, right) {
        let pivotValue = parseInt(array[pivot][propertyName]),
        partitionIndex = left;

        for (let i = left; i < right; i++) {
            if (parseInt(array[i][propertyName]) < pivotValue) {
                QuickSort.swap(array, i, partitionIndex);
                partitionIndex++;
            }
        }

        QuickSort.swap(array, right, partitionIndex);
        return partitionIndex;
    },

    /**
     * Swap elements in the array.
     *
     * @param array that contains the elements to swap.
     * @param i base index to swap.
     * @param j index destiny.
     */
    swap: function(array, i, j){
        let temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

};