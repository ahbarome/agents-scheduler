/*
 * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
 * This software is the confidential and proprietary information of the
 * Secretaria de Transito. ("Confidential Information").
 * You may not disclose such Confidential Information, and may only
 * use such Confidential Information in accordance with the terms of
 * the license agreement you entered into with the Secretaria de Transito.
 */

/**
 * Controller for transactions of transit tickets.
 */
app.controller('transitTicketTransactionController', function($scope, $http) {

    $scope.getAllAgentsWithTransitTicketTransactions = function() {
        $http.get("php/action/transit_ticket/read_active_agents_with_transit_ticket_transactions.php")
            .success(function(response) {
                if (ArrayHelper.hasData(response.records)) {
                    $scope.agentsActiveWithTransitTicketTransactions = response.records;
                }
            });
    };

    $scope.getAllTransitTicketTransactions = function() {
        $http.get("php/action/transit_ticket/read_transit_ticket_transactions.php")
            .success(function(response) {
                if (ArrayHelper.hasData(response.records)) {
                    $scope.transitTicketTransactions = response.records;
                }
        });
    };

    $scope.getAllTransitTicketTransactionsByCriteria = function(filter) {
        var selectedFilter =
            $scope.getSelectedFilter($scope.trasitTicketTransactionFilters, filter.filterId);
        selectedFilter.valueFilter = filter.valueFilter;
        $http.post(
            "php/action/transit_ticket/filter_transit_ticket_transactions.php",
            { 'filter': selectedFilter })
            .success(function(response) {
                if (ArrayHelper.hasData(response.records)) {
                    $scope.transitTicketTransactions = response.records;
                }
            });
    };

    $scope.createTransitTicketTransaction = function(isValid) {
        if ($scope.transitTicketTalonary != undefined && isValid) {
            $http.post('php/action/transit_ticket/create_transit_ticket_transaction.php', {
                'transitTicketTransaction' : $scope.transitTicketTalonary
                }
            ).success(function (data, status, headers, config) {
                Materialize.toast(data, $scope.defaultToastTime);
                $scope.clearTransitTicketTalonary();
                $scope.reloadControlsData();
                ModalHelper.closeModal('#modal-transit-ticket-talonary-form');
            });
        }
    };

    $scope.receiveTransitTicketTransaction = function(isValid) {
        if ($scope.transitTicketTransaction != undefined && isValid) {
            $http.post('php/action/transit_ticket/receive_transit_ticket_transaction.php', {
                'transitTicketTransaction' : $scope.transitTicketTransaction
                }
            ).success(function (data, status, headers, config) {
                Materialize.toast(data, $scope.defaultToastTime);
                $scope.clearTransitTicketTransaction();
                $scope.reloadControlsData();
                ModalHelper.closeModal('#modal-transit-ticket-transaction-form');
            });
        }
    };

    $scope.readOneTransitTicketTransaction = function(transaction) {
        $scope.transitTicketTransaction = transaction;
        ModalHelper.openModal('#modal-transit-ticket-transaction-form');
    };

    $scope.clearTransitTicketTalonary = function() {
        if ($scope.transitTicketTalonary != undefined) {
            $scope.transitTicketTalonary = {};
        }
        FormHelper.clearForm("#id-transit-ticket-talonary-form");
    };

    $scope.clearTransitTicketTransaction = function() {
        if ($scope.transitTicketTransaction != undefined) {
            $scope.transitTicketTransaction = {};
        }
        FormHelper.clearForm("#id-transit-ticket-transaction-form");
    };

    $scope.openNoveltyModalForm = function(talonaryNumber, ticketNumber) {
        $scope.selectedTalonaryNumber = talonaryNumber;
        $scope.selectedTicketNumber = ticketNumber;
        $('#modal-transit-ticket-transaction-novelty-form').openModal();
    };

    $scope.readTransitTicketTransaction = function(talonaryNumber, ticketNumber) {
        var transaction =
            ArrayHelper.getObject(
                $scope.transitTicketTransactions,
                ['talonaryNumber', 'ticketNumber'],
                [talonaryNumber, ticketNumber]);
        if (transaction != undefined) {
            $scope.transitTicketTransaction = transaction;
            $scope.transitTicketTransaction.diligenceDate =
                DateHelper.getAsDateTime(transaction.diligenceDate);
            $scope.transitTicketTransaction.immobilized =
                transaction.immobilized == "1" ? true : false;
            ModalHelper.openModal('#modal-transit-ticket-transaction-form');
        }
    };

    $scope.addTransitTicketTransactionNovelty = function(isValid) {
        if ($scope.transitTicketTransactionNovelty != undefined && isValid) {
            $scope.transitTicketTransactionNovelty.talonaryNumber = $scope.selectedTalonaryNumber;
            $scope.transitTicketTransactionNovelty.ticketNumber = $scope.selectedTicketNumber;
            $http.post('php/action/transit_ticket/add_transit_ticket_transaction_novelty.php', {
                'transitTicketTransactionNovelty' : $scope.transitTicketTransactionNovelty
                }
            ).success(function (data, status, headers, config) {
                Materialize.toast(data, $scope.defaultToastTime);
                $scope.clearTransitTicketTransactionNovelty();
                $scope.getAllTransitTicketTransactions();
                $scope.loadTransitTicketStatistics();
                ModalHelper.closeModal('#modal-transit-ticket-transaction-novelty-form');
            });
        }
    };

    $scope.getCountDeliveredTransactions = function(plaqueAgent) {
        $scope.totalDeliveredTransactions = 0;
        $http.post(
            "php/action/transit_ticket/count_delivered_transit_ticket_transactions.php",
            {'assignedTo' : plaqueAgent})
            .success(function(response) {
            if (response.records !== undefined) {
                $scope.totalDeliveredTransactions = response.records;
            }
        });
    };

    $scope.getCountReceivedTransactions = function(plaqueAgent) {
        $scope.totalReceivedTransactions = 0;
        $http.post(
            "php/action/transit_ticket/count_received_transit_ticket_transactions.php",
            {'assignedTo' : plaqueAgent})
            .success(function(response) {
                if (response.records !== undefined) {
                    $scope.totalReceivedTransactions = response.records;
                }
            });
    };

    $scope.getCountNovelties = function(plaqueAgent) {
        $scope.totalNovelties = 0;
        $http.post(
            "php/action/transit_ticket/count_novelties_transit_ticket_transactions.php",
            {'assignedTo' : plaqueAgent})
            .success(function(response) {
                if (response.records !== undefined) {
                    $scope.totalNovelties = response.records;
                }
            });
    };

    $scope.getTransitTicketTransactionsFiltersCriteria = function() {
        $scope.trasitTicketTransactionFilters = {};
        $http.get("php/action/transit_ticket/get_transit_ticket_transaction_filters_criteria.php")
            .success(function(response) {
                if (ArrayHelper.hasData(response.records)) {
                    $scope.trasitTicketTransactionFilters = response.records;
                }
            });
    };

    $scope.filterTransitTicketTransactionStatistics = function(plaqueAgent) {
        $scope.loadTransitTicketStatistics(plaqueAgent);
    };

    $scope.loadTransitTicketStatistics = function(plaqueAgent) {
        $scope.getCountDeliveredTransactions(plaqueAgent);
        $scope.getCountReceivedTransactions(plaqueAgent);
        $scope.getCountNovelties(plaqueAgent);
    };

    $scope.openGeneratePeaceAndSaveModal = function() {
        ModalHelper.openModal("#modal-generate-peace-and-save");
    };

    $scope.generatePeaceAndSave = function(talonaryNumber){
        window.location =
            'php/action/transit_ticket/export_pdf_peace_and_save.php?talonaryNumber='
                + talonaryNumber;
        ModalHelper.closeModal("#modal-generate-peace-and-save");
    };

    $scope.reloadControlsData = function() {
        $scope.loadTransitTicketStatistics();
        $scope.getAllTransitTicketTransactions();
        $scope.getAllAgentsWithTransitTicketTransactions();
    }

    $scope.clearTransitTicketTransactionNovelty = function() {
        if (ArrayHelper.hasData($scope.transitTicketTransactionNovelty)) {
            $scope.transitTicketTransactionNovelty = {};
        }
        FormHelper.clearForm("#id-transit-ticket-transaction-novelty-form");
    };

    $scope.getSelectedFilter = function(filters, id) {
        var selectedFilter = {};
        for (var i = filters.length - 1; i >= 0; i--) {
            var filter = filters[i];
            if (filter.filterId === parseInt(id)) {
                selectedFilter = filter;
                break;
            }
        }
        return selectedFilter;
    };

});