/*
 * Copyright (c) 2018 by Secretaria de Transito.  All Rights Reserved.
 * This software is the confidential and proprietary information of the
 * Secretaria de Transito. ("Confidential Information").
 * You may not disclose such Confidential Information, and may only
 * use such Confidential Information in accordance with the terms of
 * the license agreement you entered into with the Secretaria de Transito.
 */

/**
 * Controller for the statistics of ipat transactions.
 */
app.controller('ipatTransactionStatisticController', function($scope, $http) {

    $scope.getAllIpatDeliveryStatistics = function() {
        $http.get("php/action/ipat_delivery_statistic/read_all_statistics.php")
            .success(function(response) {
                if (ArrayHelper.hasData(response.records)) {
                    $scope.ipatDeliveryStatistics = response.records;
                }
            });
    };

    $scope.filterIpatDeliveriesSeveritiesStatistics = function(severitiesFilter) {
        if (ArrayHelper.hasData(severitiesFilter)) {
            $http.post(
                "php/action/ipat_delivery_statistic/filter_ipat_deliveries_severities_statistics.php",
                { 'filter': severitiesFilter })
                .success(function(response) {
                    if (ArrayHelper.hasData(response.records)) {
                        $scope.ipatDeliveryStatistics = response.records;
                    }
                    else if(response.records !== undefined
                        && Object.keys(response.records).length === 0) {
                        alert("No se encontró información con su criterio de búsqueda.");
                        $scope.getAllIpatDeliveryStatistics();
                    }
            });
        }
    };
});