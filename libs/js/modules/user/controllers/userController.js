/*
 * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
 * This software is the confidential and proprietary information of the
 * Secretaria de Transito. ("Confidential Information").
 * You may not disclose such Confidential Information, and may only
 * use such Confidential Information in accordance with the terms of
 * the license agreement you entered into with the Secretaria de Transito.
 */

/**
 * Controller for the users
 */
app.controller('userController', function($scope, $http) {

    $scope.hideUserModalControls = function() {
        $('#button-create-new-user').hide();
        $('#button-update-user').hide();
    };

    $scope.getAllUsers = function() {
        $http.get("php/action/user/read_users.php")
            .success(function(response) {
                if (ArrayHelper.hasData(response.records)) {
                    $scope.users = response.records;
                }
            });
    };

    $scope.getAllRoles = function() {
        $http.get("php/action/role/read_roles.php")
            .success(function(response) {
                if (ArrayHelper.hasData(response.records)) {
                    $scope.roles = response.records;
                }
            });
    };

    $scope.readOneUser = function(id) {
        FormHelper.clearForm("#id-user-form");
        $http.get("php/action/user/read_one_user.php?id=" + id)
            .success(function(response) {
                if (ArrayHelper.hasData(response.records)) {
                    $scope.modalCurrentUser = response.records;
                }
                $scope.hideUserModalControls();

                $('#button-update-user').show();
                $('#modal-user-form-title').text("Editar Usuario");
                ModalHelper.openModal("#modal-user-form");
            });
    };

    $scope.showCreateUserForm = function() {
        $scope.clearUserForm();
        $scope.hideUserModalControls();
        $('#button-create-new-user').show();
        $('#modal-user-form-title').text("Crear Nuevo Usuario");
    },

    $scope.createUser = function(isValid) {
        if ($scope.modalCurrentUser != undefined && isValid) {
            $http.post('php/action/user/create_user.php', {
                'user' : $scope.modalCurrentUser
                }
            ).success(function (data, status, headers, config) {
                Materialize.toast(data, $scope.defaultToastTime);
                $scope.clearUserForm();
                $scope.reloadUserControlsData();
                ModalHelper.closeModal("#modal-user-form");
            });
        }
    },

    $scope.updateUser = function(isValid) {
        if ($scope.modalCurrentUser != undefined && isValid) {
            $http.post('php/action/user/update_user.php', {
                'user' : $scope.modalCurrentUser
                }
            ).success(function (data, status, headers, config) {
                Materialize.toast(data, $scope.defaultToastTime);
                $scope.clearUserForm();
                $scope.reloadUserControlsData();
                ModalHelper.closeModal("#modal-user-form");
            });
        }
    },

    $scope.deleteUser = function(id) {
        console.log(id);
    };

    $scope.clearUserForm = function() {
        if ($scope.modalCurrentUser != undefined) {
            $scope.modalCurrentUser = {};
        }
        FormHelper.clearForm("#id-user-form");
    };

    $scope.reloadUserControlsData = function() {
        $scope.getAllUsers();
    }

});