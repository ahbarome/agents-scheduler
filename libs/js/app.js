/*
 * Copyright (c) 2017 by Secretaria de Transito.  All Rights Reserved.
 * This software is the confidential and proprietary information of the
 * Secretaria de Transito. ("Confidential Information").
 * You may not disclose such Confidential Information, and may only
 * use such Confidential Information in accordance with the terms of
 * the license agreement you entered into with the Secretaria de Transito.
 */

/**
 * Controller of the application
 */
app.controller('appController', function($scope, $http) {

    $scope.SYSTEM =
        {
            APP_TITLE : 'SISTEMA DE TRANSITO',
            SCREEN_SECTIONS_TITLE : 'Secciones',
            SCREEN_AGENTS_TITLE : 'Agentes',
            DEFAULT_MESSAGE_ON_ERROR_RETREIVING_DATA :
                'No fué posible obtener la información solicitada',
            SCREEN_IPATS_ADMINISTRATION_TITLE : 'Administración de IPATs',
            SCREEN_INACTIVITIES_TITLE : 'Inactividades',
            SCREEN_SCHEDULE_TITLE : 'Programación',
            SCREEN_REPORTS_TITLE : 'Reportes',
            SCREEN_TRANSIT_TICKET_TITLE : 'Administración de Comparendos',
            SCREEN_IPAT_DELIVERY_STATISTIC_TITLE : 'Estadísticas de IPATs',
            SCREEN_SUPPORT_TITLE : 'Soporte',
            ADMINISTRATOR_ID : 1,
            IPAT_ADMINISTRATOR_ID : 2,
            TRANSIT_TICKET_ADMINISTRATOR_ID : 3,
            IPAT_VIEWER_ID: 4,
            TRANSACTION_STATE_DELIVERED : 1,
            TRANSACTION_STATE_DELIVERED_DESCRIPTION : 'Entregado(s)',
            TRANSACTION_STATE_RECEIVED_WITH_PENDING : 2,
            TRANSACTION_STATE_RECEIVED_WITH_PENDING_DESCRIPTION : 'Recibido(s) Parcialmente',
            TRANSACTION_STATE_RECEIVED : 3,
            TRANSACTION_STATE_RECEIVED_DESCRIPTION : 'Recibido(s)',
            TRANSACTION_STATE_REMAINING : 4,
            TRANSACTION_STATE_REMAINING_DESCRIPTION : 'Restante(s) Rango Anterior',
            TRANSACTION_STATE_REMAINING_CURRENT_RANGE : 5,
            TRANSACTION_STATE_REMAINING_CURRENT_RANGE_DESCRIPTION : 'Restante(s) Rango Actual',
            STATED_TIME : "00:00",
            ZERO_RECORDS_FOUND_VALUE : 0,
            DEFAULT_IPATS_QUANTITY : 1
        };

    $scope.selection = [];
    $scope.defaultToastTime = 20000;

    // toggle selection for a give agent by plaque
    $scope.toggleSelection = function toggleSelection(plaque) {
        var index = $scope.selection.indexOf(plaque);
        // is currently selected
        if (index > -1) {
            $scope.selection.splice(index, 1);
        }
        // is newly selected
        else {
            $scope.selection.push(plaque);
        }
    };

    $scope.pageChangeHandler = function(newPage) {
    };

    /**
     * Hide all the screen views of the application
     */
    $scope.hideAllViews = function() {
        $scope.isSectionShown = false;
        $scope.isScheduleShown = false;
        $scope.isInactivityShown = false;
        $scope.isAgentShown = false;
        $scope.isIpatDeliveryShown = false;
        $scope.isUserChangePasswordShown = false;
        $scope.isReportIpatReceivedWithPendingShown = false;
        $scope.isUsersShown = false;
        $scope.isTransitTicketsShown = false;
        $scope.isIpatDeliveryStatisticShown = false;
        $scope.isSupportShown = false;
    };

    $scope.showAgents = function() {
        $scope.hideAllViews();
        $scope.isAgentShown = true;
        $scope.getAllAgents();
    };

    /**
     * Display the section screen
     */
    $scope.showSections = function() {
        $scope.hideAllViews();
        $scope.isSectionShown = true;
        $scope.getAllSections();
    };

    /**
     * Display the inactivities screen
     */
    $scope.showInactivities = function() {
        $scope.hideAllViews();
        $scope.isInactivityShown = true;
        $scope.getAllInactivities();
    };

    // show schedule
    $scope.showSchedules = function() {
        $scope.hideAllViews();
        $scope.isScheduleShown = true;
        $scope.getAllSchedules();
    };

    // show ipat deliveries
    $scope.showIpatDeliveries = function() {
        $scope.hideAllViews();
        $scope.isIpatDeliveryShown = true;
    };

    $scope.showIpatDeliveryStatistic = function() {
        $scope.hideAllViews();
        $scope.isIpatDeliveryStatisticShown = true;
    };

    $scope.showSupport = function() {
        $scope.hideAllViews();
        $scope.isSupportShown = true;
    };

    /**
     * Display the report screen to get the list of agents with ipats in receveid with pending state
     * and the corresponding ipats and quantity
     */
    $scope.showReportIpatReceivedWithPending = function() {
        $scope.hideAllViews();
        $scope.isReportIpatReceivedWithPendingShown = true;
        $scope.readIpatsReceivedWithPendingState();
    };

    $scope.showTransitTickets = function() {
        $scope.hideAllViews();
        $scope.isTransitTicketsShown = true;
    };

    $scope.showUsers = function() {
        $scope.hideAllViews();
        $scope.isUsersShown = true;
    };

    $scope.showUserChangePassword = function() {
        $scope.hideAllViews();
        $scope.isUserChangePasswordShown= true;
    };

    // for table sorting
    $scope.sort = function(keyname) {
        $scope.sortKey = keyname;   // set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; // if true make it false and vice versa
    }

    $scope.getUser = function(callback) {
        $http.post("php/action/user/read_current_user.php").success(function(response) {
            if (response.records != undefined) {
                $scope.user = response.records;
            }
            $.performCallback(callback);
        });
    };

    $scope.isAdministrator = function() {
        var user = $scope.user;
        return user != undefined && user.idRole == $scope.SYSTEM.ADMINISTRATOR_ID;
    };

    $scope.isIpatAdministrator = function() {
        var user = $scope.user;
        var isAdministrador =
            user != undefined && user.idRole == $scope.SYSTEM.IPAT_ADMINISTRATOR_ID;
        return isAdministrador;
    };

    $scope.isIpatViewer = function() {
        var user = $scope.user;
        var isViewer =
            user != undefined && user.idRole == $scope.SYSTEM.IPAT_VIEWER_ID;
        return isViewer;
    };

    $scope.isTransitTicketAdministrator = function() {
        var user = $scope.user;
        var isAdministrador =
            user != undefined && user.idRole == $scope.SYSTEM.TRANSIT_TICKET_ADMINISTRATOR_ID;
        return isAdministrador;
    };

    $scope.defineInitialPage = function() {
        if($scope.isAdministrator() || $scope.isIpatAdministrator() || $scope.isIpatViewer()) {
            $scope.isIpatDeliveryShown = true;
        }
        else if ($scope.isTransitTicketAdministrator()) {
            $scope.isTransitTicketsShown = true;
        }
    };

    $scope.getUser(function() {
        $scope.defineInitialPage();
    });

    // read agents
    $scope.getAllAgents = function() {
        $http.get("php/action/agent/read_agents.php").success(function(response) {
            if (response.records != undefined && response.records[0] != undefined) {
                let agents = response.records;
                if (ArrayHelper.count($scope.agentsActive) != ArrayHelper.count(agents)) {
                    agents = QuickSort.sort(agents, 'plaque', 0, Object.keys(agents).length - 1);
                    $scope.agents = agents;
                }
            }
            else{
                Materialize.toast("No se encontraron agentes.", $scope.defaultToastTime);
            }
        });
    };

    $scope.getAllAgentsActive = function() {
        $http.get("php/action/agent/read_active_agents.php")
            .success(function(response) {
                if (response.records != undefined) {
                    let agents = response.records;
                    let hash = response.records.hash;
                    if (ArrayHelper.count($scope.agentsActive) != ArrayHelper.count(agents)
                        || hash != $scope.agentsActiveHash) {
                        agents = QuickSort.sort(agents, 'plaque', 0, Object.keys(agents).length - 1);
                        $scope.agentsActive = agents;
                        $scope.agentsActiveHash = hash;
                    }
                }
                else{
                    Materialize.toast("No se encontraron agentes activos.", $scope.defaultToastTime);
                }
        });
    };

    // create new agent
    $scope.createAgent = function() {
        // fields in key-value pairs
        $http.post('php/action/agent/create_agent.php', {
                'plaque' : $scope.plaque,
                'firstName' : $scope.firstName,
                'lastName' : $scope.lastName,
                'idSection' : $scope.idSection,
                'isActive' : $scope.isActive
            }
        ).success(function (data, status, headers, config) {
            // tell the user new agent was created
            Materialize.toast(data, $scope.defaultToastTime);
            // close modal
            $('#modal-agent-form').closeModal();
            // clear modal content
            $scope.clearForm();
            // refresh the list
            $scope.getAllAgents();
            $scope.getAllAgentsActive();
        });
    };

    // retrieve record to fill out the form
    $scope.readOne = function(plaque) {
        // change modal title
        $('#modal-agent-title').text("Editar Agente");
        // show udpate agent button
        $('#btn-update-agent').show();
        // show create agent button
        $('#btn-create-agent').hide();

        // post id of agent to be edited
        $http.post('php/action/agent/read_one_agent.php', {
            'plaque' : plaque
        })
        .success(function(response) {
            if (response.records != undefined && response.records.plaque != undefined) {
                $scope.plaque = response.records.plaque;
                $scope.firstName = response.records.firstName;
                $scope.lastName = response.records.lastName;
                $scope.idSection = response.records.idSection;
                $scope.isActive = response.records.isActive == 1 ? true : false;
            }
            $('#modal-agent-form').openModal();
        })
        .error(function(data, status, headers, config) {
            Materialize.toast(
                $scope.SYSTEM.DEFAULT_MESSAGE_ON_ERROR_RETREIVING_DATA, $scope.defaultToastTime);
        });
    };

    // update agent record / save changes
    $scope.updateAgent = function() {
        $http.post('php/action/agent/update_agent.php', {
            'plaque' : $scope.plaque,
            'firstName' : $scope.firstName,
            'lastName' : $scope.lastName,
            'idSection' : $scope.idSection,
            'isActive' : $scope.isActive
        })
        .success(function (data, status, headers, config) {
            // tell the user agent record was updated
            Materialize.toast(data, $scope.defaultToastTime);
            // close modal
            $('#modal-agent-form').closeModal();
            // clear modal content
            $scope.clearForm();
            // refresh the agent list
            $scope.getAllAgents();
            $scope.getAllAgentsActive();
        });
    };

    // delete agent
    $scope.deleteAgent = function(plaque) {
        // ask the user if he is sure to delete the record
        if (confirm("Desea eliminar el registro?")) {
            // post the id of agent to be deleted
            $http.post('php/action/agent/delete_agent.php', {
                'plaque' : plaque
            }).success(function (data, status, headers, config) {
                // tell the user agent was deleted
                Materialize.toast(data, $scope.defaultToastTime);
                // refresh the list
                $scope.getAllAgents();
            });
        }
    };

    // delete all checked checkboxes
    $scope.deleteSelectedAgents = function() {
        // verify if at least one record was checked
        var selected_agents_count = $scope.selection.length;

        // if at least one record was checked
        if (selected_agents_count > 0) {

            // show confirmation
            var pop_up_question = "";
            if (selected_agents_count == 1) {
                var pop_up_question = 'Desea eliminar ' + selected_agents_count + ' agente?';
            }

            else if (selected_agents_count > 1) {
                var pop_up_question = 'Desea eliminar ' + selected_agents_count + ' agentes?';
            }

            var answer = confirm(pop_up_question);

            // user clicked 'ok'
            if (answer) {
                // fields in key-value pairs
                $http.post('php/action/agent/delete_selected_agents.php', {
                        'ids' : $scope.selection
                    }
                ).success(function (data, status, headers, config) {
                    // tell the user selected agents were deleted
                    Materialize.toast(data, $scope.defaultToastTime);
                    // refresh the list
                    $scope.getAllAgents();
                });
            }
        }

        // if no records was selected, tell the user to select at least one
        else{
            alert('Por favor seleccione al menos un agente.');
        }
    };

    // display the create form for agents
    $scope.showCreateForm = function() {
        // clear form
        $scope.clearForm();
        $scope.isActive = true;
        // change modal title
        $('#modal-agent-title').text("Nuevo Agente");
        // hide update agent button
        $('#btn-update-agent').hide();
        // show create agent button
        $('#btn-create-agent').show();
    };

    // clear variable / form values
    $scope.clearForm = function() {
        $scope.plaque = "";
        $scope.firstName = "";
        $scope.lastName = "";
        $scope.idSection = -1;
    };

    $scope.clearIpatDeliveryModalForm = function() {
        if ($scope.ipatDelivery == undefined) {
            return;
        }
        $scope.ipatDelivery.ipatNumber = "";
        $scope.ipatDelivery.quantity = "";
        $scope.ipatDelivery.deliveredBy = "";
        $scope.ipatDelivery.deliveredTo = "";
        $scope.ipatDelivery.receivedBy = "";
        $scope.ipatDelivery.receivedTo = "";
        $scope.ipatDelivery.address = "";
        $scope.ipatDelivery.accidentDate = "";
        $scope.ipatDelivery.diligenceDate = "";
        $scope.ipatDelivery.criminalNews = "";
        $scope.ipatDelivery.observation = "";
        $scope.ipatDelivery.idSeverity = "";
        $scope.ipatDelivery.idTransactionState = "";
        $scope.ipatDelivery.vehiclePlaque = "";
        $scope.ipatDelivery.exportMinistryDate = "";
    };

    // ----------------------------------------
    // SECTIONS DATA
    // ----------------------------------------

    // display the create form for sections
    $scope.showCreateSectionForm = function() {
        // clear form
        $scope.clearSectionForm();
        // change modal title
        $('#modal-section-title').text("Nueva Sección");
        // hide update section button
        $('#btn-update-section').hide();
        // show create section button
        $('#btn-create-section').show();
    };

    // clear variable / form values
    $scope.clearSectionForm = function() {
        $scope.section_id = -1;
        $scope.section_name = "";
    };

    // read sections
    $scope.getAllSections = function() {
        $http.get("php/action/section/read_sections.php").success(function(response) {
            if (response.records[0]['id']!=undefined) {
                let sections = response.records;
                let hash = response.hash;
                if (ArrayHelper.count(sections) != ArrayHelper.count($scope.sections)
                    || hash != $scope.sectionsHash) {
                    $scope.sections = response.records;
                    $scope.sectionsHash = hash;
                }
            }
        });
    };

    // create new section
    $scope.createSection = function() {
        // fields in key-value pairs
        $http.post('php/action/section/create_section.php', {
                'id' : $scope.section_id,
                'name' : $scope.section_name
            }
        ).success(function (data, status, headers, config) {
            // tell the user new agent was created
            Materialize.toast(data, $scope.defaultToastTime);
            // close modal
            $('#modal-section-form').closeModal();
            // clear modal content
            $scope.clearSectionForm();
            // refresh the list
            $scope.getAllSections();
        });
    };

    // update section record / save changes
    $scope.updateSection = function() {
        $http.post('php/action/section/update_section.php', {
            'id' : $scope.section_id,
            'name' : $scope.section_name
        })
        .success(function (data, status, headers, config) {
            // tell the user agent record was updated
            Materialize.toast(data, $scope.defaultToastTime);
            // close modal
            $('#modal-section-form').closeModal();
            // clear modal content
            $scope.clearSectionForm();
            // refresh the section list
            $scope.getAllSections();
        });
    };

    // retrieve record to fill out the form
    $scope.readOneSection = function(id) {
        // change modal title
        $('#modal-section-title').text("Editar Sección");
        // show udpate section button
        $('#btn-update-section').show();
        // show create section button
        $('#btn-create-section').hide();

        // post id of agent to be edited
        $http.post('php/action/section/read_one_section.php', {
            'id' : id
        })
        .success(function(data, status, headers, config) {
            // put the values in form
            $scope.section_id = data.id;
            $scope.section_name = data.name;
            // show modal
            $('#modal-section-form').openModal();
        })
        .error(function(data, status, headers, config) {
            Materialize.toast(
                $scope.SYSTEM.DEFAULT_MESSAGE_ON_ERROR_RETREIVING_DATA, $scope.defaultToastTime);
        });
    };

    // delete section
    $scope.deleteSection = function(id) {
        // ask the user if he is sure to delete the record
        if (confirm("Desea eliminar el registro?")) {
            // post the id of section to be deleted
            $http.post('php/action/section/delete_section.php', {
                'id' : id
            }).success(function (data, status, headers, config) {
                // tell the user agent was deleted
                Materialize.toast(data, $scope.defaultToastTime);
                // refresh the list
                $scope.getAllSections();
            });
        }
    };

    // delete all checked checkboxes
    $scope.deleteSelectedSections = function() {
        // verify if at least one record was checked
        var selected_sections_count = $scope.selection.length;

        // if at least one record was checked
        if (selected_sections_count > 0) {

            // show confirmation
            var pop_up_question = "";
            if (selected_sections_count==1) {
                var pop_up_question = 'Desea eliminar ' + selected_sections_count + ' sección?';
            }

            else if (selected_sections_count>1) {
                var pop_up_question = 'Desea eliminar ' + selected_sections_count + ' secciones?';
            }

            var answer = confirm(pop_up_question);

            // user clicked 'ok'
            if (answer) {
                // fields in key-value pairs
                $http.post('php/action/section/delete_selected_sections.php', {
                        'ids' : $scope.selection
                    }
                ).success(function (data, status, headers, config) {
                    // tell the user selected agents were deleted
                    Materialize.toast(data, $scope.defaultToastTime);
                    // refresh the list
                    $scope.getAllSections();
                });
            }
        }
        // if no records was selected, tell the user to select at least one
        else{
            alert('Por favor seleccione al menos una sección.');
        }
    };

    // ----------------------------------------
    // INACTIVITIES DATA
    // ----------------------------------------

    // display the create form for inactivities
    $scope.showCreateInactivityForm = function() {
        // clear form
        $scope.clearInactivityForm();
        // change modal title
        $('#modal-inactivity-title').text("Nueva Inactividad");
        // hide update section button
        $('#btn-update-inactivity').hide();
        // show create section button
        $('#btn-create-inactivity').show();
    };

    // clear variable / form values
    $scope.clearInactivityForm = function() {
        $scope.inactivity_id = -1;
        $scope.inactivity_description = "";
        $scope.inactivity_plaqueAgent = -1;
        $scope.inactivity_startDate = "";
        $scope.inactivity_endDate = "";
    };

    // read sections
    $scope.getAllInactivities = function() {
        $http.get("php/action/inactivity/read_inactivities.php").success(function(response) {
            if (response.records[0]['id']!=undefined) {
                $scope.inactivities = response.records;
            }
        });
    };

    // retrieve record to fill out the form
    $scope.readOneInactivity = function(id) {
        // change modal title
        $('#modal-inactivity-title').text("Editar Inactividad");
        // show udpate inactivity button
        $('#btn-update-inactivity').show();
        // show create inactivity button
        $('#btn-create-inactivity').hide();

        // post id of inactivity to be edited
        $http.post('php/action/inactivity/read_one_inactivity.php', {
            'id' : id
        })
        .success(function(data, status, headers, config) {
            // put the values in form
            $scope.inactivity_id = data.id;
            $scope.inactivity_description = data.description;
            $scope.inactivity_startDate = data.startDate;
            $scope.inactivity_endDate = data.endDate;
            $scope.inactivity_agentFirstName = data.agentFirstName;
            $scope.inactivity_agentLastName = data.agentLastName;
            $scope.inactivity_sectionName = data.sectionName;
            // show modal
            $('#modal-inactivity-form').openModal();
        })
        .error(function(data, status, headers, config) {
            Materialize.toast(
                $scope.SYSTEM.DEFAULT_MESSAGE_ON_ERROR_RETREIVING_DATA, $scope.defaultToastTime);
        });
    };

    // ----------------------------------------
    // SCHEDULES DATA
    // ----------------------------------------
    // create new schedule
    $scope.createSchedule = function() {
        // fields in key-value pairs
        $http.post('php/action/schedule/create_schedule.php', {
                'scheduleDate' : $scope.schedule_scheduleDate
            }
        ).success(function (data, status, headers, config) {
            // tell the user new schedule was created
            Materialize.toast(data, $scope.defaultToastTime);
            // refresh the list
            $scope.getAllSchedules();
        });
    };

    // read schedules
    $scope.getAllSchedules = function() {
        $http.get("php/action/schedule/read_schedules.php").success(function(response) {
            if (Object.keys(response.records).length>0) {
                $scope.schedules = response.records[0];
            }
        });
    };

    // retrieve record to fill out the form
    $scope.readOneSchedule = function(scheduleDate) {
        $('#modal-schedule-form').openModal();
    };

    // ----------------------------------------
    // SCHEDULE DETAILS DATA
    // ----------------------------------------

    // retrieve record to fill out the form
    $scope.readOneScheduleDetail = function(scheduleDate) {
        // change modal title
        $('#modal-schedule-detail-title').text("Detalle Programación");

        $scope.getAllScheduleDetails(scheduleDate, function(success) {
            if (success)
            {
                $('#modal-schedule-detail-form').openModal();
            }
        });
    };

    $scope.getAllScheduleDetails = function(scheduleDate, callback) {

        $http.post("php/action/schedule/read_schedule_details.php", {
                'scheduleDate' : scheduleDate
            }
        ).success(function(response) {
            if (Object.keys(response.records).length > 0 && response.records[0][0] != undefined) {
                $scope.scheduleDetails = response.records[0];
                return callback(true);
            }
            else
            {
                return callback(false);
            }
        });
    };

    // ----------------------------------------
    // IPAT DELIVERY DATA
    // ----------------------------------------
    // create new ipat delivery
    $scope.createIpatDelivery = function() {
        $scope.readTotalIpatsDelivered(function(hasPendingDeliveries, delivered) {
            $('#modal-ipat-delivery-form').closeModal();
            var continueSelected = false;
            if (hasPendingDeliveries) {
                var totalDelivered = Object.keys(delivered).length;
                var ipatsAsString = $scope.getIpatsDeliveredAsString(delivered);
                continueSelected =
                    confirm(
                        "Al agente " 
                            + $scope.ipatDelivery.deliveredTo + " se le han entregado " 
                            + totalDelivered + " IPATs (" + ipatsAsString +")."
                            +" Desea continuar con la operación?");
            }
            if (continueSelected || !hasPendingDeliveries)
            {
                $http.post('php/action/ipat_delivery/create_ipat_delivery.php', {
                    'quantity' : $scope.ipatDelivery.quantity,
                    'deliveredBy' : $scope.ipatDelivery.deliveredBy,
                    'deliveredTo' : $scope.ipatDelivery.deliveredTo
                }).success(function (data, status, headers, config) {
                    Materialize.toast(data, $scope.defaultToastTime);
                    $scope.clearIpatDeliveryModalForm();
                    $scope.getAllIpatDeliveries();
                    $scope.readIpatTransactionStatistics();
                    $scope.readActiveAgentsWithIpatTransactions();
                    $scope.readLastTwoIpatRanges();
                });
            }
        });
    };

    $scope.getIpatsDeliveredAsString = function(ipatsDelivered) {
        var content = "";
        for (var index = 0; index < Object.keys(ipatsDelivered).length; index++) {
            var ipatDelivered = ipatsDelivered[index];
            content = content + ipatDelivered.ipatNumber;
            if (index < Object.keys(ipatsDelivered).length - 1) {
                content = content + " , ";
            }
        }
        return content;
    };

    $scope.updateIpatDelivery = function() {
        $http.post('php/action/ipat_delivery/edit_ipat_delivery.php', {
                'quantity': $scope.SYSTEM.DEFAULT_IPATS_QUANTITY,
                'ipatNumber': $scope.ipatDelivery.records.ipatNumber,
                'deliveredTo' : $scope.ipatDelivery.deliveredTo,
                'idTransactionState' : $scope.ipatDelivery.records.idTransactionState
            }
        ).success(function (data, status, headers, config) {
            Materialize.toast(data, $scope.defaultToastTime);
            $('#modal-ipat-delivery-form').closeModal();
            $scope.clearIpatDeliveryModalForm();
            $scope.getAllIpatDeliveries();
            $scope.readIpatTransactionStatistics();
            $scope.readLastTwoIpatRanges();
        });
    };

    // display the create form for ipat deliveries
    $scope.showCreateIpatDeliveryForm = function() {
        $scope.clearIpatDeliveryModalForm();
        $scope.hideAllFieldIpatDeliveryModal();
        $('#modal-ipat-delivery-title').text("Entrega Nuevo IPAT");
        $('#ipat-delivery-quantity').show();
        $('#btn-create-ipat-delivery').show();
        $('#ipat-delivery-delivered-to').show();
    };

    $scope.changedTransactionState = function() {
        var idTransactionState = $scope.ipatDelivery.idTransactionState;
        $scope.hideAllFieldIpatDeliveryModal();

        if (idTransactionState == $scope.SYSTEM.TRANSACTION_STATE_RECEIVED_WITH_PENDING) {
            $('#ipat-delivery-partially-received-to').show();
        }
        if (idTransactionState == $scope.SYSTEM.TRANSACTION_STATE_RECEIVED) {
            $('#ipat-delivery-received-to').show();
            $('#ipat-delivery-address').show();
            $('#ipat-delivery-vehicle-plaque').show();
            $('#ipat-delivery-accident-date').show();
            $('#ipat-delivery-diligence-date').show();
            $('#ipat-delivery-severity').show();
            $('#ipat-delivery-classification').show();
            $('#ipat-delivery-criminal-news').show();
            $('#ipat-delivery-observation').show();
        }

        $('#ipat-delivery-transaction-state').show();
        $('#btn-receive-ipat-delivery').show();
    };

    $scope.hideAllFieldIpatDeliveryModal = function() {
        $('#btn-update-ipat-delivery').hide();
        $('#btn-receive-ipat-delivery').hide();
        $('#btn-create-ipat-delivery').hide();
        $('#btn-export-ministry-ipat-delivery').hide();
        $('#btn-export-ministry-pdf-ipat-delivery').hide();
        $('#ipat-delivery-quantity').hide();
        $('#ipat-delivery-delivered-to').hide();
        $('#ipat-delivery-transaction-state').hide();
        $('#ipat-delivery-received-to').hide();
        $('#ipat-delivery-partially-received-to').hide();
        $('#ipat-delivery-address').hide();
        $('#ipat-delivery-vehicle-plaque').hide();
        $('#ipat-delivery-accident-date').hide();
        $('#ipat-delivery-diligence-date').hide();
        $('#ipat-delivery-severity').hide();
        $('#ipat-delivery-classification').hide();
        $('#ipat-delivery-criminal-news').hide();
        $('#ipat-delivery-observation').hide();
        $('#ipat-delivery-export-ministry-date').hide();
    };

    $scope.readOneIpatDelivery = function(ipatNumber) {
        $http.post('php/action/ipat_delivery/read_one_ipat_delivery.php', {
            'ipatNumber' : ipatNumber
        }).success(function(transaction, status, headers, config) {

            $scope.readOneIpatInCharge(transaction, function(transaction, incharge){

                $scope.hideAllFieldIpatDeliveryModal();
                $scope.ipatDelivery = $scope.getIpatDelivery(transaction.records);

                var idTransactionState = parseInt(transaction.records.idTransactionState);
                if (idTransactionState == $scope.SYSTEM.TRANSACTION_STATE_DELIVERED) {
                    $('#modal-ipat-delivery-title').text("Editar Entrega IPAT");
                    $('#btn-update-ipat-delivery').show();
                    $('#ipat-delivery-delivered-to').show();
                }
                if (idTransactionState == $scope.SYSTEM.TRANSACTION_STATE_RECEIVED) {

                    $('#modal-ipat-delivery-title').text("Editar Recepción IPAT");
                    $('#ipat-delivery-received-to').show();
                    $('#ipat-delivery-address').show();
                    $('#ipat-delivery-vehicle-plaque').show();
                    $('#ipat-delivery-accident-date').show();
                    $('#ipat-delivery-diligence-date').show();
                    $('#ipat-delivery-severity').show();
                    $('#ipat-delivery-classification').show();
                    $('#ipat-delivery-criminal-news').show();
                    $('#ipat-delivery-observation').show();

                    var hasReceptionDate =
                        transaction.records.receptionDate != null && transaction.records.receptionDate != '' ?
                            true : false;
                    var currentAgentPlaque =
                        incharge != null && incharge.agent != undefined && incharge.agent.plaque != undefined ?
                            incharge.agent.plaque : '';
                    var previouslyReceivedBy = transaction.records.receivedBy;
                    if (hasReceptionDate
                        && ($scope.isAdministrator() || currentAgentPlaque === previouslyReceivedBy)) {
                        $('#btn-receive-ipat-delivery').show();
                    }
                }

            $('#modal-ipat-delivery-form').openModal();

            });

        }).error(function(data, status, headers, config) {
            Materialize.toast(
                $scope.SYSTEM.DEFAULT_MESSAGE_ON_ERROR_RETREIVING_DATA, $scope.defaultToastTime);
        });
    };

    $scope.getIpatDelivery = function(ipatDelivery) {

        var transaction = {};
        transaction.records = ipatDelivery;
        transaction.ipatNumber = ipatDelivery.ipatNumber;
        transaction.address = ipatDelivery.address;
        transaction.idClassification = "" + ipatDelivery.idClassification;
        transaction.idSeverity = "" + ipatDelivery.idSeverity;
        transaction.idTransactionState = "" + ipatDelivery.idTransactionState;
        transaction.vehiclePlaque = ipatDelivery.vehiclePlaque;
        transaction.observation = ipatDelivery.observation;
        transaction.receivedTo = ipatDelivery.receivedTo;
        transaction.deliveredTo = ipatDelivery.deliveredTo;
        transaction.criminalNews = ipatDelivery.criminalNews;

        if (ipatDelivery.deliveredBy != undefined && ipatDelivery.deliveredBy != "") {
            transaction.deliveredBy = ipatDelivery.deliveredBy;
        }

        if (ipatDelivery.partiallyReceivedBy != undefined
            && ipatDelivery.partiallyReceivedBy != "") {
            transaction.partiallyReceivedBy = ipatDelivery.partiallyReceivedBy;
        }

        if (ipatDelivery.partiallyReceivedTo != undefined
            && ipatDelivery.partiallyReceivedTo != "") {
            transaction.partiallyReceivedTo = ipatDelivery.partiallyReceivedBy;
        }

        if (ipatDelivery.receivedBy != undefined && ipatDelivery.receivedBy != "") {
            transaction.receivedBy = ipatDelivery.receivedBy;
        }

        if (ipatDelivery.accidentDate != undefined && ipatDelivery.accidentDate != "") {
            transaction.accidentDate =
                new Date(ipatDelivery.accidentDate + " " + $scope.SYSTEM.STATED_TIME);
        }

        if (ipatDelivery.diligenceDate != undefined && ipatDelivery.diligenceDate != "") {
            transaction.diligenceDate =
                new Date(ipatDelivery.diligenceDate + " " + $scope.SYSTEM.STATED_TIME);
        }

        if (ipatDelivery.deliveryDate != undefined && ipatDelivery.deliveryDate != "") {
            transaction.deliveryDate = new Date(ipatDelivery.deliveryDate);
        }

        if (ipatDelivery.partialReceptionDate != undefined
            && ipatDelivery.partialReceptionDate != "") {
            transaction.partialReceptionDate = new Date(ipatDelivery.partialReceptionDate);
        }

        if (ipatDelivery.receptionDate != undefined
            && ipatDelivery.receptionDate != "") {
            transaction.receptionDate = new Date(ipatDelivery.receptionDate);
        }

        return transaction;
    };

    // read all deliveries
    $scope.getAllIpatDeliveries = function() {
        $http.post("php/action/ipat_delivery/read_ipat_deliveries.php")
            .success(function(response) {
                if (Object.keys(response.records).length > 0) {
                    $scope.ipatDeliveries = response.records;
                }
        });
    };

    $scope.receiveOneIpatDelivery = function(ipatNumber, idTransactionState) {
        $http.post('php/action/ipat_delivery/read_one_ipat_delivery.php', {
                'ipatNumber' : ipatNumber,
                'idTransactionState': idTransactionState
            }
        ).success(function (data, status, headers, config) {
            $scope.hideAllFieldIpatDeliveryModal();
            $('#modal-ipat-delivery-title').text("Recepción de IPAT");
            $('#btn-receive-ipat-delivery').show();
            $('#ipat-delivery-transaction-state').show();

            $scope.ipatDelivery = data.records;
            $scope.changeDefaultReceivers();
            $('#modal-ipat-delivery-form').openModal();
        });
    };

    $scope.openModifyIpatDeliveryModal = function(ipatNumber, idTransactionState) {
        $http.post('php/action/ipat_delivery/read_one_ipat_delivery.php', {
                'ipatNumber' : ipatNumber,
                'idTransactionState': idTransactionState
            }
        ).success(function (transaction) {
            $scope.ipatDelivery = $scope.getIpatDelivery(transaction.records);
            $scope.changeDefaultReceivers();
            $scope.getAllIpatInCharges();
            $('#modal-modify-ipat-delivery-form').openModal();
        });
    };

    $scope.openViewIpatDeliveryModal = function(ipatNumber, idTransactionState) {
        $http.post('php/action/ipat_delivery/read_one_ipat_delivery.php', {
                'ipatNumber' : ipatNumber,
                'idTransactionState': idTransactionState
            }
        ).success(function (transaction) {
            $scope.ipatDelivery = $scope.getIpatDelivery(transaction.records);
            $scope.changeDefaultReceivers();
            $('#modal-view-ipat-delivery-form').openModal();
        });
    };

    $scope.changeDefaultReceivers = function(){
        if ($scope.ipatDelivery != undefined
            && $scope.ipatDelivery.agentPartiallyReceivedTo != undefined
            && $scope.ipatDelivery.agentPartiallyReceivedTo.plaque == null) {
            $scope.ipatDelivery.partiallyReceivedTo = $scope.ipatDelivery.deliveredTo;
            $scope.ipatDelivery.agentPartiallyReceivedTo = $scope.ipatDelivery.agentDeliveredTo;
        }

        if ($scope.ipatDelivery != undefined
            && $scope.ipatDelivery.agentReceivedTo != undefined
            && $scope.ipatDelivery.agentReceivedTo.plaque == null) {
            $scope.ipatDelivery.receivedTo = $scope.ipatDelivery.partiallyReceivedTo;
            $scope.ipatDelivery.agentReceivedTo = $scope.ipatDelivery.agentPartiallyReceivedTo;
        }
    };

    $scope.receiveIpat = function() {
        var idTransactionState =
            $scope.ipatDelivery.idTransactionState != undefined
                ? $scope.ipatDelivery.idTransactionState
                : $scope.ipatDelivery.records.idTransactionState
        $http.post('php/action/ipat_delivery/receive_ipat_delivery.php', {
            'ipatNumber' : $scope.ipatDelivery.ipatNumber,
            'idTransactionState' : idTransactionState,
            'idSeverity': $scope.ipatDelivery.idSeverity,
            'idClassification': $scope.ipatDelivery.idClassification,
            'accidentDate': $scope.ipatDelivery.accidentDate,
            'diligenceDate': $scope.ipatDelivery.diligenceDate,
            'address': $scope.ipatDelivery.address,
            'vehiclePlaque': $scope.ipatDelivery.vehiclePlaque,
            'deliveredBy': $scope.ipatDelivery.deliveredBy,
            'deliveredTo': $scope.ipatDelivery.deliveredTo,
            'partiallyReceivedBy': $scope.ipatDelivery.partiallyReceivedBy,
            'partiallyReceivedTo': $scope.ipatDelivery.partiallyReceivedTo,
            'receivedBy': $scope.ipatDelivery.receivedBy,
            'receivedTo': $scope.ipatDelivery.receivedTo,
            'criminalNews': $scope.ipatDelivery.criminalNews,
            'observation': $scope.ipatDelivery.observation
        }).success(function (data, status, headers, config) {
            Materialize.toast(data, $scope.defaultToastTime);
            $('#modal-ipat-delivery-form').closeModal();
            $scope.clearIpatDeliveryModalForm();
            $scope.getAllIpatDeliveries();
            $scope.readIpatTransactionStatistics();
            $scope.readLastTwoIpatRanges();
        });
    };

    $scope.modifyIpatDelivery = function(transaction) {
        $http.post('php/action/ipat_delivery/modify_ipat_delivery.php', {
            'transaction': transaction
        }).success(function (data, status, headers, config) {
            Materialize.toast(data, $scope.defaultToastTime);
            $('#modal-modify-ipat-delivery-form').closeModal();
            FormHelper.clearForm('#id-modify-ipat-delivery-form');
            $scope.getAllIpatDeliveries();
            $scope.readIpatTransactionStatistics();
            $scope.readLastTwoIpatRanges();
        });
    }

    $scope.readTotalIpatsDelivered = function(callback) {
        var deliveredTo = $scope.ipatDelivery.deliveredTo;
        $http.post('php/action/ipat_delivery/read_total_delivered_ipat_delivery.php', {
            'deliveredTo': deliveredTo
        }).success(function (data, status, headers, config) {
            var delivered = data.records;
            if (delivered == undefined || Object.keys(delivered).length == 0) {
                return callback(false, null);
            }
            return callback(true, delivered);
        });
    };

    $scope.filterIpatStatistics = function() {
        $scope.readIpatTransactionStatistics($scope.ipatDelivery.deliveredToFilter);
        $scope.areValidDeliveredDates($scope.ipatDelivery.deliveredToFilter);
    };

    $scope.getFiltersCriteria = function() {
        $http.get("php/action/ipat_delivery/get_ipat_transaction_filters_criteria.php")
            .success(function(response) {
            if (response.records != undefined) {
                $scope.ipatDeliveryFilters = response.records;
            }
        });
    };

    $scope.filterIpatDeliveries = function(filter) {
        var selectedFilter =
            $scope.searchFilter(filter.filterId);
        selectedFilter.valueFilter = filter.valueFilter;
        $http.post(
            "php/action/ipat_delivery/filter_ipat_deliveries.php",
            { 'filter': selectedFilter })
            .success(function(response) {
                if (response.records !== undefined && Object.keys(response.records).length > 0) {
                    $scope.ipatDeliveries = response.records;
                }
                else if(response.records !== undefined
                    && Object.keys(response.records).length === 0) {
                    alert("No se encontró información con su criterio de búsqueda.");
                    $scope.getAllIpatDeliveries();
                }
        });
    };

    $scope.searchFilter = function(filterId) {
        var selectedFilter = [];
        for (var i = 0; i < $scope.ipatDeliveryFilters.length; i++) {
            var filter = $scope.ipatDeliveryFilters[i];
            if (parseInt(filter.filterId) == filterId ) {
                selectedFilter = filter;
                break;
            }
        }
        return selectedFilter;
    };

    $scope.readActiveAgentsWithIpatTransactions = function() {
        $http.get("php/action/ipat_delivery/read_active_agents_with_ipat_deliveries.php")
            .success(function(response) {
                if (response.records != undefined) {
                    let transactions = response.records;
                    let hash = response.hash;
                    if (ArrayHelper.count(transactions) != ArrayHelper.count($scope.activeAgentsWithIpatTransactions)
                        || hash != $scope.activeAgentsWithIpatTransactionsHash)
                    $scope.activeAgentsWithIpatTransactions = response.records;
                    $scope.activeAgentsWithIpatTransactionsHash = hash;
                }
            }
        );
    };

    $scope.readIpatTransactionStatistics = function(deliveredTo) {
        $http.post("php/action/ipat_delivery/read_ipat_delivery_statistics.php", {
            'deliveredTo': deliveredTo
        }).success(function(response) {
            if (response.records != undefined) {
                $scope.ipatTransactionStatistics = response.records;
            }
        });
    };

    $scope.openIpatDeliveryGeneratePeaceAndSaveModal = function() {
        ModalHelper.openModal("#modal-ipat-delivery-peace-and-save");
    };

    $scope.readIpatRanges = function() {
        $http.get("php/action/ipat_delivery/read_ipat_ranges.php")
            .success(function(response) {
                if (response.records != undefined) {
                    $scope.ipatTransactionIpatRanges = response.records;
                }
            });
    };

    $scope.generateIpatDeliveryPeaceAndSave = function(selectedPlaque, selectedIpatId) {

        $http.post("php/action/ipat_delivery/read_pending_ipats_for_peace_and_save.php", {
            'plaque': selectedPlaque,
            'ipatId': selectedIpatId,
        }).success(function(response) {
            if (response.records != undefined) {
                if (Object.keys(response.records).length > 0) {
                    var message =
                        'El agente tiene los siguientes ipats pendientes: ['
                            + ArrayHelper.getValuesAsString(response.records[0], 'ipatNumber')
                            + ']';
                    alert(message);
                }
                else {
                    window.location =
                        'php/action/ipat_delivery/export_pdf_peace_and_save.php?plaque='
                        + selectedPlaque + '&ipatId=' + selectedIpatId;
                }
            }
            else {
                alert('No se encontró información asociada al agente seleccionado.');
            }
            FormHelper.clearForm("#id-ipat-transaction-peace-and-save-form");
            ModalHelper.closeModal("#modal-ipat-delivery-peace-and-save");
        });
    };

    $scope.readLastTwoIpatRanges = function() {
        $http.post("php/action/ipat_delivery/read_last_two_ipat_ranges.php")
            .success(function(response) {
                if (response.records != undefined) {
                    $scope.lastTwoIpatRanges = response.records;
                }
            });
    };

    $scope.openExportMinistryModal = function() {
        $scope.hideAllFieldIpatDeliveryModal();
        $('#ipat-delivery-export-ministry-date').show();
        $('#btn-export-ministry-ipat-delivery').show();
        $('#modal-ipat-delivery-title').text("Exportar Datos Ministerio");
        $('#modal-ipat-delivery-form').openModal();
    };

    $scope.openExportMinistryPDFModal = function() {
        $scope.hideAllFieldIpatDeliveryModal();
        $('#ipat-delivery-export-ministry-date').show();
        $('#btn-export-ministry-pdf-ipat-delivery').show();
        $('#modal-ipat-delivery-title').text("Exportar PDF Datos Ministerio");
        $('#modal-ipat-delivery-form').openModal();
    };

    $scope.exportMinistryIpatsDelivered = function(fileType) {

        $http.post('php/action/ipat_delivery/generate_content_csv_ministry_ipat_deliveries.php', {
            'exportMinistryDate':  $scope.ipatDelivery.exportMinistryDate
        }).success(function(data, status, headers, config) {
            if (data != undefined && data != '') {
                window.location =
                    'php/action/ipat_delivery/export_csv_ministry_ipat_deliveries.php';
            }
            $('#modal-ipat-delivery-form').closeModal();
            $scope.clearIpatDeliveryModalForm();
        });
    };

    $scope.exportMinistryPDFIpatsDelivered = function() {
        var date = $scope.formatDateApplier($scope.ipatDelivery.exportMinistryDate);
        window.location =
            'php/action/ipat_delivery/export_pdf_ministry_ipat_deliveries.php?exportMinistryDate='
                + date;
        $('#modal-ipat-delivery-form').closeModal();
        $scope.clearIpatDeliveryModalForm();
    };

    $scope.areValidDeliveredDates = function(deliveredTo) {
        $http.post("php/action/ipat_delivery/validate_ipat_delivery_delivered_dates.php", {
            'deliveredTo': deliveredTo
        }).success(function(response) {
            if (response.records != undefined) {
                $scope.areValidDeliveredDatesIpatTransaction = response.records;
            }
            else {
                 $scope.areValidDeliveredDatesIpatTransaction = true;
            }
        });
    };

    $scope.getIpatTransactionStatisticByState = function(idTransactionState) {
        var transactionState = null;
        if ($scope.ipatTransactionStatistics != undefined 
            && Object.keys($scope.ipatTransactionStatistics).length > 0) {
            $.each($scope.ipatTransactionStatistics, function(index, transaction) {
                if (transaction.idTransactionState == idTransactionState) {
                    transactionState = transaction;
                }
            });
        }
        return transactionState;
    };


    $scope.openIpatTransactionNoveltyModalForm = function(ipatNumber) {
        $scope.selectedIpatNumber = ipatNumber;
        $('#modal-ipat-transaction-novelty-form').openModal();
    };


     $scope.addIpatTransactionNovelty = function(isValid) {
        if ($scope.ipatTransactionNovelty != undefined && isValid) {
            $scope.ipatTransactionNovelty.ipatNumber = $scope.selectedIpatNumber;
            $http.post('php/action/ipat_delivery/add_ipat_transaction_novelty.php', {
                'ipatTransactionNovelty' : $scope.ipatTransactionNovelty
                }
            ).success(function (data, status, headers, config) {
                Materialize.toast(data, $scope.defaultToastTime);
                $scope.clearIpatTransactionNovelty();
                $scope.getAllIpatDeliveries();
                $('#modal-ipat-transaction-novelty-form').closeModal();
            });
        }
    };

    $scope.clearIpatTransactionNovelty = function() {
        if ($scope.ipatTransactionNovelty != undefined) {
            $scope.ipatTransactionNovelty = {};
        }
        $("#id-ipat-transaction-novelty-form")[0].reset();
    };

    // ----------------------------------------
    // IPAT SEVERITY DATA
    // ----------------------------------------
    $scope.getAllIpatSeverities = function() {
        $http.get("php/action/ipat_severity/read_ipat_severities.php")
            .success(function(response) {
                if (response.records[0]['id']!=undefined) {
                    $scope.ipatSeverities = response.records;
                }
                else{
                    Materialize.toast("No se encontraron gravedades.", $scope.defaultToastTime);
                }
        });
    };

    // ----------------------------------------
    // IPAT TRANSACTION STATE DATA
    // ----------------------------------------
    $scope.getIpatTransactionStates = function() {
        $http.get("php/action/ipat_transaction_state/read_ipat_transaction_states.php")
            .success(function(response) {
                if (response.records[0]['id']!=undefined) {
                    let states = response.records;
                    let hash = response.hash;
                    if (ArrayHelper.count(states) != ArrayHelper.count($scope.ipatTransactionStates)
                        || hash != $scope.ipatTransactionStatesHash) {
                        $scope.ipatTransactionStates = states;
                        $scope.ipatTransactionStatesHash = hash;
                    }
                }
                else{
                    Materialize.toast("No se encontraron estados.", $scope.defaultToastTime);
                }
        });
    };

    $scope.getAllIpatTransactionStates = function() {
        $http.get("php/action/ipat_transaction_state/read_all_ipat_transaction_states.php")
            .success(function(response) {
                if (response.records[0]['id']!=undefined) {
                    $scope.allIpatTransactionStates = response.records;
                }
                else{
                    Materialize.toast("No se encontraron estados.", $scope.defaultToastTime);
                }
            });
    };

    // ----------------------------------------
    // IPAT INCHARGES DATA
    // ----------------------------------------
    $scope.getAllIpatInCharges = function() {
        $http.get("php/action/ipat_incharge/read_ipat_incharges.php")
            .success(function(response) {
                if (response.records[0]['plaqueAgent']!=undefined) {
                    $scope.ipatInCharges = response.records;
                }
                else{
                    Materialize.toast("No se encontraron encargados para los IPATs.", $scope.defaultToastTime);
                }
        });
    };

     $scope.readOneIpatInCharge = function(transaction, callback) {
        $http.get("php/action/ipat_incharge/read_one_ipat_incharge.php")
            .success(function(response) {
                if (response.records!=undefined) {
                    $scope.ipatInCharge = response.records;
                    $.performCallback(callback(transaction, response.records));
                }
                else{
                    $.performCallback(callback(transaction, null));
                    Materialize.toast("No se encontró el encargado para los IPATs.", $scope.defaultToastTime);
                }
        });
    };
    // ----------------------------------------
    // IPAT CLASSIFICATIONS DATA
    // ----------------------------------------
    $scope.getAllIpatClassifications = function() {
        $http.get("php/action/ipat_classification/read_ipat_classifications.php")
            .success(function(response) {
                if (response.records[0]['id']!=undefined) {
                    $scope.ipatClassifications = response.records;
                }
                else{
                    Materialize.toast("No se encontraron clasificaciones para los IPATs.", $scope.defaultToastTime);
                }
        });
    };

    // ----------------------------------------
    // SESSION
    // ----------------------------------------
    $scope.closeSession = function() {
        $http.get("php/action/session/close_session.php")
            .success(function(response) {
                Materialize.toast("Cerrando sesión.", $scope.defaultToastTime);
                window.location = 'login.php';
            });
    };

    $scope.activateSelects = function activateSelects() {
        $('select').material_select();
    };

    // ----------------------------------------
    // USER
    // ----------------------------------------
    $scope.updatePassword = function() {
        var password = $scope.user != undefined ? $scope.user.password : null;
        var newPassword = $scope.user != undefined ? $scope.user.newPassword : null;
        if (confirm("Desea continuar con la actualización?")) {
            $http.post("php/action/user/change_password.php", {
            'password': password,
            'newPassword': newPassword
            }).success(function (data, status, headers, config) {
                Materialize.toast(data, $scope.defaultToastTime);
                $scope.clearChangePasswordUserForm();
            });
        }
    };

    $scope.clearChangePasswordUserForm = function() {
        $scope.user.password = "";
        $scope.user.newPassword = "";
    };

    // ----------------------------------------
    // REPORTS
    // ----------------------------------------
    $scope.readIpatsReceivedWithPendingState = function() {
        $http.get("php/action/reports/read_ipats_received_with_pending_state.php")
            .success(function(response) {
                if (Object.keys(response.records).length>0) {
                    $scope.reportDataIpatsReceivedWithPendingState = response.records;
                }
            });
    };

    $scope.formatDateApplier = function(date) {
        var prefix = "0";
        var day = date.getDate() < 10 ?  + (prefix + date.getDate()) : date.getDate();
        var currentMonth = date.getMonth() + 1;
        var month = currentMonth < 10 ? (prefix +  currentMonth) : currentMonth;
        var year = date.getFullYear();
        return (year + "-" + month + "-" + day);
    };

    $scope.executeQuery = function(queryManager) {
        $http.post('php/action/support/execute_query.php', {
                'queryInput' : queryManager.queryInput
            }
        ).success(function (output) {
            console.log(output);
            if (ArrayHelper.hasData(output.records)) {
                $scope.queryManager.queryOutput = JSON.stringify(output.records);
            }
        });
    };
});
